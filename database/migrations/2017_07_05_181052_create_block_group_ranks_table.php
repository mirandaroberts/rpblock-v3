<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockGroupRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_group_ranks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_male', 50);
            $table->string('name_female', 50);
            $table->integer('group_id');
            $table->integer('weight');
            $table->boolean('default');
            $table->boolean('can_edit');
            $table->boolean('can_update');
            $table->boolean('can_invite');
            $table->boolean('can_remove');
            $table->boolean('can_promote');
            $table->boolean('can_demote');
            $table->integer('min_age');
            $table->integer('max_age');
            $table->integer('post_reqs');
            $table->integer('posts_needed');
            $table->integer('max_amount');
            $table->boolean('leader');
            $table->boolean('coleader');
            $table->boolean('can_be_challenged');
            $table->boolean('prisoner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_group_ranks');
    }
}
