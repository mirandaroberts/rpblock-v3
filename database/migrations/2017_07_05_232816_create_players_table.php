<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 20);
            $table->string('email');
            $table->string('password');
            $table->integer('block_id');
            $table->string('role');
            $table->date('dob');
            $table->string('confirm_token')->nullable();
            $table->string('joined_ip');
            $table->string('last_ip')->nullable();
            $table->boolean('active');
            $table->integer('currency');
            $table->integer('character_slots');
            $table->boolean('hidden');
            $table->string('icon');
            $table->text('bio');
            $table->boolean('title_unlocked');
            $table->string('title');
            $table->boolean('vista_unlocked');
            $table->string('vista');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}