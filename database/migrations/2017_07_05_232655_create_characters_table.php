<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20)->unique();
            $table->string('surname', 50);
            $table->integer('block_id');
            $table->integer('player_id');
            $table->integer('cost');
            $table->string('gender');
            $table->integer('birth_month');
            $table->integer('months_old');
            $table->text('appearance');
            $table->text('personality');
            $table->text('history');
            $table->text('relationships');
            $table->integer('mother');
            $table->integer('father');
            $table->string('pvp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
