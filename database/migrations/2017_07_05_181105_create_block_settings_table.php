<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 255);
            $table->boolean('soft_deletes')->default(1);
            $table->boolean('display_trashed_posts')->default(1);
            $table->integer('min_register_age')->default(13);
            $table->integer('days_until_archive')->default(30);
            $table->integer('old_thread_threshold')->default(7);
            $table->integer('pagination_threads')->default(10);
            $table->integer('pagination_categories')->default(10);
            $table->integer('pagination_posts')->default(10);
            $table->integer('days_until_inactive')->default(30);
            $table->boolean('enable_currency')->default(1);
            $table->boolean('allow_currency_transfer')->default(1);
            $table->integer('currency_on_join')->default(100);
            $table->integer('currency_on_ooc_post')->default(0);
            $table->integer('currency_on_ic_post')->default(1);
            $table->integer('currency_on_ic_thread')->default(1);
            $table->integer('currency_on_ooc_thread')->default(1);
            $table->integer('currency_on_thread_exit')->default(1);
            $table->integer('character_slot_price')->default(50);
            $table->integer('character_slots')->default(3);
            $table->integer('character_slot_unlock')->default(100);
            $table->string('group_name', 20)->default('Group');
            $table->string('rank_name', 20)->default('Rank');
            $table->string('default_group', 50)->default('Groupless');
            $table->string('default_rank', 50)->default('Rankless');
            $table->integer('time_modifier')->default(2);
            $table->integer('min_free_age')->default(6);
            $table->integer('min_age_cost')->default(200);
            $table->integer('adult_age')->default(24);
            $table->integer('maximum_age')->default(153);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_settings');
    }
}
