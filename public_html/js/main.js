$(document).ready( function() {
    // Mobile Sidebar
    $('.ui.right.sidebar').sidebar({
        transition: 'push',
    });

    $('.sidebar-toggle').on('click', function () {
        $('.ui.right.sidebar').sidebar('toggle');
    });

    // Nav effects
    $('nav.menu a').click( function() {
        $(this).parent().find('.current').removeClass('current');
        $(this).addClass('current');
    });

    $('a[data-connect]').click( function() {
        var i = $(this).find('i');
        i.hasClass('icon-co llapse-top') ? i.removeClass('icon-collapse-top').addClass('icon-collapse') : i.removeClass('icon-collapse').addClass('icon-collapse-top');
        $(this).parent().parent().toggleClass('all').next().slideToggle();
    });

    // Scroll-to-top
    $(window).scroll(function() {
        var w = $(window).width();
        if(w < 768) {
            $('#top-button').hide();
        } else {
            var e = $(window).scrollTop();
            e>150?$('#top-button').fadeIn() :$('#top-button').fadeOut();
        }
    });

    // Theme switcher
    $('#theme-switcher').on('change', function () {
        $('#theme-sheet').href = $(this).val();
    });

    // Hack because summernote inserts <p> tags instead of <br>
    function pasteHtmlAtCaret(html) {
        var sel, range;
        if (window.getSelection) {
            // IE9 and non-IE
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();

                // Range.createContextualFragment() would be useful here but is
                // only relatively recently standardized and is not supported in
                // some browsers (IE9, for one)
                var el = document.createElement("div");
                el.innerHTML = html;
                var frag = document.createDocumentFragment(), node, lastNode;
                while ( (node = el.firstChild) ) {
                    lastNode = frag.appendChild(node);
                }
                range.insertNode(frag);

                // Preserve the selection
                if (lastNode) {
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            }
        } else if (document.selection && document.selection.type != "Control") {
            // IE < 9
            document.selection.createRange().pasteHTML(html);
        }
    }
});
