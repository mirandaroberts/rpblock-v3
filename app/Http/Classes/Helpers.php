<?php namespace App\Http\Classes;

use App\Models\Character\Character;
use App\Models\Character\CharacterInventory;
use App\Events\NotificationMade;
use App\Models\Block\Post;
use App\Models\Player\PlayerInventory;
use Carbon\Carbon;
use App\Models\Player\Player;
use App\Models\Player\CurrencyLog;
use App\Models\Block\ModboxTicket;
use App\Models\Player\Notification;
use App\Models\Block\Block;
use App\Models\Block\Thread;
use App\Models\Player\ForumTag;

class Helpers {
    public $block;

    /**
     * Get block of current subdomain
     * @return string
     */
    public function getBlock()
    {
        if ($this->block) return $this->block;
        $host = explode('.', request()->server('HTTP_HOST'))[0];
        if ($host != 'www') return Block::where('name', $host)->with('settings')->firstOrFail();
        else return 'www';
    }

    /**
     * Award points and achievements for posting.
     * @param $type
     * @param $author
     * @param $postId
     * @param bool $thread
     */
    public function addPost($type, $author, $postId, $thread = null)
    {
        $block = $this->getBlock();
        $post = Post::find($postId);

        if ($type == 'ic') { // If IC post
            $character = Character::find($author);
            $character->last_post = Carbon::now();
            $character->post_count++;
            $character->save();

            if ($block->settings->enable_currency) {
                if ($thread) {
                    $this->changePlayerCurrency($character->player->id, $block->settings->currency_on_ic_thread, 'Started a thread with ' . $character->name, $character->id, $post->getUrl());
                } else {
                    $this->changePlayerCurrency($character->player->id, $block->settings->currency_on_ic_post, 'Posted with ' . $character->name, $character->id, $post->getUrl());
                }
            }
        } else { // If OOC post
            $player = Player::find($author);
            $player->post_count++;
            $player->save();

            if ($block->settings->enable_currency) {
                if ($thread) {
                    $this->changePlayerCurrency($player->id, $block->settings->currency_on_ooc_thread, 'Started an OOC thread', NULL, $post->getUrl());
                } else {
                    $this->changePlayerCurrency($player->id, $block->settings->currency_on_ooc_post, 'Made an OOC post', NULL, $post->getUrl());
                }
            }
        }

        return;
    }

    /**
     * Tag a player or character in a thread
     * @param $type
     * @param $id
     * @param $thread
     * @param bool $notice
     * @param int $tagBy
     */
    public function tag($type, $id, $character, $thread, $notice = false, $tagBy = 0) {
        $thread = Thread::findOrFail($thread);
        $tagCheck = ForumTag::where('tagged_id', $id)->where('thread_id', $thread->id)->get();

        if ($tagCheck->count() == 0) {
            ForumTag::create([
                'thread_id'    => $thread->id,
                'tagged_id'    => $id,
                'tagged_by'    => $tagBy,
                'character_id' => $character
            ]);


            if ($notice) {
                if ($type == 'ooc') {
                    $this->makeNotice('info', "You have been tagged the thread " . $thread->title . "!", '/thread/' . $thread->id, $id);
                } else {
                    $character = Character::findOrFail($id);
                    $this->makeNotice('info', "Your character " . $character->name . " has been tagged the thread " . $thread->title . "!", '/thread/' . $thread->id, $character->player->id);
                }
            }
        }

        return;
    }

    /**
     * Notify tagged players
     * @param $id
     */
    public function notifyTagged($id, $pid = null)
    {
        $thread = Thread::findOrFail($id);
        foreach ($thread->tags as $tag) {
            if (!$pid) $this->makeNotice('info', "There has been a new post in the thread " . $thread->title . "!", '/thread/' . $thread->id, $tag->tagged_id);
            else $this->makeNotice('info', "There has been a new post in the thread " . $thread->title . "!", '/thread/' . $thread->id . '#pid' . $pid, $tag->tagged_id);
        }
        return;
    }

    /**
     * Change a player's currency
     * @param $player
     * @param $amt
     * @param $reason
     * @param null $character
     * @param null $url
     * @return bool
     */
    public function changePlayerCurrency($player, $amt, $reason, $character = NULL, $url = NULL) {
        // Validate user
        $player = Player::find($player);

        if ($player && $amt) {
            $player->currency += $amt;
            $player->save();

            CurrencyLog::create([
                'player_id'    => $player->id,
                'amount'       => $amt,
                'reason'       => $reason,
                'character_id' => $character,
                'url'          => $url
            ]);

            return true;
        } else {
            return false;
        }

    }

    /**
     * Create a modbox ticket
     * @param array $data
     * @param $block
     */
    public function makeModboxTicket(array $data, $block) {
        $ticket = ModboxTicket::create([
            'title'       => $data['title'],
            'type'        => $data['type'],
            'status'      => 'Open',
            'reported_id' => $data['reported_id'],
            'sender_id'   => $data['sender_id'],
            'body'        => $data['body'],
            'url'         => $data['url'],
            'email'       => $data['email'],
            'ip'          => $_SERVER['REMOTE_ADDR'],
            'block_id'    => $block
        ]);

        // Send notice to staff
        $roles = ['STAFF', 'ADMIN'];
        $staff = Player::where('id', '!=', auth()->guard('player')->user()->id)->where('block_id', $block)->whereIn('role', $roles)->get();
        foreach ($staff as $s) $this->makeNotice('info', 'There is a new ' . $ticket->type . ' ticket in the Modbox.', '/modbox/ticket/' . $ticket->id, $s->id);

    }

    /**
     * Create a new notification
     * @param $type
     * @param $message
     * @param $url
     * @param $player
     * @return bool
     */
    public static function makeNotice($type, $message, $url, $player)
    {
        $notice = Notification::create([
            'player_id' => $player,
            'message'   => $message,
            'type'      => $type,
            'url'       => $url
        ]);

        broadcast(new NotificationMade($notice));
        return true;
    }

    /**
     * Give a character an item
     * @param $character
     * @param $item
     * @param bool $notify
     * @return bool
     */
    public function giveCharacterItem($character, $item, $notify = false)
    {
        CharacterInventory::create([
            'character_id' => $character->id,
            'item_id'      => $item->id
        ]);
        if ($notify) $this->makeNotice('info', $character->name . ' has been given a(n) ' . $item->name . '.', '/inventory', $character->player_id);
        return true;
    }

    /**
     * Give player an item
     * @param $player
     * @param $item
     * @param bool $notify
     * @return bool
     */
    public function givePlayerItem($player, $item, $notify = false)
    {
        PlayerInventory::create([
            'player_id' => $player->id,
            'item_id'   => $item->id
        ]);
        if ($notify) $this->makeNotice('info', 'You have been given a(n) ' . $item->name . '.', '/inventory', $player->id);
        return true;
    }
}
