<?php namespace App\Http\Classes;
/**
 * For use only by RPBlock hosted websites.
 * Creator: Miranda Roberts
 * Date: 12/22/2016
 *
 * Uses given modifier to calculate time elapsed
 */

use Carbon\Carbon;
use App\Models\Block\Block;

class TimeHandler {
    public $time;
    public $origin;
    public $modifier;

    /**
     * TimeHandler constructor.
     */
    public function __construct() {
        $host = explode('.', request()->server('HTTP_HOST'))[0];
        $block = Block::where('name', $host)->with('settings')->firstOrFail();
        $this->time = Carbon::now();
        $this->origin = Carbon::createFromDate(2018, 1, 19, 'America/Los_Angeles');
        $this->modifier = $block->settings->time_modifier;
    }

    /**
     * Returns difference in days
     * @return mixed
     */
    public function getDiff() {
        $dt = $this->time->diffInDays($this->origin);
        $dt = $dt * $this->modifier;
        return $dt;
    }

    /**
     * Returns current month
     * @return mixed
     */
    public function getMonth() {
        $dt = $this->getDiff();
        // Add to origin date and get month
        $month = $this->origin->addDays($dt)->format('F');
        return $month;
    }

    /**
     * Gets the month in number format for character creation
     * @return mixed
     */
    public function getMonthForCreation() {
        $dt = $this->getDiff();
        // Add to origin date and get month
        $month = $this->origin->addDays($dt)->format('m');
        return $month;
    }

    /**
     * Gets year
     * @return float
     */
    public function getYear() {
        $dt = $this->getDiff();
        $year = floor($dt / 365) + 1;
        return $year;
    }

    /**
     * Evaluates the age of a character
     * @param $value
     * @return string
     */
    public function getAge($value) {
        $years = floor($value / 12);
        $months = (($value / 12) - $years) * 12;
        if ($years < 1) return $value . ' months';
        else if ($months == 0) return $years . ' years';
        else if ($months == 1) return $years . ' years ' . $months . ' month';
        else return $years . ' years ' . $months . ' months';
    }
}
