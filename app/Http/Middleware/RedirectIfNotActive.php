<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RedirectIfNotActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routes = ['resend', 'activate', 'postActivate'];
        if (!in_array(Route::currentRouteName(), $routes) && request()->getRequestUri() != '/') {
            if (Auth::guard('player')->check() && Auth::guard('player')->user()->confirm_token != NULL) {
                $request->session()->flash('warning', 'You must activate your account before viewing this page!');
                return redirect('/activate');
            }
        }

        return $next($request);
    }
}
