<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Superuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() && auth()->user()->role != 'ROLE_ADMIN') {
            $request->session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/portal');
        }

        return $next($request);
    }
}
