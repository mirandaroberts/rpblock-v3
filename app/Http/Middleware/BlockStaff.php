<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BlockStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guard('player')->user() && (auth()->guard('player')->user()->role == 'STAFF' || auth()->guard('player')->user()->role == 'ADMIN')) {
            $request->session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/');
        }

        return $next($request);
    }
}
