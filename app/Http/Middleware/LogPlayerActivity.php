<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LogPlayerActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('player')->check()) {
            $player = Auth::guard('player')->user();

            $player->last_active = Carbon::now();
            $player->last_ip = $_SERVER['REMOTE_ADDR'];
            $player->save();
        }

        return $next($request);
    }
}
