<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Barryvdh\Debugbar\Facade as DebugBar;

class DisableDebugBar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((auth()->guard('player')->user() && (auth()->guard('player')->user()->username != 'Miranda')) || !auth()->guard('player')->user()) {
            DebugBar::disable();
        }

        return $next($request);
    }
}
