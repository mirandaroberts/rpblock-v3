<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = NULL)
    {
        if (!Auth::guard($guard)->check()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }
            else {
                $request->session()->flash('danger', 'You must be signed in to do this!');
                return redirect()->guest('/');
            }
        }

        return $next($request);
    }
}