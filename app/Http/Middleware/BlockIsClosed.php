<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Classes\Helpers;
use Illuminate\Support\Facades\Auth;

class BlockIsClosed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->getRequestUri() != '/inactive' && request()->getRequestUri() != '/' && request()->getRequestUri() != '/login') {
            $helpers = new Helpers();
            $block = $helpers->getBlock();

            if (!$block->active) {
                if (!auth()->guard('player')->check() || (!auth()->guard('player')->check() && !auth()->guard('player')->user()->role != 'ADMIN')) {
                    return redirect('/inactive');
                }
            }
        }

        return $next($request);
    }
}
