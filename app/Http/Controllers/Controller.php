<?php

namespace App\Http\Controllers;

use App\Models\Block\BlockSkin;
use App\Http\Classes\Helpers;
use App\Http\Classes\TimeHandler as Time;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $player;
    public $skin;
    public $helper;
    public $block;
    public $date = 'Unknown';
    public $time;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->helper = new Helpers();
            $this->block  = $this->helper->getBlock();
            $this->player = auth()->guard('player')->user();

            if (auth()->guard('player')->check()) $this->handlePlayer();
            if ($this->block != 'www') {
                if (!$this->skin) $this->skin = BlockSkin::where('block_id', $this->block->id)->where('default', 1)->first();
                $this->time = new Time();
                $this->date = $this->time->getMonth() . ', Y' . $this->time->getYear();
                view()->share('date', $this->date);
            }

            view()->share('block', $this->block);
            view()->share('skin', $this->skin);
            view()->share('player', $this->player);

            return $next($request);
        });
    }

    /**
     * Build Player
     */
    protected function handlePlayer()
    {
        if ($this->player->skin) $this->skin = $this->player->skin;
    }

    /**
     * Return current block
     * @return mixed
     */
    public function getBlock()
    {
        return $this->block;
    }
}
