<?php namespace App\Http\Controllers\Forum;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Block\Post;
use App\Models\Block\Thread;
use Carbon\Carbon;

class PostController extends BaseController
{
    /**
     * Return the model to use for this controller.
     *
     * @return Post
     */
    protected function model()
    {
        return new Post;
    }

    /**
     * Return the translation file name to use for this controller.
     *
     * @return string
     */
    protected function translationFile()
    {
        return 'posts';
    }

    /**
     * GET: Return an index of posts by thread ID.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function index(Request $request)
    {
        $this->validate($request, ['thread_id' => ['required']]);
        $posts = $this->model()->where('thread_id', $request->input('thread_id'))->get();
        return $this->response($posts);
    }

    /**
     * GET: Return a post.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function fetch($id, Request $request)
    {
        $post = $this->model()->find($id);
        if (is_null($post) || !$post->exists) return $this->notFoundResponse();
        return $this->response($post);
    }

    /**
     * POST: Create a new post.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['thread_id' => ['required'], 'author_id' => ['required'], 'content' => ['required']]);
        $thread = Thread::find($request->input('thread_id'));
        $this->authorize('reply', $thread);
        $post = $this->model()->create($request->only(['thread_id', 'post_id', 'author_id', 'content']));
        $post->load('thread');
        return $this->response($post, $this->trans('created'), 201);
    }

    /**
     * Edit forum for post
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit($block, $id)
    {
        $post = $this->model()->where('id', $id)->with('thread', 'thread.category', 'thread.player')->firstOrFail();
        if ($post->thread->category->block_id != $this->block->id) abort(404);
        if ($post->player->id != $this->player->id && !$this->player->isStaff()) {
            session()->flash('danger', 'You don\'t have permission to do this!');
            return redirect()->back();
        } else return view('blocks.forum.posts.edit', compact('post'));
    }

    /**
     * Edit a post and log action
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $block, $id)
    {
        $this->validate($request, [
            'reason' =>  'required|min:5',
            'content' => 'required'
        ]);
        $post = $this->model()->where('id', $id)->with('thread', 'thread.category', 'thread.player')->firstOrFail();
        if ($post->thread->category->block_id != $this->block->id) abort(404);
        if ($post->player->id != $this->player->id && !$this->player->isStaff()) {
            session()->flash('danger', 'You don\'t have permission to do this!');
            return redirect()->back();
        } else {
            $post->edited_by = $this->player->id;
            $post->edited_at = Carbon::now();
            $post->edited_content = $post->content;
            $post->edited_reason = $request->reason;
            $post->save();
            $post->update($request->all());
            session()->flash('success', 'You have updated a post!');
            return redirect('/thread/' . $post->thread->id . '#pid' . $post->id);
        }
    }
}
