<?php namespace App\Http\Controllers\Forum;

use App\Models\Block\BlockCategory;
use App\Models\Block\BlockGroup;
use App\Models\Character\Character;
use App\Models\Character\Table;
use App\Models\Player\ForumTag;
use App\Models\Player\Player;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Block\Category;
use App\Models\Block\Post;
use App\Models\Block\Thread;
use Carbon\Carbon;

class ThreadController extends BaseController
{
    /**
     * Return the model to use for this controller.
     *
     * @return Thread
     */
    protected function model()
    {
        return new Thread;
    }

    /**
     * Return the translation file name to use for this controller.
     *
     * @return string
     */
    protected function translationFile()
    {
        return 'threads';
    }

    /**
     * GET: return an index of threads by category ID.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function index(Request $request)
    {
        $this->validate($request, ['category_id' => ['required']]);

        $threads = $this->model()
            ->withRequestScopes($request)
            ->where('category_id', $request->input('category_id'))
            ->get();

        return $this->response($threads);
    }

    /**
     * GET: Return a thread by ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function fetch($block, $id, Request $request)
    {
        $block = $this->block;
        $thread = $this->model();
        $thread = $request->input('include_deleted') ? $thread->withTrashed()->find($id) : $thread->find($id);

        if (is_null($thread) || !$thread->exists) {
            return $this->notFoundResponse();
        }

        if ($thread->trashed()) {
            $this->authorize('delete', $thread);
        }

        if ($thread->category->private) {
            $this->authorize('view', $thread->category);
        }

        return view('blocks.forum.threads.show', compact('block', 'thread'));
    }

    /**
     * Show form to create a new thread
     * @param $block
     * @param $category
     * @param Request $request
     * @return mixed
     */
    public function newForm($block, $category, Request $request)
    {
        $block = $this->block;
        $category = Category::find($category);

        if (is_null($category) || !$category->exists || $category->block_id != $block->id) {
            return $this->notFoundResponse();
        }

        if (!$category->enable_threads) {
            $request->session()->flash('warning', 'You cannot make a thread here!');
            return redirect('/category/' . $category->id);
        }

        return view('blocks.forum.threads.create', compact('block', 'category'));
    }

    /**
     * Create new thread.
     * @param $block
     * @param $category
     * @param Request $request
     * @return mixed
     */
    public function store($block, $category, Request $request)
    {
        $this->validate($request, [
            'title'   => 'required',
            'content' => 'required'
        ]);

        $category = BlockCategory::find($category);
        if (!$category->enable_threads) {
            return $this->notFoundResponse();
        }

        // Save or preview thread
        if ($request->method == 'submit') {
            $thread = $this->makeThread($request, $category);
            $this->makePost($request, $thread);
            $request->session()->flash('success', 'You have made a new thread!');
            return redirect('/thread/' . $thread->id);
        } else {
            return $this->preview($category, $request);
        }
    }

    /**
     * Create a new thread entry
     * @param $data
     * @param $category
     * @return mixed
     */
    protected function makeThread($data, $category)
    {
        $thread = $this->model()->create([
            'category_id'  => $category->id,
            'title'        => $data['title'],
            'mature'       => $data['mature'],
            'private'      => $data['private'],
            'type'         => $data['type'],
            'character_id' => $data['character_id'],
            'author_id'    => auth()->guard('player')->user()->id,
            'reply_count'  => 1
        ]);

        $category->thread_count++;
        $category->save();

        // Tag players
        if ($data['tags']) $this->tagPlayers($data['tags'], $category, $thread, $data['character_id']);
        return $thread;
    }

    /**
     * Create a new post within the thread
     * @param $data
     * @param $thread
     * @return mixed
     */
    protected function makePost($data, $thread)
    {
        $post = Post::create([
            'thread_id'    => $thread->id,
            'author_id'    => auth()->guard('player')->user()->id,
            'content'      => $data['content'],
            'character_id' => $data['character_id'],
            'table_id'     => $data['table_id']
        ]);

        $thread->reply_count++;
        $thread->save();
        $thread->category->post_count++;
        $thread->category->save();

        if ($post->character_id) {
            $this->helper->addPost('ic', $post->character_id, $post->id);
            $this->helper->tag('ic', $this->player->id, $post->character_id, $thread->id);
        } else {
            $this->helper->addPost('ooc', $post->author_id, $post->id);
        }

        return $post;
    }

    /**
     * Tag proper entities
     * @param $tags
     * @param $category
     * @param $thread
     * @return mixed
     */
    protected function tagPlayers($tags, $category, $thread, $character)
    {
        foreach ($tags as $tag) {
            if ($category->type == 'ooc') {
                if ($tag == 'staff') { // Tag all staff members
                    $staffers = Player::where('role', 'STAFF')->orWhere('role', 'ADMIN')->get();
                    foreach ($staffers as $staff) $this->helper->tag('ooc', $staff->id, 0, $thread->id, true, auth()->guard('player')->user()->id);
                } else { // Otherwise tag individual player
                    $player = Player::find($tag);
                    if (!$player) {
                        $this->request->session()->flash('danger', 'One or more of the players you tagged was not found! Please check your spelling and be sure you are tagging a player and not a character.');
                        return redirect()->back();
                    }
                    $this->helper->tag('ooc', $player->id, 0, $thread->id, true, auth()->guard('player')->user()->id);
                }
            } else {
                if (substr($tag, 0, 1) == 'g') {
                    $group = BlockGroup::find(substr($tag, 1));
                    //TODO: tag all members in group
                } else {
                    $character = Character::find($tag);
                    if (!$character) {
                        $this->request->session()->flash('danger', 'One or more of the characters you tagged was not found! Please check your spelling and be sure you are tagging a character and not a player.');
                        return redirect()->back();
                    }
                    $this->helper->tag('ic', $character->player->id, $character->id, $thread->id, true, auth()->guard('player')->user()->id);
                }
            }
        }

        // Tag author
        if ($category->type == 'ooc') $this->helper->tag('ooc', auth()->guard('player')->user()->id, 0, $thread->id);
        else $this->helper->tag('ic', auth()->guard('player')->user()->id, $character, $thread->id);
        return true;
    }

    /**
     * Quick reply to a thread
     * @param $block
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function quickReply($block, $id, Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $thread = Thread::findOrFail($id);
        if (!$thread->locked) {
            $post = $this->makePost($request, $thread);
            $this->helper->notifyTagged($thread->id, $post->id);
            $request->session()->flash('success', 'You have replied to ' . $thread->title . '!');
            return redirect('/thread/' . $thread->id . '#pid' . $post->id);
        } else {
            $request->session()->flash('danger', 'This thread is locked!');
            return redirect('/thread/' . $thread->id);
        }
    }

    /**
     * Previews a thread before posting
     * @param $category
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function preview($category, $data)
    {
        $character = NULL;
        $table = NULL;
        if ($data['character_id']) $character = Character::findOrFail($data['character_id']);
        if ($data['table_id']) $table = Table::findOrFail($data['table_id']);
        return view('blocks.forum.threads.preview', compact('category', 'data', 'character', 'table'));
    }

    /**
     * Previews a post before posting
     * @param $thread
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function previewReply($thread, $data)
    {
        $character = NULL;
        $table = NULL;
        if ($data['character_id']) $character = Character::findOrFail($data['character_id']);
        if ($data['table_id']) $table = Table::findOrFail($data['table_id']);
        return view('blocks.forum.posts.preview', compact('thread', 'data', 'character', 'table'));
    }

    /**
     * Form to create a reply
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function replyForm($block, $id)
    {
        $block = $this->block;
        $thread = Thread::findOrFail($id);

        if ($thread->locked) {
            $this->request->session()->flash('danger', 'This thread is locked!');
            return redirect('/thread/' . $thread->id);
        }

        // TODO: check if thread is private and whether player can post.

        return view('blocks.forum.posts.create', compact('block', 'thread'));
    }

    /**
     * Submit a reply
     * @param $block
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reply($block, $id, Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $thread = Thread::findOrFail($id);

        if ($request->method == 'submit') {
            if (!$thread->locked) {
                $post = $this->makePost($request, $thread);
                $this->helper->notifyTagged($thread->id, $post->id);
                $request->session()->flash('success', 'You have replied to ' . $thread->title . '!');
                return redirect('/thread/' . $thread->id . '#pid' . $post->id);
            } else {
                $request->session()->flash('danger', 'This thread is locked!');
                return redirect('/thread/' . $thread->id);
            }
        } else {
            return $this->previewReply($thread, $request);
        }
    }

    /**
     * Form to exit a thread
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function exitForm($block, $id)
    {
        $thread = Thread::findOrFail($id);
        if ($thread->category->block_id != $this->block->id) abort(404);
        $tags = ForumTag::where('tagged_id', $this->player->id)->where('thread_id', $id)->with('character')->get();
        if (!$tags) return redirect('/thread/' . $id);
        return view('blocks.forum.threads.exit', compact('tags', 'thread'));
    }

    /**
     * Exit a character from a thread
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function exit(Request $request, $block, $id)
    {
        $thread = Thread::findOrFail($id);
        if ($thread->category->block_id != $this->block->id) abort(404);
        $tag = ForumTag::findOrFail($request->tag);
        $character = Character::where('id', $tag->character_id)->where('player_id', $this->player->id)->firstOrFail();
        // TODO: Redeem achievements and tasks and stuff.
        // Check if a thread has remaining tags and archive it if it doesn't
        $tagCount = ForumTag::where('thread_id', $thread->id)->count();
        if ($tagCount <= 1) $this->archive($thread->id);
        if ($this->block->settings->enable_currency && $this->block->settings->currency_on_thread_exit) {
            $this->helper->changePlayerCurrency($this->player->id, $this->block->settings->currency_on_thread_exit,
                'Exited Thread: ' . $thread->title, $character->id, '/thread/' . $thread->id);
        }
        $tag->delete();
        $request->session()->flash('success', $character->name . ' has exited from ' . $thread->title);
        return redirect('/thread/' . $thread->id);
    }

    /**
     * Archive a thread
     * @param $threadId
     * @return bool
     */
    protected function archive($threadId)
    {
        $thread = Thread::find($threadId);
        if ($thread) {
            $thread->archived = 1;
            $thread->locked = 1;
            $thread->archived_at = Carbon::now();
            $thread->save();

            // Remove post and thread count from category
            $thread->category->thread_count--;
            $thread->category->post_count -= $thread->post_count;
            $thread->category->save();
            return true;
        } else return false;
    }

    /**
     * PATCH: Restore a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function restore($id, Request $request)
    {
        $thread = $this->model()->withTrashed()->find($id);

        $this->authorize('deleteThreads', $thread->category);

        return parent::restore($id, $request);
    }

    /**
     * GET: Return an index of new/updated threads for the current user, optionally filtered by category ID.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function indexNew(Request $request)
    {
        $this->validate($request);

        $threads = $this->model()->recent();

        if ($request->has('category_id')) {
            $threads = $threads->where('category_id', $request->input('category_id'));
        }

        $threads = $threads->get();

        // If the user is logged in, filter the threads according to read status
        if (auth()->check()) {
            $threads = $threads->filter(function ($thread)
            {
                return $thread->userReadStatus;
            });
        }

        // Filter the threads according to the user's permissions
        $threads = $threads->filter(function ($thread)
        {
            return (!$thread->category->private || Gate::allows('view', $thread->category));
        });

        return $this->response($threads);
    }

    /**
     * PATCH: Mark the current user's new/updated threads as read, optionally limited by category ID.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function markNew(Request $request)
    {
        $this->authorize('markNewThreadsAsRead');

        $threads = $this->indexNew($request)->getOriginalContent();

        $primaryKey = auth()->user()->getKeyName();
        $userID = auth()->user()->{$primaryKey};

        $threads->transform(function ($thread) use ($userID)
        {
            return $thread->markAsRead($userID);
        });

        return $this->response($threads, $this->trans('marked_read'));
    }

    /**
     * PATCH: Move a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function move($id, Request $request)
    {
        $this->validate($request, ['category_id' => ['required']]);

        $thread = $this->model()->find($id);

        $category = Category::find($request->input('category_id'));

        if (!$category->threadsEnabled) {
            return $this->buildFailedValidationResponse($request, trans('forum::validation.category_threads_enabled'));
        }

        return $this->updateModel($thread, ['category_id' => $category->id], ['moveThreadsTo', $category]);
    }

    /**
     * PATCH: Lock a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function lock($id, Request $request)
    {
        $thread = $this->model()->where('locked', 0)->find($id);

        $category = !is_null($thread) ? $thread->category : [];

        return $this->updateModel($thread, ['locked' => 1], ['lockThreads', $category]);
    }

    /**
     * PATCH: Unlock a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function unlock($id, Request $request)
    {
        $thread = $this->model()->where('locked', 1)->find($id);

        $category = !is_null($thread) ? $thread->category : [];

        return $this->updateModel($thread, ['locked' => 0], ['lockThreads', $category]);
    }

    /**
     * PATCH: Pin a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function pin($id, Request $request)
    {
        $thread = $this->model()->where('pinned', 0)->find($id);

        $category = !is_null($thread) ? $thread->category : [];

        return $this->updateModel($thread, ['pinned' => 1], ['pinThreads', $category]);
    }

    /**
     * PATCH: Unpin a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function unpin($id, Request $request)
    {
        $thread = $this->model()->where('pinned', 1)->find($id);

        $category = ($thread) ? $thread->category : [];

        return $this->updateModel($thread, ['pinned' => 0], ['pinThreads', $category]);
    }

    /**
     * PATCH: Rename a thread.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function rename($id, Request $request)
    {
        $this->validate($request, ['title' => ['required']]);

        $thread = $this->model()->find($id);

        return $this->updateModel($thread, ['title' => $request->input('title')], 'rename');
    }

    /**
     * PATCH: Move threads in bulk.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function bulkMove(Request $request)
    {
        return $this->bulk($request, 'move', 'updated', $request->only('category_id'));
    }

    /**
     * PATCH: Lock threads in bulk.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function bulkLock(Request $request)
    {
        return $this->bulk($request, 'lock', 'updated');
    }

    /**
     * PATCH: Unlock threads in bulk.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function bulkUnlock(Request $request)
    {
        return $this->bulk($request, 'unlock', 'updated');
    }

    /**
     * PATCH: Pin threads in bulk.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function bulkPin(Request $request)
    {
        return $this->bulk($request, 'pin', 'updated');
    }

    /**
     * PATCH: Unpin threads in bulk.
     *
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function bulkUnpin(Request $request)
    {
        return $this->bulk($request, 'unpin', 'updated');
    }
}
