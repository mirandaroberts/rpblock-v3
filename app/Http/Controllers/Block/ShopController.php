<?php

namespace App\Http\Controllers\Block;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Block\Item;
use App\Models\Character;
use App\Models\Player;
use App\Models\Player\PlayerInventory;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Show site shop.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $items = Item::where('block_id', $this->block->id)->where('cost', '!=', 0)->orderBy('cost', 'asc')->get();
        return view('blocks.shop.index', compact('items'));
    }

    /**
     * Buy and item
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function buy(Request $request, $block, $id)
    {
        $character = null;
        $item = Item::findOrFail($id);
        $player = auth()->guard('player')->user();
        if ($request->character_id) $character = Character::findOrFail($request->character_id);
        if ($item->block_id == $this->block->id && $item->cost <= $player->currency) {
            $this->helper->changePlayerCurrency($player->id, -$item->cost, 'Purchased Item: ' . $item->name);
            if ($character) $this->helper->giveCharacterItem($character, $item);
            else $this->helper->givePlayerItem($player, $item);
            session()->flash('alert-success', 'You have purchased the ' . $item->name . '!');
            return redirect('/shop');
        } else {
            session()->flash('alert-danger', 'You need more ' . $this->block->settings->currency_name . ' to buy that!');
            return redirect('/shop');
        }
    }

    /**
     * Show a player's inventory
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function inventory($block, $user = null)
    {
        $player = auth()->guard('player')->user();
        if ($user) $p = Player::where('block_id', $this->block->id)->where('username', $user)->firstOrFail();
        else $p = $player;
        if ($p != $player && !$player->isStaff()) {
            session()->flash('alert-danger', 'You don\'t have permission to do this!');
            return redirect()->back();
        }
        $items = PlayerInventory::where('player_id', $p->id)->select('item_id', DB::raw('count(*) as total'))->groupBy('item_id')->get();
        return view('blocks.players.inventory', compact('p', 'items'));
    }
}