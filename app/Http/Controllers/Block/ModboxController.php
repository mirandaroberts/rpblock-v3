<?php

namespace App\Http\Controllers\Block;

use App\Models\Block\ModboxTicketComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Player\Player;
use App\Models\Character\Character;
use App\Models\Block\ModboxTicket;
use Carbon\Carbon;

class ModboxController extends Controller
{
    /**
     * Show
     */
    public function show()
    {
        if (auth()->guard('player')->user()) {
            $tickets = ModboxTicket::where('block_id', $this->block->id)->where('sender_id', '!=', auth()->guard('player')->user()->id)->where('closed_by', 0)->orderBy('created_at')->get();
        } else {
            $tickets = ModboxTicket::where('block_id', $this->block->id)->where('ip', '==', $_SERVER['REMOTE_ADDR'])->where('closed_by', 0)->orderBy('created_at')->get();
        }

        return view('blocks.modbox.show', compact('tickets'));
    }

    /**
     * Show Ticket
     */
    public function showTicket($block, $id)
    {
        $ticket = ModboxTicket::findOrFail($id);

        if ($this->validateUser($ticket)) {
            return view('blocks.modbox.ticket', compact('ticket'));
        } else {
            session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/modbox');
        }
    }

    /**
     * Check if user can view a ticket
     */
    public function validateUser($ticket)
    {
        $player = auth()->guard('player')->user();

        if ($player) {
            if ($player->isStaff() || $ticket->player->id == $player->id) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($player->isStaff() || $ticket->ip == $_SERVER['REMOTE_ADDR']) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Post comment
     */
    public function postComment($block, $id, Request $request)
    {
        $ticket = ModboxTicket::findOrFail($id);

        $this->validate($request, [
            'body'           => 'required',
        ]);

        if ($this->validateUser($ticket)) {
            ModboxTicketComment::create([
                'ticket_id'    => $ticket->id,
                'player_id'    => auth()->guard('player')->user()->id,
                'private'      => $request->private,
                'body'         => $request->body
            ]);

            if (!$request->private && auth()->guard('player')->user()->id != $ticket->sender_id) {
                $this->helper->makeNotice('info', 'A new comment has been added to your ticket ' . $ticket->title, '/modbox/ticket/' . $ticket->id, $ticket->player->id);
            }

            // Send notice to staff
            $roles = ['STAFF', 'ADMIN'];
            $staff = Player::where('id', '!=', auth()->guard('player')->user()->id)
                           ->where('id', '!=', $ticket->sender_id)
                           ->where('block_id', $this->block->id)
                           ->whereIn('role', $roles)
                           ->get();

            foreach ($staff as $s) {
                $this->helper->makeNotice('info', auth()->guard('player')->user()->username . ' has commented on ticket #' . $ticket->id, '/modbox/ticket/' . $ticket->id, $s->id);
            }

            $request->session()->flash('alert-success', 'You have posted a new comment.');

            return redirect('/modbox/ticket/' . $ticket->id);
        } else {
            session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/modbox');
        }
    }

    /**
     * Edit Comment
     */
    public function editComment($block, $id)
    {
        $comment = ModboxTicketComment::findOrFail($id);

        if ($comment->player_id != auth()->guard('player')->user()->id) {
            session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/modbox/ticket/' . $comment->ticket_id);
        }

        return view('blocks.modbox.edit', compact('comment'));
    }

    /**
     * Post edit comment
     */
    public function postEditComment($block, $id, Request $request) {
        $comment = ModboxTicketComment::findOrFail($id);

        if ($comment->player_id != auth()->guard('player')->user()->id) {
            session()->flash('alert-danger', 'You do not have permission to do this!');
        }

        $comment->update($request->all());
        session()->flash('alert-success', 'You have edited a ticket comment!');

        return redirect('/modbox/ticket/' . $comment->ticket_id);
    }

    /**
     * Delete Comment
     */
    public function deleteComment($block, $id)
    {
        $comment = ModboxTicketComment::findOrFail($id);

        if ($comment->player_id != auth()->guard('player')->user()->id) {
            session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/modbox/ticket/' . $comment->ticket_id);
        }

        return view('blocks.modbox.delete', compact('comment'));
    }

    /**
     * Post delete comment
     */
    public function postDeleteComment($block, $id, Request $request) {
        $comment = ModboxTicketComment::findOrFail($id);
        $url = '/modbox/ticket/' . $comment->ticket_id;

        if ($comment->player_id != auth()->guard('player')->user()->id) {
            session()->flash('alert-danger', 'You do not have permission to do this!');
        }

        $comment->delete();
        session()->flash('alert-success', 'You have deleted a ticket comment!');

        return redirect($url);
    }

    /**
     * Accept Ticket
     */
    public function accept($block, $id) {
        $ticket = ModboxTicket::findOrFail($id);
        $player = auth()->guard('player')->user();

        if ($player->id == $ticket->sender_id || !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this!');

            return redirect('/modbox/ticket/' . $ticket->id);
        }

        // switch through acceptable ticket types
        switch ($ticket->type) {
            case 'Character Audition':
                $ticket->reportedChar->accepted = 1;
                $ticket->reportedChar->save();

                $this->acceptTicket($ticket);

                $this->helper->makeNotice('success', 'Your character ' . $ticket->reportedChar->name . ' has been accepted!', '/character/' . $ticket->reportedChar->name, $ticket->reportedChar->player->id);
            break;
        }

        return redirect('/modbox/ticket/' . $ticket->id);
    }

    /**
     * Accept ticket
     */
    public function acceptTicket($ticket) {
        $player = auth()->guard('player')->user();

        $ticket->status = 'Accepted';
        $ticket->closed_by = $player->id;
        $ticket->closed_date = Carbon::now();
        $ticket->save();

        return true;
    }
}
