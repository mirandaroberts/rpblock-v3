<?php

namespace App\Http\Controllers\Block;

use App\Models\Character\Character;
use App\Models\Character\Table;
use App\Models\Player\Player;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TableController extends Controller
{
    /**
     * Shows a player's tables
     * @param $block
     * @param null $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index($block, $name = null)
    {
        $name ? $p = Player::where('block_id', $this->block->id)->where('username', $name)->firstOrFail() : $p = $this->player;
        if ($p != $this->player && !$this->player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this!');
            return redirect('/tables');
        } else {
            $tables = Table::where('player_id', $p->id)->orderBy('name', 'asc')->paginate();
            return view('blocks.tables.index', compact('p', 'tables'));
        }
    }

    /**
     * Show form to create table
     * @param $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($block)
    {
        return view('blocks.tables.create');
    }

    /**
     * Create a table
     * @param Request $request
     * @param $block
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request, $block)
    {
        $character = 0;
        $this->validate($request, [
            'name'           => 'required|max:100',
            'header'         => 'required',
            'footer'         => 'required',
            'pre'            => 'required',
            'post'           => 'required'
        ]);

        if ($request->character) {
            $character = Character::where('block_id', $this->block->id)->where('id', $request->character)->firstOrFail();
            if ($character && $character->player->id != $this->player->id) {
                session()->flash('alert-danger', 'You do not have permission to do this!');
                return redirect()->withInput()->back();
            }
        }

        $character ? $character = $character->id : $character = 0;
        $table = Table::create([
            'name'         => $request->name,
            'player_id'    => $this->player->id,
            'character_id' => $character,
            'header'       => $request->header,
            'footer'       => $request->footer,
            'pre'          => $request->pre,
            'post'         => $request->post
        ]);

        if ($table) {
            session()->flash('alert-success', 'You have made a new table!');
            return redirect('/tables');
        } else {
            session()->flash('alert-danger', 'There was an error creating this table!');
            return redirect()->withInput()->back();
        }
    }

    /**
     * Form to edit a table
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEdit($block, $id)
    {
        $table = Table::findOrFail($id);
        if ($table->player->block_id != $this->block->id) abort(404);
        if ($table->player_id != $this->player->id && !$this->player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this!');
            return redirect('/tables');
        } else return view('blocks.tables.edit', compact('table'));
    }

    /**
     * Edit a table
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $block, $id)
    {
        $table = Table::findOrFail($id);
        if ($table->player->block_id != $this->block->id) abort(404);
        $this->validate($request, [
            'name'           => 'required|max:100',
            'header'         => 'required',
            'footer'         => 'required',
            'pre'            => 'required',
            'post'           => 'required'
        ]);
        if ($table->player_id != $this->player->id && !$this->player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this!');
            return redirect('/tables');
        } else {
            $table->update($request->all());
            $table->save();
            session()->flash('alert-success', 'You have updated this table!');
            return redirect('/tables/edit/' . $table->id);
        }
    }
}