<?php

namespace App\Http\Controllers\Block;

use App\Models\Block\BlockGroupInvite;
use App\Models\Block\BlockGroupLeader;
use App\Models\Block\BlockGroupNews;
use App\Models\Block\BlockGroupRank;
use App\Models\Block\BlockGroupRule;
use Auth;
use App\Models\Block\BlockGroup;
use App\Models\Character\Character;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Show groups index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $groups = BlockGroup::where('block_id', $this->block->id)->with('ranks')->orderBy('name', 'asc')->get();
        return view('blocks.groups.index', compact('groups'));
    }

    /**
     * Show one group
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showOne($block, $id)
    {
        $invites = null;
        $group = BlockGroup::where('block_id', $this->block->id)->with('ranks')->where('id', $id)->firstOrFail();
        $groups = BlockGroup::where('block_id', $this->block->id)->with('relations')->where('id', '!=', $group->id)->orderBy('name', 'asc')->get();
        if ($player = auth()->guard('player')->user()) {
            $characters = array();
            foreach ($player->characters as $c) array_push($characters, $c->id);
            $invites = BlockGroupInvite::where('group_id', $group->id)->whereIn('character_id', $characters)->get();
        }
        return view('blocks.groups.show', compact('group', 'groups', 'invites'));
    }

    /**
     * Claim a group
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function claim(Request $request, $block, $id)
    {
        session()->flash('alert-danger', 'Cannot do this right now!');
        return redirect()->back();
        $group = BlockGroup::where('block_id', $this->block->id)->with('ranks')->where('id', $id)->firstOrFail();
        if (!$character = Character::find($request->character)) {
            session()->flash('alert-danger', 'Could not locate character. Please try again');
            return redirect('/groups/' . $group->id);
        }

        if (!$character->fitsRank($group->leader->id)) {
            session()->flash('alert-danger', 'This character cannot claim ' . $group->name . '.');
            return redirect('/groups/' . $group->id);
        }

        if ($this->awardLeadership($character, $group)) {
            if ($group->leader->challenge_amt && auth()->guard('player')->user()->currency < $group->leader->challenge_amt) $this->helper->changePlayerCurrency($character->player->id, -$group->leader->challenge_amt, $character->name . ' challenged for or claimed ' . $group->name, $character->id);
            session()->flash('alert-success', $character->name . ' has claimed ' . $group->name);
        } else {
            session()->flash('alert-danger', 'There was a problem performing this action, please contact the block administrator.');
        }

        return redirect('/groups/' . $group->id);
    }

    /**
     * Grant a character leadership of a group
     * @param $character
     * @param $group
     * @return bool
     */
    protected function awardLeadership($character, $group)
    {
        $character->rank_id = $group->leader->id;
        $character->group_id = $group->id;
        $character->save();

        BlockGroupLeader::create([
            'character_id' => $character->id,
            'group_id' => $group->id,
            'current' => 1
        ]);
        return true;
    }

    /**
     * Show the edit rules page.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function rules($group, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRules($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            return view('blocks.groups.rules', compact('group'));
        }
    }

    /**
     * Create new rule
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function newRule(Request $request, $block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRules($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            $this->validate($request, ['rule' => 'required|max:255|min:10']);
            BlockGroupRule::create(['group_id' => $group->id, 'rule' => $request->rule]);
            session()->flash('alert-success', 'You have made a new rule.');
            return view('blocks.groups.rules', compact('group'));
        }
    }

    /**
     * Form to edit a rule
     * @param $block
     * @param $id
     * @param $rule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function editRule($block, $id, $rule)
    {
        $group = BlockGroup::findOrFail($id);
        $rule = BlockGroupRule::where('id', $rule)->where('group_id', $id)->firstOrFail();
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRules($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            return view('blocks.groups.rules_edit', compact('group', 'rule'));
        }
    }

    /**
     * @param Request $request
     * @param $block
     * @param $id
     * @param $rule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function postEditRule(Request $request, $block, $id, $rule)
    {
        $group = BlockGroup::findOrFail($id);
        $rule = BlockGroupRule::where('id', $rule)->where('group_id', $id)->firstOrFail();
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRules($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            $this->validate($request, ['rule' => 'required|max:255|min:10']);
            $rule->update($request->all());
            session()->flash('alert-success', 'You have updated a rule.');
            return view('blocks.groups.rules', compact('group'));
        }
    }

    /**
     * Show group news
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function news($block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $news = BlockGroupNews::where('group_id', $group->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('blocks.groups.news', compact('group', 'news'));
    }

    /**
     * Edit news form
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function editNews($block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupNews($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            return view('blocks.groups.news_edit', compact('group'));
        }
    }

    /**
     * Update news
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function postEditNews(Request $request, $block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRules($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            $this->validate($request, ['content' => 'required']);
            BlockGroupNews::create(['group_id' => $group->id, 'content' => $request->content]);
            $this->notifyGroupMembers($group, $group->name . '\'s news has been updated!');
            session()->flash('alert-success', 'You have updated the news.');
            return redirect('/groups/' . $group->id);
        }
    }

    /**
     * Send a notification to a group's members
     * @param $group
     * @param $message
     */
    protected function notifyGroupMembers($group, $message)
    {
        $members = Character::where('group_id', $group->id)->get();
        foreach ($members as $member) $this->helper->makeNotice('warning', $message, '/groups/' . $group->id, $member->player_id);
        return;
    }

    /**
     * Show rank edit.
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    protected function editRanks($block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        if (!$player->canEditGroupRanks($group->id) && !$player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            return view('blocks.groups.ranks', compact('group'));
        }
    }

    /**
     * Send an invite to a character
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function invite(Request $request, $block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        $character = Character::where('group_id', '!=', $group->id)->where('id', $request->invite_character)->first();
        if (!$player->canGroupInvite($group->id) && !$player->isStaff() || !$character) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            BlockGroupInvite::create([
                'group_id' => $group->id,
                'character_id' => $character->id,
                'invited_by' => $player->id
            ]);
            $this->helper->makeNotice('warning', $character->name . ' has been invited to join ' . $group->name, '/groups/' . $group->id, $character->player_id);
            session()->flash('alert-success', 'You have invited ' . $character->name . ' to join ' . $group->name . '.');
            return redirect('/groups/' . $group->id . '/ranks');
        }
    }

    /**
     * Accept a group invite
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptInvite($block, $id)
    {
        $player = auth()->guard('player')->user();
        $invite = BlockGroupInvite::findOrFail($id);
        if ($invite->character->player_id != $player->id) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $invite->group_id);
        } else {
            // Check if a character is allowed to leave their current group
            if ($invite->character->group_id && (!$invite->character->rank->can_leave || $invite->character->rank->prisoner)) {
                session()->flash('alert-danger', 'This character must first be released from their current ' . $this->block->settings->group_name . '.');
                return redirect('/groups/' . $invite->group_id);
            } else {
                // Sort character into appropriate rank.
                $rank = $invite->group->defaultRank;
                if (!$invite->character->fitsRank($rank->id)) {
                    $rank = null;
                    $otherRanks = BlockGroupRank::where('group_id', $invite->group_id)->where('weight', '<', $rank->weight)->get();
                    foreach ($otherRanks as $r) if ($invite->character->fitsRank($r->id)) {
                        $rank = $r;
                        break;
                    }
                }
                if ($rank) {
                    $invite->character->group_id = $invite->group_id;
                    $invite->character->rank_id = $rank->id;
                    $invite->character->save();
                    session()->flash('alert-success', $invite->character->name . ' has joined ' . $invite->group->name . '.');
                    $invite->delete();
                    return redirect('/groups/' . $invite->group_id);
                } else {
                    session()->flash('alert-danger', 'There was an error sorting this character, likely do to incorrect configuration by site admins.');
                    return redirect('/groups/' . $invite->group_id);
                }
            }
        }
    }

    /**
     * Decline invite
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function declineInvite($block, $id)
    {
        $player = auth()->guard('player')->user();
        $invite = BlockGroupInvite::findOrFail($id);
        if ($invite->character->player_id != $player->id) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $invite->group_id);
        } else {
            $invite->delete();
            session()->flash('alert-success', 'You have declined this invite.');
            return redirect('/groups/' . $invite->group_id);
        }
    }

    /**
     * Removes a character from group
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function remove(Request $request, $block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $character = Character::where('group_id', $group->id)->where('id', $request->character_remove)->first();
        $player = auth()->guard('player')->user();
        if ((!$player->canGroupRemove($group->id) && !$player->isStaff()) || !$character || $group->leader->id == $character->rank_id) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else {
            $character->group_id = 0;
            $character->rank_id = 0;
            $character->save();
            $this->helper->makeNotice('danger', $character->name . ' has been removed from ' . $group->name, '/groups/' . $group->id, $character->player_id);
            session()->flash('alert-success', 'You have removed ' . $character->name . '.');
            return redirect('/groups/' . $group->id . '/ranks');
        }
    }

    /**
     * Change a character's rank
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postChangeRank(Request $request, $block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $player = auth()->guard('player')->user();
        $character = Character::where('group_id', $group->id)->where('id', $request->character)->firstOrFail();
        $rank = BlockGroupRank::where('group_id', $group->id)->where('id', $request->rank_id)->firstOrFail();
        if ($character->rank->weight < $rank->weight) $method = 'demote';
        else $method = 'promote';
        if (($method == 'demote' && !$player->canGroupPromote($group->id))
            || ($method == 'promote' && !$player->canGroupDemote($group->id))
            || $rank->id == $group->leader->id
            || $character->rank->id == $group->leader->id) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/groups/' . $group->id);
        } else if (!$character->fitsRank($rank->id)) {
            session()->flash('alert-danger', 'This character does not belong in that rank.');
            return redirect('/groups/' . $group->id);
        } else {
            $character->rank_id = $rank->id;
            $character->save();
            $this->helper->makeNotice('danger', $character->name . ' has had their rank changed to ' . $rank->name(), '/groups/' . $group->id, $character->player_id);
            session()->flash('alert-success', 'Change successful.');
            return redirect('/groups/' . $group->id);
        }
    }
}