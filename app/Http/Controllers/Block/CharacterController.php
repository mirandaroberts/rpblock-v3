<?php

namespace App\Http\Controllers\Block;

use App\Models\Block\BlockCharacterAttribute;
use App\Models\Block\BlockCharacterTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Player\Player;
use App\Models\Character\Character;
use App\Models\Character\CharacterTrait;
use App\Models\Block\Block;
use App\Http\Classes\TimeHandler;
use App\Models\Character\CharacterInventory;

class CharacterController extends Controller
{
    /**
     * Show characters
     * @param $block
     * @param null $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($block, $name = NULL) {
        $block = Block::where('name', $block)->firstOrFail();
        if ($name) {
            $player = Player::where('username', $name)->where('block_id', $block->id)->firstOrFail();
            $characters = Character::where('player_id', $player->id)->orderBy('name', 'asc')->paginate(12);
        } else {
            $characters = Character::where('block_id', $block->id)->orderBy('name', 'asc')->paginate(12);
        }
        return view('blocks.characters.show', compact('characters', 'name'));
    }

    /**
     * @param $block
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile($block, $name) {
        $character = Character::where('name', $name)
            ->where('block_id', $this->block->id)
            ->with('traits', 'traits.data.attribute')
            ->firstOrFail();
        return view('blocks.characters.profile', compact('character'));
    }

    /**
     * Show create form
     */
    public function create($block, Request $request) {
        $player = auth()->guard('player')->user();
        $attributes = BlockCharacterAttribute::where('block_id', $this->block->id)->orderBy('name', 'asc')->get();
        if ($player->canMakeCharacter()) return view('blocks.characters.create', compact('attributes'));
        else {
            $request->session()->flash('alert-danger', 'Your account is either inactive or you do not have enough character slots to make a new character.');
            return redirect('/dashboard');
        }
    }

    /**
     * Get Traits
     * @param $block
     * @param Request $request
     * @return string
     */
    public function getTraits($block, Request $request) {
        $attribute = BlockCharacterAttribute::find($request->attr);
        $first = BlockCharacterTrait::where('attribute_id', $attribute->id)->orderBy('name', 'asc')->first();
        $traits = array();
        foreach ($attribute->traits as $t) {
            if ($t->cost) $traits[] = '<option value="' . $t->id . '">' . $t->name . ' (' . $t->cost . ' ' . $this->block->settings->currency_name . ')</option>';
            else $traits[] = '<option value="' . $t->id . '">' . $t->name . '</option>';
        }
        $traits = implode(' ', $traits);
        return json_encode(['traits' => $traits, 'attr' => $first->id]);

    }

    /**
     * Process character creation
     * @param $block
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate($block, Request $request) {
        if ($this->player->canMakeCharacter()) {
            $this->validate($request, [
                'name'           => 'required|max:30|alpha|unique:characters',
                'age'            => 'integer',
                'birth_month'    => 'required',
                'appearance'     => 'required|min:10',
                'personality'    => 'required|min:10'
            ]);
            if (!is_numeric($request->age)) {
                $request->session()->flash('alert-danger', 'Only enter numbers into the age field.');
                return redirect()->back();
            }
            $character = $this->make($request->all());
            if (!$character) {
                $request->session()->flash('alert-danger', 'There was an error creating this character, do you have enough ' . $this->block->settings->currency_name . ' and character slots?');
                return redirect()->back();
            }
            $ticketData = [
                'title'       => 'Character Audition: ' . $character->name,
                'type'        => 'Character Audition',
                'reported_id' => $character->id,
                'sender_id'   => $this->player->id,
                'body'        => '',
                'email'       => '',
                'url'         => '/character/' . $character->name
            ];
            $this->helper->makeModboxTicket($ticketData, $this->block->id);
            $request->session()->flash('alert-success', 'You have submitted this character for approval. Please allow 48 hours for staff to review your audition.');
            return redirect('/character/' . $character->name);
        } else {
            $request->session()->flash('alert-danger', 'Your account is either inactive or you do not have enough character slots to make a new character.');
            return redirect('/dashboard');
        }
    }

    /**
     * Make character
     * @param array $data
     * @return mixed
     */
    public function make(array $data) {
        $cost = $this->calculateCost($data);
        $age = $this->calculateAge($data);
        if (!$this->playerManager($data, $cost)) return false;
        $this->validateParents($data['mother'], $data['father']);
        $character = Character::create([
            'name'          => $data['name'],
            'surname'       => $data['surname'],
            'player_id'     => $this->player->id,
            'block_id'      => $this->block->id,
            'appearance'    => $data['appearance'],
            'personality'   => $data['personality'],
            'history'       => $data['history'],
            'relationships' => $data['relationships'],
            'months_old'    => $age,
            'birth_month'   => $data['birth_month'],
            'cost'          => $cost,
            'gender'        => $data['gender'],
            'pvp'           => $data['pvp'],
        ]);
        foreach ($data['attributes'] as $key => $attr) $this->makeTrait($attr, $character);
        if (isset($data['values'])) $this->setValues($character, $data['values']);
        return $character;
    }

    /**
     * Make a trait
     * @param $trait
     * @param $character
     * @return mixed
     */
    public function makeTrait($trait, $character)
    {
        return CharacterTrait::create([
            'character_id' => $character->id,
            'trait_id'     => $trait
        ]);
    }

    /**
     * Calculate a character's cost
     * @param array $data
     * @return int
     */
    public function calculateCost(array $data) {
        $cost = 0;
        // Add Trait costs
        $traits = $data['attributes'];
        foreach ($traits as $trait) {
            $t = BlockCharacterTrait::findOrFail($trait);
            $cost += $t->cost;
        }
        // Add age cost
        $age = $this->calculateAge($data);
        if ($age < $this->block->settings->min_free_age && !$data['mother'] && !$data['father']) {
            $cost += $this->block->settings->min_age_cost;
        }
        return $cost;
    }

    /**
     * Calculate a character's age
     */
    public function calculateAge(array $data) {
        $monthsOld = $data['age'] * 12;
        $bm = $data['birth_month'];
        $obj = new TimeHandler();
        $month = $obj->getMonthForCreation();
        if ($month > $bm && $monthsOld == 0) $age = ($month - $monthsOld) - $bm;
        else if ($month < $bm && $monthsOld == 0) $age = (12 - ($bm - $month)) + $monthsOld;
        else if ($month < $bm && $monthsOld > 0) $age = ($monthsOld - $month) + $bm;
        else $age = (12 + ($bm - $month)) + $monthsOld;
        if ($age >= $this->block->settings->maximum_age) {
            session()->flash('alert-danger', 'This character exceeds the max age.');
            return redirect()->withInput()->back();
        }
        return $age;
    }

    /**
     * Handles currency/slot unlocks
     * @param array $data
     * @param $cost
     * @return bool
     */
    public function playerManager(array $data, $cost)
    {
        if (!$this->subPoints($data, $cost)) return false;
        // Removes a character slot from the player
        if ($this->block->settings->character_slots) {
            $this->player->character_slots--;
            $this->player->save();
        }
        return true;
    }

    /**
     * Subtracts points for creation
     * @param array $data
     * @param $cost
     * @return bool
     */
    public function subPoints(array $data, $cost) {
        if ($cost > $this->player->currency) {
            session()->flash('alert-danger', 'You need more ' . $this->block->settings->currency_name . ' to do this!');
            return false;
        } else {
            $this->helper->changePlayerCurrency($this->player->id, -$cost, 'Creating Character ' . $data['name']);
            return true;
        }
    }

    /**
     * Subtracts points for editing
     * @param array $data
     * @param $cost
     * @return bool
     */
    public function subPointsEdit(array $data, $cost) {
        if ($cost < 0 && abs($cost) > $this->player->currency) return false;
        else $this->helper->changePlayerCurrency($this->player->id, $cost, 'Editing Character ' . $data['name']);
        return true;
    }

    /**
     * Validate parents
     */
    public function validateParents($mother, $father) {
        // TODO: Validate parents and send offspring request.
    }

    /**
     * Show edit form
     * @param $block
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($block, $name) {
        $player = auth()->guard('player')->user();
        $character = Character::where('name', $name)->firstOrFail();
        $attributes = BlockCharacterAttribute::where('block_id', $this->block->id)->orderBy('name', 'asc')->get();
        if ($character->player->id == $player->id || $player->isStaff()) {
            return view('blocks.characters.edit', compact('character', 'attributes'));
        } else {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/character/' . $character->name);
        }
    }

    /**
     * Update a character
     * @param $block
     * @param $name
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit($block, $name, Request $request) {
        $character = Character::where('name', $name)->firstOrFail();
        $this->validate($request, [
            'name'           => 'required|max:30|alpha',
            'appearance'     => 'required|min:10',
            'personality'    => 'required|min:10'
        ]);
        if ($character->player->id == $this->player->id || $this->player->isStaff()) {
            if (!$character->accepted) {
                $cost = $this->calculateCost($request->all());
                $diff = $character->cost - $cost;
                if ($diff > 0) {
                    if (!$this->subPointsEdit($request->all(), $diff)) {
                        session()->flash('alert-danger', 'You need more ' . $this->block->settings->currency_name . ' to do this!');
                        return redirect()->withInput()->back();
                    }
                }
                $character->cost = $cost;
            }
            $character->update($request->all());
            $character->save();

            if (!$character->accepted) {
                $character->purgeTraits();
                foreach ($request->all()['attributes'] as $key => $attr) $this->makeTrait($attr, $character);
                if (isset($request->values)) $this->setValues($character, $request->values);
            }
        } else session()->flash('alert-danger', 'You do not have permission to do this.');
        return redirect('/character/' . $character->name);
    }

    /**
     * Assigns the correct value to a trait
     * @param $character
     * @param $values
     */
    protected function setValues($character, $values)
    {
        foreach ($values as $index => $value) {
            $used = array();
            foreach ($value as $val) {
                if ($val) {
                    $trait = CharacterTrait::where('character_id', $character->id)
                        ->where('trait_id', $index)->whereNotIn('id', $used)->first();
                    if ($trait && $trait->data->attribute->value) {
                        $used[] = $trait->id;
                        $trait->value = $val;
                        $trait->save();
                    }
                }
            }
        }
    }

    /**
     * Display a character's inventory
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	protected function getInventory($id) {
		$items = CharacterInventory::with('itemData')->where('character_id', $id)->get();
		return view('blocks.characters.inventory', compact('items'));
	}

}
