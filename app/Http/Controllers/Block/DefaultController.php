<?php

namespace App\Http\Controllers\Block;

use App\Http\Controllers\Controller;
use App\Models\Block\BlockCategory;
use App\Models\Block\Category;
use Illuminate\Http\Request;
use App\Mail\PlayerConfirm;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use App\Models\Block\BlockSkin;
use App\Models\Block\BlockGuidebookPage;
use App\Models\Character\Character;
use App\Models\Block\Thread;
use App\Models\Player\Player;

class DefaultController extends Controller
{
    /**
     * Show the block index, which must be initialized before showing.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        if ($this->block->initialized) {
            $categories = BlockCategory::where('block_id', $this->block->id)
                ->with('children', 'parent', 'threads', 'children.threads', 'children.children')
                ->get();
            return view('blocks.index', compact('categories'));
        } else {
            abort(404);
        }
    }

    /**
     * Return player dashboard
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        return view('blocks.dashboard');
    }

    /**
     * Shows the inactive page if the block is inactive or redirects to index if not.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function inactive()
    {
        if (!$this->block->active) {
            $block = $this->block;
            return view('blocks.inactive', compact('block'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Show activation page
     * @param $block
     * @param null $code
     * @return mixed
     */
    public function activate($block, $code = NULL)
    {
        if (auth()->guard('player')->check() && auth()->guard('player')->user()->confirm_token)
            return view('blocks.activate', compact('code'));
        else {
            return redirect('/');
        }
    }

    /**
     * Activates an account
     * @param $block
     * @param null $code
     * @param Request $request
     * @return mixed
     */
    public function postActivate($block, $code = NULL, Request $request)
    {
        $player = auth()->guard('player')->user();
        if ($code == $player->confirm_token && $request->email == $player->email) {
            $player->confirm_token = NULL;
            $player->save();

            $request->session()->flash('success', 'Thank you for activating your account! Be sure to read the guidebook before creating a character!');
            return redirect('/guidebook');
        } else {
            $request->session()->flash('danger', 'The code or email provided do not match our records!');
            return redirect('/activate');
        }
    }

    /**
     * Resend activation code.
     * @param Request $request
     * @return mixed
     */
    public function resendCode(Request $request)
    {
        $player = auth()->guard('player')->user();

        // Send them an email
        $mailData = [
            'block'   => $this->block->name,
            'code'    => $player->confirm_token
        ];

        Mail::to($player->email)->send(new PlayerConfirm($mailData));

        $request->session()->flash('success', 'We are sending you another email with your activation code!');
        return redirect('/activate');
    }

    /**
     * Change the user's active skin
     * @param Request $request
     * @return string
     */
    public function changeSkin(Request $request)
    {
        if ($request->skin == 0) {
            auth()->guard('player')->user()->skin_id = 0;
            auth()->guard('player')->user()->save();

            return json_encode('success');
        }

        $skin = BlockSkin::find($request->skin);

        if ($skin) {
            auth()->guard('player')->user()->skin_id = $skin->id;
            auth()->guard('player')->user()->save();

            return json_encode('');
        } else {
            return json_encode(['type' => 'error', 'message' => 'No skin found!']);
        }
    }

    /**
     * Search a block
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	protected function postSiteSearch(Request $request) {
		$q = $request->q;
		$results = new Collection();
		$block = $this->block->id;

		$characters = Character::where('block_id', $this->block->id)
            ->where('name', 'like', '%'.$q.'%')
            ->orWhere('surname', 'like', '%'.$q.'%')
            ->orderBy('name', 'asc')
            ->orderBy('surname', 'asc')
            ->get();
		$pages = BlockGuideBookPage::where('block_id', $this->block->id)
            ->where('name', 'like', '%'.$q.'%')
            ->orderBy('name', 'asc')
            ->get();
		$players = Player::where('block_id', $this->block->id)
            ->where('username', 'like', '%'.$q.'%')
            ->orderBy('username', 'asc')
            ->get();
		$threads = Thread::whereHas('category', function ($query) use ($block) {
                $query->where('block_id', $block);
            })
            ->where('title', 'like', '%'.$q.'%')
            ->orderBy('title', 'asc')
            ->get();

		$results = $results->merge($characters)->merge($pages)->merge($players)->merge($threads);
		return view('blocks.sitesearch', compact('results', 'q'));
	}

    /**
     * Show forum archives
     * @param $block
     * @param null $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function archives($block, $filter = null)
    {
        $block = $this->block;
        if ($filter) {
            $filter = Category::where('id', $filter)->where('block_id', $block->id)->firstOrFail();
            $archives = Thread::where('category_id', $filter->id)->where('archived', 1)->paginate();
        } else {
            $archives = Thread::whereHas('category', function ($query) use ($block) {
                $query->where('block_id', $block->id);
            })->where('archived', 1)->paginate();
        }
        $categories = Category::where('type', 'ic')->where('block_id', $block->id)->get();
        return view('blocks.forum.archives', compact('archives', 'categories', 'filter'));
    }
}