<?php

namespace App\Http\Controllers\Block;

use App\Models\Player\CurrencyLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Player\Player;
use App\Models\Character\Character;
use App\Models\Player\PlayerInventory;

class PlayerController extends Controller
{
    /**
     * Show all players
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $players = Player::where('block_id', $this->block->id)->orderBy('username', 'asc')->paginate(12);
        return view('blocks.players.show', compact('players'));
    }

    /**
     * Player profile
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile($block, $name) {
        $p = Player::where('username', $name)->where('block_id', $this->block->id)->firstOrFail();
        return view('blocks.players.profile', compact('p'));
    }

    /**
     * Show edit form
     * @param $block
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($block, $name) {
        $p = Player::where('username', $name)->where('block_id', $this->block->id)->firstOrFail();
        $player = auth()->guard('player')->user();
        if ($p->id == $player->id || $player->isStaff()) {
            return view('blocks.players.edit', compact('p'));
        } else {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/player/' . $p->username);
        }
    }

    /**
     * Post edit player
     *
     * @param $block
     * @param $name
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit($block, $name, Request $request) {
        $p = Player::where('username', $name)->where('block_id', $this->block->id)->first();
        $player = auth()->guard('player')->user();

        $this->validate($request, [
            'email'    => 'required|email|max:255'
        ]);

        if ($p->id == $player->id || $player->isStaff()) {
            $p->update($request->all());
            $p->save();
            session()->flash('alert-success', 'Success!');
        } else session()->flash('alert-danger', 'You do not have permission to do this.');
        return redirect('/player/' . $p->username);
    }

    /**
     * show currency log
     * @param $block
     * @param $name
     * @param $currency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currencyLog($block, $name, $currency)
    {
        if ($currency != $this->block->settings->currency_name) abort(404);
        $p = Player::where('username', $name)->where('block_id', $this->block->id)->firstOrFail();
        $logs = CurrencyLog::where('player_id', $p->id)->orderBy('created_at', 'desc')->paginate(20);
        return view('blocks.players.currencyLog', compact('p', 'logs'));
    }

    /**
     * Modify a player's currency
     * @param Request $request
     * @param $block
     * @param $name
     * @param $currency
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editCurrencyLog(Request $request, $block, $name, $currency)
    {
        $this->validate($request, [
            'amt'            => 'required|integer',
            'reason'         => 'required|max:255'
        ]);
        if ($currency != $this->block->settings->currency_name) abort(404);
        $p = Player::where('username', $name)->where('block_id', $this->block->id)->firstOrFail();
        $character = Character::where('name', $request->character)->where('block_id', $this->block->id)->first();
        $request->url ? $url = $request->url : $url = null;
        if (!$this->player->isStaff()) {
            session()->flash('alert-danger', 'You do not have permission to do this.');
            return redirect('/player/' . $p->username);
        }
        $this->helper->changePlayerCurrency($p->id, $request->amt, $request->reason, $character, $url);
        $this->helper->makeNotice('warning',
            'Your ' . $this->block->settings->currency_name . ' log has been updated',
            '/player/' . $p->username . '/points', $p->id);
        session()->flash('alert-success', 'Success.');
        return redirect('/player/' . $p->username . '/points');
    }

	/**
	* Display the specified player's inventory.
	* @return view
	*/
	protected function getInventory() {
		$items = PlayerInventory::with('itemData')->where('player_id', auth()->guard('player')->user()->id)->get();
		return view('blocks.players.inventory', compact('items'));
	}

    /**
     * Show settings page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	protected function settings()
    {
        return redirect('/player/' . $this->player->username . '/edit');
    }
}
