<?php

namespace App\Http\Controllers\Block;

use App\Models\Player\Conversation;
use App\Models\Player\Message;
use App\Models\Player\Player;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ConversationController extends Controller
{
    /**
     * Show inbox page
     * @param $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($block)
    {
        $conversations = Conversation::where('player1', $this->player->id)->orWhere('player1', $this->player->id)
            ->with('messages', 'receivingPlayer', 'sendingPlayer')->orderBy('updated_at', 'desc')->paginate();
        return view('blocks.conversations.index', compact('conversations'));
    }

    /**
     * Get conversation start page
     * @param $block
     * @param null $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSend($block, $username = null)
    {
        if ($username) $recipient = Player::where('block_id', $this->block->id)->where('username', $username)->firstOrFail();
        else $recipient = null;
        return view('blocks.conversations.send', compact('recipient'));
    }

    /**
     * Start a conversation
     * @param $block
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postSend($block, Request $request)
    {
        $this->validate($request, [
            'recipient' => 'required|exists:players,id',
            'subject'   => 'required|max:100',
            'content'   => 'required|max:10000'
        ]);

        $recipient = Player::find($request->recipient);
        $conversation = Conversation::create([
            'player1' => $this->player->id,
            'player2' => $recipient->id,
            'subject' => $request->subject
        ]);

        if ($conversation) {
            if ($this->createMessage($request->all(), $conversation)) {
                $request->session()->flash('success', 'You have started a conversation with ' . $recipient->username . '!');
                return redirect('/conversations');
            } else {
                $request->session()->flash('danger', 'There was an error creating your message!');
                return redirect()->withInput()->back();
            }
        } else {
            $request->session()->flash('danger', 'There was an error creating your conversation!');
            return redirect()->withInput()->back();
        }
    }

    /**
     * Create a new message
     * @param $data
     * @param $conversation
     * @return mixed
     */
    protected function createMessage($data, $conversation)
    {
        if ($message = Message::create([
            'conversation_id' => $conversation->id,
            'player_id'       => $this->player->id,
            'body'            => $data['content']])
        ) {
            $conversation->updated_at = Carbon::now();
            $conversation->save();
            // Establish who the recipient is and send them a notification
            $this->player->id != $conversation->player1 ? $recipient = $conversation->player1 : $recipient = $conversation->player2;
            $this->helper->makeNotice('info',
                'You have a new message from ' . $this->player->username . '!',
                '/conversations/' . $conversation->id,
                $recipient
            );
            return true;
        } else return false;
    }

    /**
     * Show a conversation.
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showConversation(Request $request, $block, $id)
    {
        $conversation = Conversation::findOrFail($id);
        $messages = Message::where('conversation_id', $id)->orderBy('created_at', 'asc')->paginate(10);
        if ($conversation->receivingPlayer->id != $this->player->id && $conversation->sendingPlayer->id != $this->player->id) {
            $request->session()->flash('danger', 'You do not have permission to do this!');
            return redirect('/conversations');
        } else return view('blocks.conversations.show', compact('conversation', 'messages'));
    }

    /**
     * Reply to a conversation
     * @param Request $request
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postMessage(Request $request, $block, $id)
    {
        $this->validate($request, [
            'content'   => 'required|max:10000'
        ]);
        $conversation = Conversation::findOrFail($id);
        if ($conversation->receivingPlayer->id != $this->player->id && $conversation->sendingPlayer->id != $this->player->id) {
            $request->session()->flash('danger', 'You do not have permission to do this!');
            return redirect('/conversations');
        } else {
            if ($this->createMessage($request->only('content'), $conversation)) {
                $request->session()->flash('success', 'You have sent a message!');
                return redirect('/conversations');
            } else {
                $request->session()->flash('danger', 'There was an error creating your message!');
                return redirect()->withInput()->back();
            }
        }
    }
}