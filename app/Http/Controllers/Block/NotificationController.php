<?php

namespace App\Http\Controllers\Block;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Player\Player;
use App\Models\Player\Notification;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function read($block, $id)
    {
        $notice = Notification::findOrFail($id);

        if ($notice->player->id != auth()->guard('player')->user()->id) {
            session()->flash('alert-danger', 'Oops! You have gotten here by mistake.');
            return redirect('/');
        }

        $notice->read = 1;
        $notice->read_at = Carbon::now();
        $notice->save();
        return redirect($notice->url);
    }

    public function show($block)
    {
        return view('blocks.notifications');
    }
}