<?php

namespace App\Http\Controllers\Block;

use App\Models\Block\BlockGuidebookPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuidebookController extends Controller
{
    /**
     * Show the guidebook.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($block, $page = NULL) {
        if ($page) {
            $page = BlockGuidebookPage::findOrFail($page);

            // Ensure page belongs to the block
            if ($page->block_id != $this->block->id) {
                abort(404);
            }

            return view('blocks.guidebook.page', compact('page'));
        } else {
            return view('blocks.guidebook.index');
        }
    }
}