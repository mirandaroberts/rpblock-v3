<?php

namespace App\Http\Controllers\Block;

use App\Models\Character\Character;
use App\Models\Player\ForumTag;
use App\Http\Controllers\Controller;
use App\Models\Block\Thread;
use App\Models\Player\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TagController extends Controller
{
    /**
     * Get tags for a player, character, or thread
     * @param $block
     * @param $method
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($block, $method, $id)
    {
        $tagged = NULL;
        $thread = NULL;

        switch ($method) {
            case 'player':
                $tagged = Player::findOrFail($id);
                break;
            case 'character':
                $tagged = Character::findOrFail($id);
                break;
            case 'thread':
                $thread = Thread::findOrFail($id);
                break;
        }

        return view('blocks.tags', compact('tagged', 'thread', 'method'));
    }

    /**
     * Allows players to tag themselves in ooc threads
     * @param $block
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function tagInto($block, $id)
    {
        $tagged = false;
        $thread = Thread::where('id', $id)->with('tags', 'category')->firstOrFail();
        if ($thread->category->block_id != $this->block->id) abort(404);
        foreach ($thread->tags as $tag) if ($tag->player == $this->player) $tagged = true;
        if ($tagged) {
            session()->flash('alert-danger', 'You are already tagged in this thread.');
            return redirect()->back();
        } else {
            $this->helper->tag('ooc', $this->player->id, 0, $thread->id);
            session()->flash('alert-success', 'You have tagged yourself in ' . $thread->title . '.');
            return redirect()->back();
        }
    }
}