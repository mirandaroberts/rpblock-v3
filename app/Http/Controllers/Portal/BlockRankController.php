<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockGroupRank;
use App\Models\Block\BlockGroup;
use App\Models\Block\Block;

class BlockRankController extends Controller
{
    /**
     * Show
     */
    public function show($id) {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($group->block_id);

        return view('portal.blocks.ranks.show', compact('group', 'block'));
    }


    /**
     * create form
     */
    public function create($id) {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($group->block_id);

        return view('portal.blocks.ranks.create', compact('block', 'group'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($group->block_id);

        $this->validate($request, [
            'name_male'            => 'required_without:name_female|max:20',
            'name_female'          => 'required_without:name_male|max:20',
        ]);

        if ($request->leader) {
            $this->validateLeader($request, $group);
        }

        if ($request->default) {
            $this->validateDefault($request, $group);
        }

        $this->make($request->all(), $group);

        $request->session()->flash('alert-success', 'You have created a new ' . $block->settings->rank_name . '!');

        return redirect(route('editRanks', ['id' => $group->id]));
    }

    /**
     * Leader Validation
     */
    public function validateLeader($request, $group)
    {
        foreach ($group->ranks as $rank) {
            if ($rank->leader) {
                $request->session()->flash('alert-danger', 'There is already a leader! Edit your existing leader instead of making a new one.');
                return redirect(route('editRank', ['id' => $rank->id, 'block' => $group->block_id]));
            }

            if ($request->coleader || $request->default) {
                $request->session()->flash('alert-danger', 'The leader cannot be default or a co-leader.');
                return redirect(route('editRanks', ['id' => $group->id]));
            }
        }

        return true;
    }

    /**
     * Leader Validation
     */
    public function validateDefault($request, $group)
    {
        foreach ($group->ranks as $rank) {
            if ($rank->default) {
                $request->session()->flash('alert-danger', 'There can only be one default.');
                return redirect(route('editRanks', ['id' => $group->id]));
            }
        }

        return true;
    }

    /**
     * Create constraints
     */
    public function make(array $data, $group)
    {
        return BlockGroupRank::create([
            'name_male'            => $data['name_male'],
            'name_female'          => $data['name_female'],
            'min_age'              => $data['min_age'],
            'max_age'              => $data['max_age'],
            'post_reqs'            => $data['post_reqs'],
            'posts_needed'         => $data['posts_needed'],
            'max_amount'           => $data['max_amount'],
            'leader'               => $data['leader'],
            'coleader'             => $data['coleader'],
            'default'              => $data['default'],
            'can_invite'           => $data['can_invite'],
            'can_remove'           => $data['can_remove'],
            'can_promote'          => $data['can_promote'],
            'can_demote'           => $data['can_demote'],
            'can_edit'             => $data['can_edit'],
            'can_update'           => $data['can_update'],
            'prisoner'             => $data['prisoner'],
            'group_id'             => $group->id
        ]);
    }

    /**
     * Edit form
     */
    public function update($block, $id)
    {
        $rank = BlockGroupRank::findOrFail($id);
        $group = BlockGroup::findOrFail($rank->group_id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.ranks.update', compact('rank', 'group', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($block, $id, Request $request)
    {
        $rank = BlockGroupRank::findOrFail($id);
        $group = BlockGroup::findOrFail($rank->group_id);
        $block = Block::findOrFail($block);

        $this->validate($request, [
            'name_male'            => 'required_without:name_female|max:20',
            'name_female'          => 'required_without:name_male|max:20',
        ]);

        if ($request->leader) {
            $this->validateLeader($request, $group);
        }

        if ($request->default) {
            $this->validateDefault($request, $group);
        }

        $rank->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a ' . $block->settings->rank_name . '!');

        return redirect(route('editRanks', ['id' => $rank->group_id]));
    }

    /**
     * Delete form
     */
    public function delete($block, $id)
    {
        $rank = BlockGroupRank::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.ranks.delete', compact('rank', 'block'));
    }

    /**
     * Delete action
     */
    public function postDelete($id, $block, Request $request)
    {
        $rank = BlockGroupRank::findOrFail($id);
        $group = BlockGroup::findOrFail($rank->group_id);

        $request->session()->flash('alert-success', 'You have deleted ' . $rank->name . '!');

        $rank->delete();

        return redirect(route('editRanks', ['id' => $group->id]));
    }

    /**
     * Reorder ranks
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reorder($id) {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($group->block_id);

        return view('portal.blocks.ranks.reorder', compact('group', 'block'));
    }

    /**
     * Process reorder ranks
     *
     * @param Request $request
     * @return string
     */
    public function processReorder(Request $request) {
        $group = BlockGroup::find($request->group);

        if (!$group) {
            $data = array('type' => 'error', 'message' => 'Error! Please refresh page and try again.');
            return json_encode($data);
        }

        // Set index for rank layering
        $index = 1; // Set as 1 because leader is always 0

        // Loop through coleaders
        foreach ($request->coleaders as $r) {
            if ($r > 0) {
                $check = BlockGroupRank::find($r);

                if ($check) {
                    $check->weight = $index;
                    $check->save();
                } else {
                    $data = array('type' => 'error', 'message' => 'Error! Could not find rank.');
                    return json_encode($data);
                }

                $index++;
            }
        }

        // Next through regular ranks
        foreach ($request->ranks as $r) {
            if ($r > 0) {
                $check = BlockGroupRank::find($r);

                if ($check) {
                    $check->weight = $index;
                    $check->save();
                } else {
                    $data = array('type' => 'error', 'message' => 'Error! Could not find rank.');
                    return json_encode($data);
                }

                $index++;
            }
        }

        return json_encode('true');
    }

}
