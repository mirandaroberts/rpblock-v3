<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockThreadType;
use App\Models\Block\Block;

class BlockThreadTypeController extends Controller
{
    /**
     * Show
     */
    public function show($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.threadType.show', compact('block'));
    }


    /**
     * create form
     */
    public function create($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.threadType.create', compact('block'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $block = Block::findOrFail($id);

        $this->validate($request, [
            'name'            => 'required|string|max:20',
            'acronym'         => 'required|string|max:3'
        ]);

        $this->make($request->all(), $block);

        $request->session()->flash('alert-success', 'You have created a new thread type!');

        return redirect(route('portalThreadTypes', ['id' => $block->id]));
    }

    /**
     * Create constraints
     */
    public function make(array $data, $block)
    {
        return BlockThreadType::create([
            'name'            => $data['name'],
            'acronym'         => $data['acronym'],
            'block_id'        => $block->id
        ]);
    }

    /**
     * Edit form
     */
    public function update($block, $id)
    {
        $type = BlockThreadType::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.threadType.update', compact('type', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($block, $id, Request $request)
    {
        $type = BlockThreadType::findOrFail($id);
        $block = Block::findOrFail($block);

        $this->validate($request, [
            'name'            => 'required|string|max:20',
            'acronym'         => 'required|string|max:3'
        ]);

        $type->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a thread type!');

        return redirect(route('portalThreadTypes', ['id' => $block->id]));
    }

    /**
     * Delete form
     */
    public function delete($id)
    {
        $type = BlockThreadType::findOrFail($id);

        return view('portal.blocks.threadType.delete', compact('type'));
    }

    /**
     * Delete action
     */
    public function postDelete($id, $block, Request $request)
    {
        $type = BlockThreadType::findOrFail($id);
        $block = Block::findOrFail($block);

        $request->session()->flash('alert-success', 'You have deleted ' . $type->name . '!');

        $type->delete();

        return redirect(route('portalThreadTypes', ['id' => $block->id]));
    }
}
