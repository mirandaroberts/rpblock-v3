<?php

namespace App\Http\Controllers\Portal;

use App\Models\Block\BlockSetting;
use App\Models\Block\BlockWidget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Block\Block;
use App\Models\Block\User;
use App\Models\Block\Plan;
use App\Models\Block\Widget;
use App\Models\Player\Player;

class BlockController extends Controller
{
    /**
     * Show a list of blocks.
     */
    public function getBlocks()
    {
        $blocks = Block::all();
        return view('portal.blocks.show', compact('blocks'));
    }

    /**
     * Get one block
     */
    public function getOneBlock($id)
    {
        $block = Block::findOrFail($id);
        $widgets = Widget::all();
        return view('portal.blocks.showOne', compact('block', 'widgets'));
    }

    /**
     * Initialize Block
     */
    public function initialize($id, Request $request)
    {
        $block = Block::findOrFail($id);

        // Update settings
        $settings = BlockSetting::where('block_id', $block->id)->first();
        if ($settings) {
            $settings->update($request->all());
            $widgets = BlockWidget::where('block_id', $block->id)->get();
            foreach ($widgets as $widget) $widget->delete();
        } else {
            $settings = new BlockSetting($request->all());
            $settings->block_id = $block->id;
            $settings->save();
        }

        // Enable any widgets
        foreach ($request->widget as $w) {
            $entry = new BlockWidget();
            $entry->widget_id = $w;
            $entry->block_id = $block->id;
            $entry->save();
        }

        // Set inactive or active
        if ($request->active) $block->active = 1;
        else $block->active = 0;

        if ($block->initialized == 0) $this->registerOwner($block);
        $block->initialized = 1;
        $block->save();
        return redirect('/portal/blocks/' . $block->id);
    }

    /**
     * Show form for creating a block
     */
    public function createBlock()
    {
        $users = User::all();
        $plans = Plan::all();
        return view('portal.blocks.create', compact('users', 'plans'));
    }

    /**
     * Create a block
     */
    public function postCreateBlock(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|string|unique:blocks|max:20',
            'user_id'  => 'required|integer',
            'plan_id'  => 'required|integer',
        ]);

        $this->create($request->all());
        $request->session()->flash('alert-success', 'You have created a new block!');
        return redirect('/portal/blocks');
    }

    /**
     * Create block constraints
     */
    public function create(array $data)
    {
        $block =  Block::create([
            'name'        => $data['name'],
            'user_id'     => $data['user_id'],
            'plan_id'     => $data['plan_id'],
            'last_paid'   => Carbon::now(),
            'active'      => 1,
        ]);

        return $block;
    }

    /**
     * Create the master user account
     * @param $block
     * @return mixed
     */
    public function registerOwner($block)
    {
        $user = User::findOrFail($block->user_id);

        $player = Player::create([
            'block_id'        => $block->id,
            'username'        => $user->username,
            'email'           => $user->email,
            'dob'             => '1980-01-01',
            'password'        => $user->password,
            'confirm_token'   => NULL,
            'joined_ip'       => $_SERVER['REMOTE_ADDR'],
            'last_ip'         => $_SERVER['REMOTE_ADDR'],
            'role'            => 'ADMIN',
            'active'          => 1,
            'currency'        => 0,
            'character_slots' => $block->settings->character_slots,
            'last_active'     => Carbon::now()
        ]);

        $this->helper->changePlayerCurrency($player->id, $block->settings->currency_on_join, 'Joining Bonus');
        return $player;
    }

    /**
     * Show form for updating a block
     */
    public function updateBlock($id)
    {
        $block = Block::findOrFail($id);
        $users = User::all();
        $plans = Plan::all();
        return view('portal.blocks.update', compact('block', 'users', 'plans'));
    }

    /**
     * Update a block
     */
    public function postUpdateBlock($id, request $request)
    {
        $block = Block::findOrFail($id);
        $this->validate($request, [
            'name'     => 'required|string|max:20',
            'user_id'  => 'required|integer',
            'plan_id'  => 'required|integer',
        ]);

        $block->update($request->all());
        $request->session()->flash('alert-success', 'You have updated ' . $block->name . '!');
        return redirect('/portal/blocks');
    }

    /**
     * Delete a user
     */
    public function deleteBlock($id)
    {
        $block = Block::findOrFail($id);
        return view('portal.blocks.delete', compact('block'));
    }

    /**
     * Process delete user
     */
    public function postDeleteBlock($id, Request $request)
    {
        $block = Block::findOrFail($id);
        $request->session()->flash('alert-success', 'You have deleted ' . $block->name . '!');
        $block->delete();
        return redirect('/portal/blocks');
    }
}
