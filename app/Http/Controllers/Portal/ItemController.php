<?php

namespace App\Http\Controllers\Portal;

use App\Models\Block\Block;
use App\Http\Controllers\Controller;
use App\Models\Block\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{

	/**
	* Display a list of items.
	*
	* @return view
	*/
	protected function getItems($id) {
        $block = Block::findOrFail($id);
		$items = Item::where('block_id', $block->id)->orderBy('name', 'asc')->get();
		return view('portal.items.index', compact('items', 'block'));
	}

	/**
	* Display the create item form.
	*
	* @return view
	*/
	protected function getCreateItem($id) {
        $block = Block::findOrFail($id);
		return view('portal.items.create', compact('block'));
	}

	/**
	* Create a new item.
	*
	* @return redirect
	*/
	protected function postCreateItem($id, Request $request) {
        $block = Block::findOrFail($id);
		$this->validate($request, [
			'type' => 'required',
			'icon' => 'required|string|max:255',
			'name' => 'required|string|max:255',
			'description' => 'required|string',
			'cost' => 'integer'
		]);
		$item = new Item($request->all());
		$item->block_id = $block->id;
		$item->save();
		session()->flash('alert-success', 'You have successfully created this item.');
		return redirect('portal/blocks/items/' . $block->id);
	}

	/**
	* Display the edit item form.
	*
	* @return view
	*/
	protected function getEditItem($id) {
		$item = Item::findOrFail($id);
		return view('portal.items.edit', compact('item'));
	}

	/**
	* Edit an existing item.
	*
	* @return redirect
	*/
	protected function postEditItem(Request $request, $id) {
        $item = Item::findOrFail($id);
		$this->validate($request, [
			'type' => 'required',
			'icon' => 'required|string|max:255',
			'name' => 'required|string|max:255',
			'description' => 'required|string',
			'cost' => 'integer'
		]);
		$item->update($request->all());
		session()->flash('alert-success', 'You have successfully edited this item.');
		return redirect('portal/blocks/items/' . $item->block_id);
	}

	/**
	* Display the delete item verification.
	*
	* @return view
	*/
	protected function getDeleteItem($id) {
		$item = Item::findOrFail($id);
		return view('portal.items.delete', compact('item'));
	}

	/**
	* Delete an existing item.
	*
	* @return redirect
	*/
	protected function postDeleteItem($id) {
		$item = Item::findOrFail($id);
		$block = $item->block_id;
		$item->delete();
		session()->flash('alert-success', 'You have successfully deleted this item.');
		return redirect('portal/blocks/items/'  . $block);
	}

}
