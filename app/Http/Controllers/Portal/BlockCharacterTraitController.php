<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockCharacterAttribute;
use App\Models\Block\BlockCharacterTrait;
use App\Models\Block\Block;

class BlockCharacterTraitController extends Controller
{
    /**
     * create trait form
     */
    public function createTrait($id) {
        $attribute = BlockCharacterAttribute::findOrFail($id);
        $block = Block::findOrFail($attribute->block_id);

        return view('portal.blocks.characterTraits.create', compact('attribute', 'block'));
    }

    /**
     * Create attribute
     */
    public function postCreateTrait($id, Request $request)
    {
        $attribute = BlockCharacterAttribute::findOrFail($id);
        $block = Block::findOrFail($attribute->block_id);

        $this->validate($request, [
            'name'            => 'required|string|max:50',
            'cost'            => 'integer'
        ]);

        $this->create($request->all(), $attribute);

        $request->session()->flash('alert-success', 'You have created a new trait!');

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }

    /**
     * Create constraints
     */
    public function create(array $data, $attribute)
    {
        return BlockCharacterTrait::create([
            'name'            => $data['name'],
            'cost'            => $data['cost'],
            'attribute_id'    => $attribute->id
        ]);
    }

    /**
     * Edit trait
     */
    public function updateTrait($block, $id)
    {
        $trait = BlockCharacterTrait::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.characterTraits.update', compact('trait', 'block'));
    }

    /**
     * Post edit trait
     */
    public function postUpdateTrait($block, $id, Request $request)
    {
        $trait = BlockCharacterTrait::findOrFail($id);
        $block = Block::findOrFail($block);

        $this->validate($request, [
            'name'            => 'required|string|max:50',
            'cost'            => 'integer'
        ]);

        $trait->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a trait!');

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }

    /**
     * Delete a trait
     */
    public function deleteTrait($id)
    {
        $trait = BlockCharacterTrait::findOrFail($id);

        return view('portal.blocks.characterTraits.delete', compact('trait'));
    }

    /**
     * Process delete trait
     */
    public function postDeleteTrait($id, Request $request)
    {
        $trait = BlockCharacterTrait::findOrFail($id);
        $block = Block::findOrFail($trait->attribute->block_id);

        $request->session()->flash('alert-success', 'You have deleted ' . $trait->name . '!');

        $trait->delete();

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }
}
