<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockSkin;
use App\Models\Block\Block;

class SkinController extends Controller
{
    /**
     * Show a list of skins.
     */
    public function show($block)
    {
        $block = Block::findOrFail($block);

        return view('portal.blocks.skins.show', compact('block'));
    }

    /**
     * Form for creating a new skin
     */
    public function create($block)
    {
        $block = Block::findOrFail($block);

        return view('portal.blocks.skins.create', compact('block'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $block = Block::findOrFail($id);
        $regex = '/^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$/';

        $this->validate($request, [
            'name'               => 'required|string|max:50',
            'bg_color'           => 'required',
            'font_color'         => 'required',
            'link_color'         => 'required',
            'link_hover'         => 'required',
            'link_visited'       => 'required',
            'link_active'        => 'required',
            'banner'             => 'required',
            'accent_color'       => 'required',
            'accent_font'        => 'required',
            'accent_dark'        => 'required',
            'secondary_color'    => 'required',
            'secondary_font'     => 'required',
            'category_color'     => 'required',
            'category_icon'      => 'required',
            'category_border'    => 'required',
            'navbar_links'       => 'required',
            'navbar_hover'       => 'required',
            'breadcrumbs'        => 'required',
            'breadcrumbs_border' => 'required',
            'breadcrumbs_links'  => 'required',
            'breadcrumbs_hover'  => 'required',
        ]);

        $skin = new BlockSkin($request->all());
        $skin->block_id = $block->id;
        $skin->save();

        $request->session()->flash('alert-success', 'You have created a skin!');

        return redirect(route('portalSkins', ['block' => $block->id]));
    }

    /**
     * Edit form
     */
    public function update($block, $id)
    {
        $skin = BlockSkin::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.skins.update', compact('skin', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($block, $id, Request $request)
    {
        $skin = BlockSkin::findOrFail($id);
        $block = Block::findOrFail($block);
        $this->validate($request, [
            'name'               => 'required|string|max:50',
            'bg_color'           => 'required',
            'font_color'         => 'required',
            'link_color'         => 'required',
            'link_hover'         => 'required',
            'link_visited'       => 'required',
            'link_active'        => 'required',
            'banner'             => 'required',
            'accent_color'       => 'required',
            'accent_font'        => 'required',
            'accent_dark'        => 'required',
            'secondary_color'    => 'required',
            'secondary_font'     => 'required',
            'category_color'     => 'required',
            'category_icon'      => 'required',
            'category_border'    => 'required',
            'navbar_links'       => 'required',
            'navbar_hover'       => 'required',
            'breadcrumbs'        => 'required',
            'breadcrumbs_border' => 'required',
            'breadcrumbs_links'  => 'required',
            'breadcrumbs_hover'  => 'required',
        ]);
        // If set as default, clear the other default first.
        if ($request->default) {
            $defaultSkin = BlockSkin::where('default', 1)->where('block_id', $block->id)->first();
            if ($defaultSkin) {
                $defaultSkin->default = 0;
                $defaultSkin->save();
            }
        }
        $skin->update($request->all());
        $request->session()->flash('alert-success', 'You have updated a Skin!');
        return redirect(route('editSkin', ['block' => $block->id, 'id' => $skin->id]));
    }

    /**
     * Delete form
     */
    public function delete($block, $id)
    {
        $skin = BlockSkin::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.skins.delete', compact('skin', 'block'));
    }

    /**
     * Delete action
     */
    public function postDelete($block, $id, Request $request)
    {
        $skin = BlockSkin::findOrFail($id);
        $block = Block::findOrFail($block);

        $request->session()->flash('alert-success', 'You have deleted ' . $skin->name . '!');

        $skin->delete();

        return redirect(route('portalSkins', ['id' => $block->id]));
    }
}
