<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\Widget;

class WidgetController extends Controller
{
    /**
     * Show a list of widgets.
     */
    public function getWidgets()
    {
        $widgets = Widget::all();

        return view('portal.widgets.show', compact('widgets'));
    }

    /**
     * Show form for creating a widget
     */
    public function createWidget()
    {
        return view('portal.widgets.create');
    }

    /**
     * Create a widget
     */
    public function postCreateWidget(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:225',
            'description' => 'required|string|max:255'
        ]);

        $this->create($request->all());

        $request->session()->flash('alert-success', 'You have created a new widget!');

        return redirect('/portal/widgets');
    }

    /**
     * Create widget constraints
     */
    public function create(array $data)
    {
        return Widget::create([
            'name'        => $data['name'],
            'description' => $data['description']
        ]);
    }

    /**
     * Show form for updating a widget
     */
    public function updateWidget($id)
    {
        $widget = Widget::findOrFail($id);

        return view('portal.widgets.update', compact('widget'));
    }

    /**
     * Update a widget
     */
    public function postUpdateWidget($id, request $request)
    {
        $widget = Widget::findOrFail($id);

        $this->validate($request, [
            'name'        => 'required|string|max:225',
            'description' => 'required|string|max:255'
        ]);

        $widget->update($request->all());

        $request->session()->flash('alert-success', 'You have updated ' . $widget->name . '!');

        return redirect('/portal/widgets');
    }

    /**
     * Delete a widget
     */
    public function deletePlan($id)
    {
        $widget = Widget::findOrFail($id);

        return view('portal.widgets.delete', compact('widget'));
    }

    /**
     * Process delete widget
     */
    public function postDeletePlan($id, Request $request)
    {
        $widget = Widget::findOrFail($id);

        $request->session()->flash('alert-success', 'You have deleted ' . $widget->name . '!');

        $widget->delete();

        return redirect('/portal/widgets');
    }
}
