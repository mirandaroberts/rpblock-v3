<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockGuidebookPage;
use App\Models\Block\Block;

class GuidebookController extends Controller
{
    /**
     * Show
     */
    public function show($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.guidebookPages.show', compact('block'));
    }


    /**
     * create form
     */
    public function create($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.guidebookPages.create', compact('block'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $block = Block::findOrFail($id);

        $this->validate($request, [
            'name'            => 'required|string|max:50',
            'body'            => 'required'
        ]);

        $this->make($request->all(), $block);

        $request->session()->flash('alert-success', 'You have created a new guidebook page!');

        return redirect(route('portalGuidebook', ['block' => $block->id]));
    }

    /**
     * Create constraints
     */
    private function make(array $data, $block)
    {
        $page =  BlockGuidebookPage::create([
            'name'            => $data['name'],
            'body'            => $data['body'],
            'block_id'        => $block->id
        ]);

        return $page;
    }

    /**
     * Edit form
     */
    public function update($block, $id)
    {
        $page = BlockGuidebookPage::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.guidebookPages.update', compact('page', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($block, $id, Request $request)
    {
        $page = BlockGuidebookPage::findOrFail($id);
        $block = Block::findOrFail($block);

        $this->validate($request, [
            'name'            => 'required|string|max:50',
            'body'            => 'required'
        ]);

        $page->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a guidebook page!');

        return redirect(route('editGuidebookPage', ['block' => $block->id, 'id' => $page->id]));
    }

    /**
     * Delete form
     */
    public function delete($id)
    {
        $page = BlockGuidebookPage::findOrFail($id);

        return view('portal.blocks.guidebookPages.delete', compact('page'));
    }

    /**
     * Delete action
     */
    public function postDelete($id, $block, Request $request)
    {
        $page = BlockGuidebookPage::findOrFail($id);
        $block = Block::findOrFail($block);

        $request->session()->flash('alert-success', 'You have deleted ' . $page->name . '!');

        $page->delete();

        return redirect(route('portalGuidebook', ['block' => $block->id]));
    }

    /**
     * Reorder
     * @param $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reorder($block) {
        $block = Block::findOrFail($block);

        return view('portal.blocks.guidebookPages.reorder', compact('block'));
    }

    /**
     * Process reorder
     * @param Request $request
     * @return string
     */
    public function processReorder(Request $request) {
        $block = Block::find($request->block);

        if (!$block) {
            $data = array('type' => 'error', 'message' => 'Error! Please refresh page and try again.');
            return json_encode($data);
        }

        // Set index for layering
        $index = 0;

        // Loop through items
        foreach ($request->pages as $r) {
            if ($r > 0) {
                $check = BlockGuidebookPage::find($r);

                if ($check) {
                    $check->weight = $index;
                    $check->save();
                } else {
                    $data = array('type' => 'error', 'message' => 'Error! Could not find page.');
                    return json_encode($data);
                }

                $index++;
            }
        }

        return json_encode('true');
    }
}
