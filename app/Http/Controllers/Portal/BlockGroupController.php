<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockGroup;
use App\Models\Block\BlockGroupRank;
use App\Models\Block\Block;

class BlockGroupController extends Controller
{
    /**
     * Show
     */
    public function show($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.groups.show', compact('block'));
    }


    /**
     * create form
     */
    public function create($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.groups.create', compact('block'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $block = Block::findOrFail($id);

        $this->validate($request, [
            'name'            => 'required|string|max:20',
            'hex'             => 'required|string|max:7'
        ]);

        $this->make($request->all(), $block);

        $request->session()->flash('alert-success', 'You have created a new group!');

        return redirect(route('portalGroups', ['id' => $block->id]));
    }

    /**
     * Create constraints
     */
    private function make(array $data, $block)
    {
        $group =  BlockGroup::create([
            'name'            => $data['name'],
            'hex'             => $data['hex'],
            'block_id'        => $block->id
        ]);

        if ($data['copy_ranks']) {
            $this->copyRanks($group, $data['copy_ranks']);
        }

        return $group;
    }

    /**
     * Copies the ranks from specified group and creates them for the new group.
     * @param $group
     * @param $copyFrom
     * @return bool
     */
    private function copyRanks($group, $copyFrom)
    {
        $parentRanks = BlockGroupRank::where('group_id', $copyFrom)->get();

        if ($parentRanks) {
            foreach ($parentRanks as $rank) {
                $newRank = $rank->replicate();
                $newRank->group_id = $group->id;
                $newRank->save();
            }

            return true;
        }

        return false;
    }

    /**
     * Edit form
     */
    public function update($block, $id)
    {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($block);

        return view('portal.blocks.groups.update', compact('group', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($block, $id, Request $request)
    {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($block);

        $this->validate($request, [
            'name'            => 'required|string|max:20',
            'hex'             => 'required|string|max:7'
        ]);

        $group->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a group!');

        return redirect(route('portalGroups', ['id' => $block->id]));
    }

    /**
     * Delete form
     */
    public function delete($id)
    {
        $group = BlockGroup::findOrFail($id);

        return view('portal.blocks.groups.delete', compact('group'));
    }

    /**
     * Delete action
     */
    public function postDelete($id, $block, Request $request)
    {
        $group = BlockGroup::findOrFail($id);
        $block = Block::findOrFail($block);

        $request->session()->flash('alert-success', 'You have deleted ' . $group->name . '!');

        $group->delete();

        return redirect(route('portalGroups', ['id' => $block->id]));
    }
}
