<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockCharacterAttribute;
use App\Models\Block\Block;

class BlockCharacterAttributeController extends Controller
{
    /**
     * Show attributes and traits.
     */
    public function show($id)
    {
        $block = Block::findOrFail($id);

        return view('portal.blocks.characterAttributes.show', compact('block'));
    }

    /**
     * create attribute form
     */
    public function createAttribute($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.characterAttributes.create', compact('block'));
    }

    /**
     * Create attribute
     */
    public function postCreateAttribute($id, Request $request)
    {
        $block = Block::findOrFail($id);

        $this->validate($request, [
            'name'            => 'required|string|max:50|unique_with:block_character_attributes,block_id,' . $block->name . '|not_in:age,gender',
            'fixed'           => 'integer',
            'multi_attribute' => 'integer',
            'value'           => 'integer',
        ]);

        $this->create($request->all(), $block);

        $request->session()->flash('alert-success', 'You have created a new attribute!');

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }

    /**
     * Create plan constraints
     */
    public function create(array $data, $block)
    {
        return BlockCharacterAttribute::create([
            'name'            => $data['name'],
            'fixed'           => $data['fixed'],
            'multi_attribute' => $data['multi_attribute'],
            'value'           => $data['value'],
            'block_id'        => $block->id
        ]);
    }

    /**
     * Edit attribute
     */
    public function updateAttribute($block, $id)
    {
        $block = Block::findOrFail($block);
        $attribute = BlockCharacterAttribute::findOrFail($id);

        return view('portal.blocks.characterAttributes.update', compact('block', 'attribute'));
    }

    /**
     * Post edit attribute
     */
    public function postUpdateAttribute($block, $id, Request $request)
    {
        $block = Block::findOrFail($block);
        $attribute = BlockCharacterAttribute::findOrFail($id);

        $this->validate($request, [
            'name'            => 'required|string|max:50|not_in:age,gender',
            'fixed'           => 'integer',
            'multi_attribute' => 'integer',
            'value'           => 'integer',
        ]);

        $attribute->update($request->all());

        $request->session()->flash('alert-success', 'You have updated an attribute!');

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }

    /**
     * Delete an attribute
     */
    public function deleteAttribute($block, $id)
    {
        $block = Block::findOrFail($block);
        $attribute = BlockCharacterAttribute::findOrFail($id);

        return view('portal.blocks.characterAttributes.delete', compact('block', 'attribute'));
    }

    /**
     * Process delete Attribute
     */
    public function postDeleteAttribute($block, $id, Request $request)
    {
        $block = Block::findOrFail($block);
        $attribute = BlockCharacterAttribute::findOrFail($id);

        $request->session()->flash('alert-success', 'You have deleted ' . $attribute->name . '!');

        $attribute->delete();

        return redirect(route('portalCharacterCreation', ['id' => $block->id]));
    }
}
