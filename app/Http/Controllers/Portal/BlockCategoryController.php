<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\BlockCategory;
use App\Models\Block\Block;

class BlockCategoryController extends Controller
{
    /**
     * Show
     */
    public function show($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.categories.show', compact('block'));
    }


    /**
     * create form
     */
    public function create($id) {
        $block = Block::findOrFail($id);
        $categories = BlockCategory::where('block_id', $block->id)->orderBy('weight', 'asc')->get();

        return view('portal.blocks.categories.create', compact('block', 'categories'));
    }

    /**
     * Create action
     */
    public function postCreate($id, Request $request)
    {
        $block = Block::findOrFail($id);

        $this->validate($request, [
            'title'            => 'required|string|max:225'
        ]);

        $this->make($request->all(), $block);

        $request->session()->flash('alert-success', 'You have created a new category!');

        return redirect(route('portalCategories', ['id' => $block->id]));
    }

    /**
     * Create constraints
     */
    public function make(array $data, $block)
    {
        return BlockCategory::create([
            'parent_id'        => $data['parent_id'],
            'block_id'         => $block->id,
            'title'            => $data['title'],
            'description'      => $data['description'],
            'type'             => $data['type'],
            'enable_threads'   => $data['enable_threads'],
            'private'          => $data['private'],
            'img'              => $data['img']
        ]);
    }

    /**
     * Edit form
     */
    public function update($id)
    {
        $category = BlockCategory::findOrFail($id);
        $block = Block::findOrFail($category->block_id);
        $categories = BlockCategory::where('block_id', $block->id)->orderBy('weight', 'asc')->get();

        return view('portal.blocks.categories.update', compact('category', 'categories', 'block'));
    }

    /**
     * edit action
     */
    public function postUpdate($id, Request $request)
    {
        $category = BlockCategory::findOrFail($id);
        $block = Block::findOrFail($category->block_id);

        $this->validate($request, [
            'title'            => 'required|string|max:225'
        ]);

        $category->update($request->all());

        $request->session()->flash('alert-success', 'You have updated a Category!');

        return redirect(route('portalCategories', ['id' => $block->id]));
    }

    /**
     * Delete form
     */
    public function delete($id)
    {
        $category = BlockCategory::findOrFail($id);
        $block = Block::findOrFail($category->block_id);

        return view('portal.blocks.categories.delete', compact('category', 'block'));
    }

    /**
     * Delete action
     */
    public function postDelete($id, Request $request)
    {
        $category = BlockCategory::findOrFail($id);
        $block = Block::findOrFail($category->group_id);

        $request->session()->flash('alert-success', 'You have deleted ' . $category->name . '!');

        $category->delete();

        return redirect(route('portalCategories', ['id' => $block->id]));
    }

    /**
     * Reorder ranks
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reorder($id) {
        $block = Block::findOrFail($id);

        return view('portal.blocks.categories.reorder', compact('block'));
    }

    /**
     * Process reorder ranks
     *
     * @param Request $request
     * @return string
     */
    public function processReorder(Request $request) {
        $block = Block::find($request->block);

        if (!$block) {
            $data = array('type' => 'error', 'message' => 'Error! Please refresh page and try again.');
            return json_encode($data);
        }

        // Set index for rank layering
        $index = 0;

        // Loop through coleaders
        foreach ($request->parents as $r) {
            if ($r > 0) {
                $check = BlockCategory::find($r);

                if ($check) {
                    $check->weight = $index;
                    $check->save();
                } else {
                    $data = array('type' => 'error', 'message' => 'Error! Could not find category.');
                    return json_encode($data);
                }

                $index++;
            }
        }

        // Next through regular ranks
        foreach ($request->children as $r) {
            if ($r > 0) {
                $check = BlockCategory::find($r);

                if ($check) {
                    $check->weight = $index;
                    $check->save();
                } else {
                    $data = array('type' => 'error', 'message' => 'Error! Could not find category.');
                    return json_encode($data);
                }

                $index++;
            }
        }

        return json_encode('true');
    }

}
