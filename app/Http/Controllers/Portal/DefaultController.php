<?php

namespace App\Http\Controllers\Portal;

use App\Models\Block\Block;
use App\Models\Character\Character;
use App\Models\Block\Post;
use App\Models\Block\Thread;
use App\Models\Player\Player;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefaultController extends Controller
{
    /**
     * Return portal dashboard
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $blockCount = Block::count();
        $playerCount = Player::count();
        $characterCount = Character::count();
        $femaleCount = Character::where('gender', 'female')->count();
        $maleCount = Character::where('gender', 'male')->count();
        $postCount = Post::count();
        return view('portal.dashboard', compact('blockCount', 'playerCount', 'characterCount',
            'femaleCount', 'maleCount', 'postCount'));
    }
}