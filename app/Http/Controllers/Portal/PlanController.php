<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Block\Plan;

class PlanController extends Controller
{
    /**
     * Show a list of plans.
     */
    public function getPlans()
    {
        $plans = Plan::all();

        return view('portal.plans.show', compact('plans'));
    }

    /**
     * Show form for creating a plan
     */
    public function createPlan()
    {
        return view('portal.plans.create');
    }

    /**
     * Create a plan
     */
    public function postCreatePlan(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required|string|max:225',
            'amt'          => 'required|integer',
            'players'      => 'required|integer',
            'characters'   => 'required|integer'
        ]);

        $this->create($request->all());

        $request->session()->flash('alert-success', 'You have created a new plan!');

        return redirect('/portal/plans');
    }

    /**
     * Create plan constraints
     */
    public function create(array $data)
    {
        return Plan::create([
            'name'       => $data['name'],
            'amt'        => $data['amt'],
            'players'    => $data['players'],
            'characters' => $data['characters']
        ]);
    }

    /**
     * Show form for updating a plan
     */
    public function updatePlan($id)
    {
        $plan = Plan::findOrFail($id);

        return view('portal.plans.update', compact('plan'));
    }

    /**
     * Update a plan
     */
    public function postUpdatePlan($id, request $request)
    {
        $plan = Plan::findOrFail($id);

        $this->validate($request, [
            'name'  => 'required|string|max:225',
            'amt'   => 'required|integer',
            'players'      => 'required|integer',
            'characters'   => 'required|integer'
        ]);

        $plan->update($request->all());

        $request->session()->flash('alert-success', 'You have updated ' . $plan->name . '!');

        return redirect('/portal/plans');
    }

    /**
     * Delete a plan
     */
    public function deletePlan($id)
    {
        $plan = Plan::findOrFail($id);

        return view('portal.plans.delete', compact('plan'));
    }

    /**
     * Process delete plan
     */
    public function postDeletePlan($id, Request $request)
    {
        $plan = Plan::findOrFail($id);

        $request->session()->flash('alert-success', 'You have deleted ' . $plan->name . '!');

        $plan->delete();

        return redirect('/portal/plans');
    }
}
