<?php

namespace App\Http\Controllers\Portal;

use App\Mail\UserInvite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\Block\User;

class UserController extends Controller
{
    /**
     * Show a list of users.
     */
    public function getUsers()
    {
        $users = User::all();
        return view('portal.users.show', compact('users'));
    }

    /**
     * Show form for creating a user
     */
    public function createUser()
    {
        return view('portal.users.create');
    }

    /**
     * Create a user
     */
    public function postCreateUser(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|string|email|unique:users|max:255',
            'username'  => 'required|string|max:20',
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'password'  => 'required|string|min:6|confirmed'
        ]);

        $this->create($request->all());
        $request->session()->flash('alert-success', 'You have created a new user!');
        return redirect('/portal/users');
    }

    /**
     * Create user constraints
     */
    public function create(array $data)
    {
        $user = User::create([
            'username'  => $data['username'],
            'email'     => $data['email'],
            'firstname' => $data['firstname'],
            'lastname'  => $data['lastname'],
            'password'  => bcrypt($data['password']),
            'role'      => 'ROLE_USER',
            'active'    => 1
        ]);

        // Send them an email
        Mail::to($data['email'])->send(new UserInvite());
        return $user;
    }

    /**
     * Show form for updating a user
     */
    public function updateUser($id)
    {
        $user = User::findOrFail($id);
        return view('portal.users.update', compact('user'));
    }

    /**
     * Update a user
     */
    public function postUpdateUser($id, request $request)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'email'     => 'required|string|email|max:255',
            'username'  => 'required|string|max:20',
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255'
        ]);

        $user->update($request->all());
        $request->session()->flash('alert-success', 'You have updated ' . $user->username . '!');
        return redirect('/portal/users');
    }

    /**
     * Delete a user
     */
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        return view('portal.users.delete', compact('user'));
    }

    /**
     * Process delete user
     */
    public function postDeleteUser($id, Request $request)
    {
        $user = User::findOrFail($id);
        $request->session()->flash('alert-success', 'You have deleted ' . $user->username . '!');
        $user->delete();
        return redirect('/portal/users');
    }
}
