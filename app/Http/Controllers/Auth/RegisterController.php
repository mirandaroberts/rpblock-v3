<?php

namespace App\Http\Controllers\Auth;

use App\Mail\PlayerConfirm;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Models\Player\Player;
use App\Http\Classes\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public $block;
    public $helper;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->helper = new Helpers();
        $this->block = $this->helper->getBlock();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegisterForm()
    {
        $block = $this->block;
        return view('blocks.auth.register', compact('block'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:20|unique_with:players,block_id',
            'email'    => 'required|string|email|max:255|unique_with:players,block_id',
            'dob'      => 'required|date|before:' . $this->block->settings->min_register_age . ' years ago',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new player instance after a valid registration.
     *
     * @param  array  $data
     * @return Player
     */
    protected function create(array $data)
    {
        $player = Player::create([
            'block_id'        => $this->block->id,
            'username'        => $data['username'],
            'email'           => $data['email'],
            'dob'             => $data['dob'],
            'password'        => bcrypt($data['password']),
            'confirm_token'   => md5(uniqid(rand(), true)),
            'joined_ip'       => $_SERVER['REMOTE_ADDR'],
            'last_ip'         => $_SERVER['REMOTE_ADDR'],
            'role'            => 'PLAYER',
            'active'          => 1,
            'character_slots' => $this->block->settings->character_slots,
            'last_active'     => Carbon::now()
        ]);

        // Send them an email
        $mailData = [
            'block'   => $this->block->name,
            'code'    => $player->confirm_token
        ];

        Mail::to($data['email'])->send(new PlayerConfirm($mailData));

        // Give them intro points
        if ($this->block->settings->enable_currency) {
            $this->helper->changePlayerCurrency($player->id, $this->block->settings->currency_on_join, 'Registration Bonus');
        }

        return $player;
    }
}
