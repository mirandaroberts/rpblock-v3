<?php

namespace App\Providers;

use App\Models\Block\User;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set public_html as our public folder
        $this->app->bind('path.public', function() {
            return base_path().DIRECTORY_SEPARATOR.'public_html';
        });

        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
