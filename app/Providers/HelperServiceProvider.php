<?php

namespace App\Providers;

use App\Events\NotificationMade;
use Carbon\Carbon;
use App\Models\Block\Block;
use App\Models\Player\Player;
use App\Models\Player\CurrencyLog;
use App\Models\Block\ModboxTicket;
use App\Models\Player\Notification;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path().'/Classes/*.php') as $filename){
            require_once($filename);
        }
    }
}
