<?php

namespace App\Models\Character;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = [
        'character_id', 'player_id', 'name', 'header', 'footer', 'pre', 'post'
    ];

    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character');
    }

    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player');
    }
}
