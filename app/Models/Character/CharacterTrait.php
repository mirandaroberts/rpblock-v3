<?php

namespace App\Models\Character;

use Illuminate\Database\Eloquent\Model;

class CharacterTrait extends Model
{
    protected $fillable = [
        'character_id', 'trait_id', 'value'
    ];

    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character');
    }

    public function data()
    {
        return $this->belongsTo('App\Models\Block\BlockCharacterTrait', 'trait_id');
    }
}
