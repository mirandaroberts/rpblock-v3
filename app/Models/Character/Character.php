<?php

namespace App\Models\Character;

use App\Models\Block\BlockGroupRank;
use App\Models\Block\BlockSetting;
use Carbon\Carbon;
use App\Models\Block\Post;
use Illuminate\Database\Eloquent\Model;
use App\Http\Classes\TimeHandler;

class Character extends Model
{
    protected $dates = ['last_post', 'pvp_updated_at'];
    protected $fillable = [
        'name', 'surname', 'player_id', 'block_id', 'cost', 'gender', 'birth_month', 'months_old', 'appearance',
        'personality', 'history', 'relationships', 'mother', 'father', 'pvp', 'inactive', 'icon'
    ];

    /**
     * Get the blocks record associated with the plan.
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player');
    }

    /**
     * Return character's full name
     */
    public function fullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * Get icon mutator - either return their icon or the default avatar.
     */
    public function getIconAttribute($value)
    {
        if ($value) {
            return $value;
        } else {
            return '/img/defaultavatar.jpg';
        }
    }

    /**
     * Character status
     */
    public function getActiveStatus()
    {
        if ($this->accepted) {
            if ($this->inactive) {
                return 'Inactive';
            } else {
                return 'Active';
            }
        } else {
            return 'Auditioning';
        }
    }

    /**
     * Character group name
     */
    public function groupName() {
        if ($this->group_id) {
            return ucfirst($this->group->name);
        } else {
            $settings = BlockSetting::where('block_id', $this->block_id)->first();
            return ucfirst($settings->default_group);
        }
    }

    /**
     * Character rank name
     */
    public function rankName() {
        if ($this->rank) {
            if ($this->gender == 'male') {
                return ucfirst($this->rank->name_male);
            } else {
                return ucfirst($this->rank->name_female);
            }
        } else {
            $settings = BlockSetting::where('block_id', $this->block_id)->first();
            return ucfirst($settings->default_rank);
        }
    }

    /**
     * Get character Rank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo('App\Models\Block\BlockGroupRank', 'rank_id');
    }

    /**
     * Get character Group
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group_id');
    }

    /**
     * Get age
     */
    public function age() {
        $time = new TimeHandler();
        $age = $time->getAge($this->months_old);

        return $age;
    }

    /**
     * Format pvp
     */
    public function pvpRank()
    {
        if ($this->pvp == "Closed") {
            return '<span style="color:rgb(88, 173, 83); font-size: 11px;">Closed PvP</span>';
        } else if ($this->pvp == "Optional") {
            return '<span style="color:rgb(176, 181, 79); font-size: 11px;">Optional PvP</span>';
        } else if ($this->pvp == 'Open') {
            return '<span style="color:rgb(203, 123, 28); font-size: 11px;">Open PvP</span>';
        } else {
            return '<span style="color:rgb(105, 11, 11); font-size: 11px;">Hardcore PvP</span>';
        }
    }

    /**
     * has many traits
     */
    public function traits() {
        return $this->hasMany('App\Models\Character\CharacterTrait');
    }

    /**
     * birth month attribute
     */
    public function birthMonthString() {
        return date("F", strtotime("2001-" . intval($this->birth_month) . "-01"));
    }

    /**
     * See if character has trait
     */
    public function hasTrait($id) {
        $trait = CharacterTrait::where('character_id', $this->id)->where('trait_id', $id)->first();

        if ($trait) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete traits
     */
    public function purgeTraits()
    {
        foreach ($this->traits as $trait) {
            $trait->delete();
        }

        return true;
    }

    /**
     * Get posts made this week
     * @return mixed
     */
    public function postsThisWeek()
    {
        $posts = Post::where('character_id', $this->id)->where('created_at', '>=', Carbon::now()->subDays(7));
        return $posts->count();
    }

    /**
     * Get a character's items.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Models\Character\CharacterInventory');
    }

    /**
     * Get posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Block\Post');
    }

    /**
     * Determine if character meets rank requirements
     * @param $id
     * @return bool
     */
    public function fitsRank($id)
    {
        $return = true;
        if (!$rank = BlockGroupRank::find($id)) $return = false;
        if (($rank->name_male && !$rank->name_female) && $this->gender == 'female') $return = false;
        if ((!$rank->name_male && $rank->name_female) && $this->gender == 'male') $return = false;
        if ($rank->post_reqs && $this->postsThisWeek() < $rank->post_reqs) $return = false;
        if ($rank->posts_needed && $this->posts->count() < $rank->posts_needed) $return = false;
        if ($rank->min_age && $this->months_old < $rank->min_age) $return = false;
        if ($rank->max_age && $this->months_old > $rank->max_age) $return = false;
        return $return;
    }
}