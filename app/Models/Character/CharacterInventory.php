<?php

namespace App\Models\Character;

use App\Models\Block\Item as Item;
use Illuminate\Database\Eloquent\Model;

class CharacterInventory extends Model
{

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'character_inventory';
	protected $fillable = ['character_id', 'item_id'];

    /**
     * Belongs to item data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function itemData()
    {
		return $this->belongsTo(Item::class, 'item_id');
	}

}
