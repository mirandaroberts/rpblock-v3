<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGuidebookPage extends Model
{
    protected $fillable = ['name', 'weight', 'body', 'block_id'];

    /**
     * Get the block that belongs too the settings.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }
}