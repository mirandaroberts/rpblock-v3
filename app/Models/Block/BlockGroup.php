<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroup extends Model
{
    protected $fillable = ['name', 'hex', 'block_id'];

    /**
     * Get the block that belongs too the settings.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * Get group's ranks
     */
    public function ranks()
    {
        return $this->hasMany('App\Models\Block\BlockGroupRank', 'group_id')->orderBy('weight', 'asc');
    }

    /**
     * Get Leader
     */
    public function leader()
    {
        return $this->hasOne('App\Models\Block\BlockGroupRank', 'group_id')->where('leader', 1);
    }

    /**
     * Get Default rank
     */
    public function defaultRank()
    {
        return $this->hasOne('App\Models\Block\BlockGroupRank', 'group_id')->where('default', 1);
    }

    /**
     * get male characters
     */
    public function males()
    {
        return $this->hasMany('App\Models\Character\Character', 'group_id')->where('gender', 'male');
    }

    /**
     * get female characters
     */
    public function females()
    {
        return $this->hasMany('App\Models\Character\Character', 'group_id')->where('gender', 'female');
    }

    /**
     * Get all characters
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function characters()
    {
        return $this->hasMany('App\Models\Character\Character', 'group_id');
    }

    /**
     * Get past leaders
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pastLeaders()
    {
        return $this->hasMany('App\Models\Block\BlockGroupLeader', 'group_id');
    }

    /**
     * Get current leader
     * @return mixed
     */
    public function currentLeader()
    {
        return $this->hasOne('App\Models\Block\BlockGroupLeader', 'group_id')->where('current', 1);
    }

    /**
     * Determine if group is open for claim
     * @return bool
     */
    public function isForClaim()
    {
        if ($this->currentLeader()->count() > 0) return true;
        return false;
    }

    /**
     * Return relations
     * @return mixed
     */
    public function relations()
    {
        return $this->hasMany('App\Models\Block\BlockGroupRelation', 'group1');
    }

    /**
     * Get groups relationship with another group
     * @param $id
     * @return string
     */
    public function relationWith($id)
    {
        $group = BlockGroupRelation::where('group1', $this->id)->where('group2', $id)->first();
        if ($group) return $group->relation;
        else return 'Unknown';
    }

    /**
     * Get most recent news entry
     * @return mixed
     */
    public function news()
    {
        return $this->hasOne('App\Models\Block\BlockGroupNews', 'group_id')->orderBy('created_at', 'desc');
    }

    /**
     * Get group rules
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rules()
    {
        return $this->hasMany('App\Models\Block\BlockGroupRule', 'group_id');
    }
}