<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockSkin extends Model
{
    protected $fillable = ['block_id', 'name', 'bg_color', 'font_color', 'link_color',
        'link_hover', 'link_visited', 'link_active', 'header_bg', 'banner', 'accent_color',
        'accent_font', 'accent_dark', 'secondary_color', 'secondary_font', 'category_color',
        'category_font', 'category_icon', 'category_border', 'navbar_links', 'navbar_hover',
        'breadcrumbs', 'breadcrumbs_border', 'breadcrumbs_links', 'breadcrumbs_hover', 'default'
    ];

    /**
     * Get the block that belongs too the settings.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }
}