<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class ModboxTicketComment extends Model
{
    protected $fillable = [
        'ticket_id', 'player_id', 'private', 'body'
    ];

    /**
     * belongs to a sender
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player', 'player_id');
    }

    /**
     * belongs to a ticket
     */
    public function ticket()
    {
        return $this->belongsTo('App\Models\Block\ModboxTicket', 'ticket_id');
    }
}