<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupInvite extends Model
{
    protected $fillable = ['group_id', 'character_id', 'invited_by'];

    public function invitedBy()
    {
        return $this->belongsTo('App\Models\Player\Player', 'invited_by');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group_id');
    }

    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character', 'character_id');
    }
}