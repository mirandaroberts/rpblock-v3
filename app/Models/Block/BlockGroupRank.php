<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupRank extends Model
{
    protected $fillable = ['name_male', 'name_female', 'group_id', 'weight', 'default', 'can_edit', 'can_update',
        'can_invite', 'can_remove', 'can_promote', 'can_demote', 'min_age', 'max_age', 'post_reqs', 'posts_needed',
        'max_amount', 'leader', 'coleader', 'can_be_challenged', 'prisoner', 'challenge_amt', 'can_leave'];

    /**
     * Get the group that belongs too the rank.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * Format the name
     * @return string
     */
    public function name()
    {
        if ($this->name_male && !$this->name_female) {
            $name = '<i class="fa fa-mars"></i> ' . $this->name_male;
        } else if (!$this->name_male && $this->name_female) {
            $name = '<i class="fa fa-venus"></i> ' . $this->name_female;
        } else if ($this->name_male == $this->name_female) {
            $name = '<i class="fa fa-venus-mars"></i> ' . $this->name_female;
        } else {
            $name = '<i class="fa fa-mars"></i> ' . $this->name_male . ' // <i class="fa fa-venus"></i> ' . $this->name_female;
        }


        return ucfirst($name);
    }

    /**
     * Get all characters
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function characters()
    {
        return $this->hasMany('App\Models\Character\Character', 'rank_id');
    }
}