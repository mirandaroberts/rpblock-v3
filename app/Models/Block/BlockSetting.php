<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockSetting extends Model
{
    protected $fillable = ['description', 'soft_deletes', 'display_trashed_posts', 'min_register_age',
        'days_until_archive', 'old_thread_threshold', 'pagination_categories', 'pagination_threads',
        'pagination_posts', 'days_until_inactive', 'enable_currency', 'allow_currency_transfer', 'currency_name',
        'currency_on_join', 'currency_on_ooc_post', 'currency_on_ic_post', 'currency_on_ooc_thread',
        'currency_on_ic_thread', 'currency_on_thread_exit', 'character_slots', 'character_slot_price',
        'character_slot_unlock', 'group_name', 'rank_name', 'default_group', 'default_rank', 'time_modifier',
        'min_free_age', 'min_age_cost', 'adult_age', 'maximum_age'
    ];

    /**
     * Get the block that belongs too the settings.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }
}