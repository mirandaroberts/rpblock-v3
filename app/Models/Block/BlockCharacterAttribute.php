<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockCharacterAttribute extends Model
{
    protected $fillable = [
        'name', 'block_id', 'fixed', 'multi_attribute', 'value'
    ];

    /**
     * belongs to a block
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * has many traits
     */
    public function traits()
    {
        return $this->hasMany('App\Models\Block\BlockCharacterTrait', 'attribute_id')->orderBy('cost', 'asc')->orderBy('name', 'asc');
    }
}