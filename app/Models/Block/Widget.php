<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Get the instance of use in blocks.
     */
    public function block()
    {
        return $this->hasMany('App\Models\Block\BlockWidget');
    }
}