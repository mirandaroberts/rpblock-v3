<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockCharacterTrait extends Model
{
    protected $fillable = [
        'name', 'attribute_id', 'cost'
    ];

    /**
     * Block belongs to an attribute
     */
    public function attribute()
    {
        return $this->belongsTo('App\Models\Block\BlockCharacterAttribute');
    }
}