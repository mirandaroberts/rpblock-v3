<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class ModboxTicket extends Model
{
    protected $dates = ['closed_date'];
    protected $fillable = [
        'title', 'type', 'status', 'sender_id', 'reported_id', 'email', 'ip', 'body', 'url', 'block_id'
    ];

    /**
     * belongs to a sender
     */
    public function player()
    {
        return $this->belongsTo('App\Models\player\Player', 'sender_id');
    }

    /**
     * belongs to a sender
     */
    public function reportedChar()
    {
        return $this->belongsTo('App\Models\Character\Character', 'reported_id');
    }

    /**
     * belongs to a sender
     */
    public function reportedPlayer()
    {
        return $this->belongsTo('App\Models\Player\Player', 'reported_id');
    }

    /**
     * get reported data
     */
    public function reported()
    {
        if ($this->type == 'Character Audition' || $this->type == 'Report Character') {
            return '<a href="/character/' . $this->reportedChar->name . '">' . $this->reportedChar->name . '</a>';
        } elseif ($this->type == 'Report Player') {
            return $this->reportedPlayer()->name;
        }
    }

    /**
     * get all comments
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Block\ModboxTicketComment', 'ticket_id');
    }

    /**
     * Get comments that are public only
     * @return mixed
     */
    public function privateComments()
    {
        return $this->hasMany('App\Models\Block\ModboxTicketComment', 'ticket_id')->where('private', 0);
    }
}