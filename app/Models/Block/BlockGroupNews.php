<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupNews extends Model
{
    protected $table = 'block_group_news';
    protected $fillable = ['group_id', 'character_id', 'content'];

    /**
     * Get the group that belongs too the rank.
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group_id');
    }

    /**
     * Get the group that belongs too the rank.
     */
    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character');
    }
}