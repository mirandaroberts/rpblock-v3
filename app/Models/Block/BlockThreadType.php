<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockThreadType extends Model
{
    protected $fillable = [
        'name', 'acronym', 'block_id'
    ];

    /**
     * Belongs to a block
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }
}