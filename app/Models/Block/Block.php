<?php

namespace App\Models\Block;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Block\BlockWidget;

class Block extends Model
{
    protected $fillable = [
        'name', 'user_id', 'plan_id'
    ];

    protected $dates = [
        'last_paid'
    ];

    /**
     * Block belongs to a user
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Block\User');
    }

    /**
     * has many players
     */
    public function players()
    {
        return $this->hasMany('App\Models\Player\Player', 'block_id');
    }

    /**
     * Get Online players
     * @return mixed
     */
    public function onlinePlayers()
    {
        return $this->hasMany('App\Models\Player\Player', 'block_id')->where('last_active', '>=', Carbon::now()->subMinutes(5));
    }

    /**
     * Get players on in the last 24 hours
     * @return mixed
     */
    public function recentPlayers()
    {
        return $this->hasMany('App\Models\Player\Player', 'block_id')->where('last_active', '>=', Carbon::now()->subHours(24));
    }

    /**
     * Get newest player
     * @return mixed
     */
    public function newestPlayer()
    {
        return $this->hasOne('App\Models\Player\Player', 'block_id')->orderBy('created_at', 'desc');
    }

    /**
     * has many characters
     */
    public function characters()
    {
        return $this->hasMany('App\Models\Character\Character', 'block_id');
    }

    /**
     * get male characters
     */
    public function males()
    {
        return $this->hasMany('App\Models\Character\Character', 'block_id')->where('gender', 'male');
    }

    /**
     * get female characters
     */
    public function females()
    {
        return $this->hasMany('App\Models\Character\Character', 'block_id')->where('gender', 'female');
    }

    /**
     * get characters that belong to a group
     */
    public function groupChars()
    {
        return $this->hasMany('App\Models\Character\Character', 'block_id')->where('group_id', '!=', 0);
    }

    /**
     * Block belongs to a plan
     */
    public function plan()
    {
        return $this->belongsTo('App\Models\Block\Plan');
    }

    /**
     * Get the settings record associated with the block.
     */
    public function settings()
    {
        return $this->hasOne('App\Models\Block\BlockSetting');
    }

    /**
     * Gets the widgets enabled on this block.
     */
    public function widgets()
    {
        return $this->hasMany('App\Models\Block\BlockWidget');
    }

    /**
     * Check if certain widget is enabled (by id)
     */
    public function hasWidget($id)
    {
        $widget = BlockWidget::where('block_id', $this->id)->where('widget_id', $id)->first();

        if ($widget) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets character attributes
     */
    public function characterAttributes()
    {
        return $this->hasMany('App\Models\Block\BlockCharacterAttribute', 'block_id');
    }

    /**
     * Get thread types
     */
    public function types()
    {
        return $this->hasMany('App\Models\Block\BlockThreadType', 'block_id');
    }

    /**
     * Get groups
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Block\BlockGroup', 'block_id');
    }

    /**
     * Get categories
     */
    public function categories()
    {
        return $this->hasMany('App\Models\Block\BlockCategory', 'block_id');
    }

    /**
     * Get categories
     */
    public function icCategories()
    {
        return $this->hasMany('App\Models\Block\BlockCategory', 'block_id')->where('type', 'ic');
    }

    /**
     * Get front page categories
     */
    public function mainCategories()
    {
        return $this->hasMany('App\Models\Block\BlockCategory', 'block_id')->where('parent_id', 0)->orderBy('weight', 'asc');
    }

    /**
     * get tickets
     */
    public function tickets()
    {
        return $this->hasMany('App\Models\Block\ModboxTicket');
    }

    /**
     * Get guidebook pages
     * @return mixed
     */
    public function guidebookPages()
    {
        return $this->hasMany('App\Models\Block\BlockGuidebookPage')->orderBy('weight', 'asc');
    }

    /**
     * Get Skins
     * @return mixed
     */
    public function skins()
    {
        return $this->hasMany('App\Models\Block\BlockSkin');
    }

    /**
     * Get default skin
     * @return mixed
     */
    public function defaultSkin()
    {
        return $this->hasOne('App\Models\Block\BlockSkin')->where('default', 1);
    }

    /**
     * Returns number of ic threads
     * @return int
     */
    public function threadCount()
    {
        $id = $this->id;
        $categories = $this->icCategories;
        $count = Thread::whereHas('category', function ($query) use ($id) {
            $query->where('block_id', $id);
        })->where('archived', 1)->count();
        foreach ($categories as $cat) $count += $cat->thread_count;
        return $count;
    }

    /**
     * Returns number of posts
     * @return int
     */
    public function postCount()
    {
        $id = $this->id;
        $categories = $this->icCategories;
        $archived = Thread::whereHas('category', function ($query) use ($id) {
            $query->where('block_id', $id);
        })->where('archived', 1)->get();
        $count = 0;
        foreach ($categories as $cat) $count += $cat->post_count;
        foreach ($archived as $a) $count += $a->reply_count;
        return $count;
    }

    public function fiveLatest()
    {
        $id = $this->id;
        return Thread::whereHas('category', function ($q) use ($id) {
            $q->where('type', 'ic');
            $q->where('block_id', $id);
        })->where('archived', 0)->orderBy('updated_at', 'desc')->take(5)->get();
    }
}