<?php

namespace App\Models\Block;

use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\Model;
use App\Models\Block\Thread;
use App\Models\Block\Category;

class BlockCategory extends Model
{
    protected $table = 'block_categories';
    protected $fillable = [
        'block_id', 'parent_id', 'title', 'img', 'type', 'description', 'weight', 'enable_threads', 'private'
    ];

    /**
     * Create a new category model instance.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->perPage = 15;
    }

    /**
     * belongs to a block
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * belongs to another category
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Block\BlockCategory');
    }

    /**
     * Has many children
     */
    public function children()
    {
        return $this->hasMany('App\Models\Block\BlockCategory', 'parent_id');
    }

    /**
     * Get ooc image icon
     * @return mixed|string
     */
    public function getImg()
    {
        if ($this->img) {
            return $this->img;
        } else {
            if ($this->type == 'ooc') {
                return 'file';
            } else {
                return 'http://via.placeholder.com/675x71';
            }
        }
    }

    /**
     * Relationship: Threads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        $withTrashed = Gate::allows('viewTrashedThreads');
        $query = $this->hasMany(Thread::class, 'category_id')->orderBy('created_at', 'desc');
        return $withTrashed ? $query->withTrashed() : $query;
    }

    /**
     * Last active thread
     * @return mixed
     */
    public function latestThread()
    {
        $array = [$this->id];
        $categories = $this->children;
        foreach ($categories as $c) $array[] = $c->id;
        return Thread::whereIn('category_id', $array)
            ->where('archived', 0)
            ->orderBy('updated_at', 'desc')
            ->first();
    }

    /**
     * Attribute: Newest thread.
     *
     * @return Thread
     */
    public function getNewestThreadAttribute()
    {
        return $this->threads()->where('archived', 0)->orderBy('created_at', 'desc')->first();
    }

    /**
     * Attribute: Paginated threads.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getThreadsPaginatedAttribute()
    {
        return $this->threads()->orderBy('pinned', 'desc')->orderBy('updated_at', 'desc')
            ->paginate($this->perPage);
    }

    /**
     * Attribute: New threads enabled.
     *
     * @return bool
     */
    public function getThreadsEnabledAttribute()
    {
        return $this->enable_threads;
    }

    /**
     * Count threads with subcatas
     * @return mixed
     */
    public function threadCount()
    {
        $count = $this->thread_count;
        foreach ($this->children as $cat) $count += $cat->thread_count;
        return $count;
    }

    /**
     * Count posts with subcatas
     * @return mixed
     */
    public function postCount()
    {
        $count = $this->post_count;
        foreach ($this->children as $cat) $count += $cat->post_count;
        return $count;
    }
}