<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name', 'amt', 'players', 'characters'
    ];

    /**
     * Get the blocks record associated with the plan.
     */
    public function blocks()
    {
        return $this->hasMany('App\Models\Block\Block');
    }
}