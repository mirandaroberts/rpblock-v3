<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockWidget extends Model
{
    protected $fillable = ['widget_id', 'block_id'];

    /**
     * Get the block that belongs too the settings.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * Get the widget dara.
     */
    public function widget()
    {
        return $this->belongsTo('App\Models\Block\Widget');
    }
}