<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupLeader extends Model
{
    protected $fillable = ['group_id', 'character_id', 'current', 'ended_at'];
    protected $dates = ['ended_at'];

    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group_id');
    }

    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character', 'character_id');
    }
}