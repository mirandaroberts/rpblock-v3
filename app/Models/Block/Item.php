<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'items';

	/**
	* The fields that should be mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['type', 'icon', 'name', 'description', 'cost'];

}
