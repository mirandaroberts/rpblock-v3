<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupRule extends Model
{
    protected $fillable = ['group_id', 'character_id', 'rule'];

    /**
     * Get the group that belongs too the rank.
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group_id');
    }

    /**
     * Get the group that belongs too the rank.
     */
    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character');
    }
}