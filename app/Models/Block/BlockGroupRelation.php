<?php

namespace App\Models\Block;

use Illuminate\Database\Eloquent\Model;

class BlockGroupRelation extends Model
{
    protected $fillable = ['group1', 'group2', 'relation'];

    public function firstGroup()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group1');
    }

    public function secondGroup()
    {
        return $this->belongsTo('App\Models\Block\BlockGroup', 'group2');
    }
}