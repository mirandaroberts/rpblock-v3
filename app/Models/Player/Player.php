<?php

namespace App\Models\Player;

use App\Models\Block\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Player extends Authenticatable
{
    use Notifiable;

    protected $guard = "player";
    protected $dates = ['last_active'];
    protected $fillable = [
        'username', 'email', 'password', 'block_id', 'role', 'dob', 'confirm_token', 'joined_ip', 'last_ip', 'active',
        'currency', 'character_slots', 'hidden', 'icon', 'bio', 'notes', 'title_unlocked', 'title',
        'vista_unlocked', 'vista'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the block record associated with the user.
     */
    public function block()
    {
        return $this->belongsTo('App\Models\Block\Block');
    }

    /**
     * Get icon mutator - either return their icon or the default avatar.
     */
    public function getIconAttribute($value)
    {
        if ($value) {
            return $value;
        } else {
            return '/img/defaultavatar.jpg';
        }
    }

    /**
     * has many characters
     */
    public function characters()
    {
        return $this->hasMany('App\Models\Character\Character');
    }

    /**
     * Checks if they can make a character
     */
    public function canMakeCharacter()
    {
        if ($this->block->settings->character_slots) {
            if ($this->character_slots && is_null($this->confirm_token)) {
                return true;
            } else {
                return false;
            }
        } else {
            if (is_null($this->confirm_token)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Has many modbox tickets
     */
    public function tickets()
    {
        return $this->hasMany('App\Models\Block\ModboxTicket', 'sender_id');
    }

    /**
     * Has many notifications
     */
    public function notifications()
    {
        return $this->hasMany('App\Models\Player\Notification')->orderBy('created_at');
    }

    /**
     * Has many unread notifications
     */
    public function unreadNotifications()
    {
        return $this->hasMany('App\Models\Player\Notification')->where('read', 0)->orderBy('created_at');
    }

    /**
     * Checks permission status
     */
    public function isStaff()
    {
        if ($this->role == 'STAFF' || $this->role == 'ADMIN') return true;
        return false;
    }

    /**
     * Gets a player's skin
     * @return mixed
     */
    public function skin()
    {
        return $this->belongsTo('App\Models\Block\BlockSkin');
    }

    /**
     * Get player tags
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->hasMany('App\Models\Player\ForumTag', 'tagged_id')->where('character_id', 0);
    }

    /**
     * Get all tags
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allTags()
    {
        return $this->hasMany('App\Models\Player\ForumTag', 'tagged_id');
    }

    /**
     * Checks if player is tagged in the thread
     * @param $threadId
     * @return bool
     */
    public function checkTag($threadId)
    {
        $tags = $this->tags;
        foreach ($tags as $tag) if ($tag->thread_id == $threadId) return true;
        return false;
    }

    /**
     * Checks whether a player or any of their characters are tagged in the thread
     * @param $threadId
     * @return bool
     */
    public function checkAllTags($threadId)
    {
        $tags = $this->allTags;
        foreach ($tags as $tag) if ($tag->thread_id == $threadId) return true;
        return false;
    }

    /**
     * Get Currency logs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function currencyLogs()
    {
        return $this->hasMany('App\Models\Player\CurrencyLog')->orderBy('created_at', 'desc');
    }

    /**
     * Get total currency earned
     * @return int
     */
    public function currencyEarned()
    {
        $return = 0;
        foreach ($this->currencyLogs as $log) if ($log->amount > 0) $return += $log->amount;
        return $return;
    }

    /**
     * Get total currency spent
     * @return int
     */
    public function currencySpent()
    {
        $return = 0;
        foreach ($this->currencyLogs as $log) if ($log->amount < 0) $return += $log->amount;
        return $return;
    }

    /**
     * Get active characters
     * @return mixed
     */
    public function activeCharacters()
    {
        return $this->hasMany('App\Models\Character\Character')->where('accepted', 1)->where('inactive', 0);
    }

    /**
     * Get inactive characters
     * @return mixed
     */
    public function inactiveCharacters()
    {
        return $this->hasMany('App\Models\Character\Character')->where('accepted', 1)->where('inactive', 1);
    }

    /**
     * Get a count of ooc posts
     * @return mixed
     */
    public function oocPosts()
    {
        return Post::where('author_id', $this->id)->where('character_id', 0)->count();
    }

    /**
     * Get a count of ic posts
     * @return mixed
     */
    public function icPosts()
    {
        return Post::where('author_id', $this->id)->where('character_id', '!=', 0)->count();
    }

    /**
     * Has many tables
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tables()
    {
        return $this->hasMany('App\Models\Character\Table');
    }

    /**
     * Player can edit group rules
     * @param $id
     * @return bool
     */
    public function canEditGroupRules($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) {
            if ($character->rank->can_edit) $return = true;
        }
        return $return;
    }

    /**
     * Player can edit group news
     * @param $id
     * @return bool
     */
    public function canEditGroupNews($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) {
            if ($character->rank->can_update) $return = true;
        }
        return $return;
    }

    /**
     * Player can edit group ranks
     * @param $id
     * @return bool
     */
    public function canEditGroupRanks($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) {
            if ($character->rank->can_remove || $character->rank->can_promote ||
                $character->rank->can_demote || $character->rank->can_invite) $return = true;
        }
        return $return;
    }

    public function canGroupInvite($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) if ($character->rank->can_invite) $return = true;
        return $return;
    }

    public function canGroupRemove($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) if ($character->rank->can_remove) $return = true;
        return $return;
    }

    public function canGroupPromote($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) if ($character->rank->can_promote) $return = true;
        return $return;
    }

    public function canGroupDemote($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) if ($character->rank->can_demote) $return = true;
        return $return;
    }

    /**
     * Player can edit group affiliations
     * @param $id
     * @return bool
     */
    public function canEditGroupAffiliations($id)
    {
        $return = false;
        foreach ($this->characters as $character) if ($character->group_id == $id) {
            if ($character->rank->can_edit) $return = true;
        }
        return $return;
    }
}
