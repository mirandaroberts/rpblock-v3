<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class ForumTag extends Model
{
    protected $fillable = [
        'thread_id', 'tagged_id', 'tagged_by', 'character_id'
    ];

    public function player() {
        return $this->belongsTo('App\Models\Player\Player', 'tagged_id');
    }

    public function character() {
        return $this->belongsTo('App\Models\Character\Character', 'character_id');
    }

    public function thread() {
        return $this->belongsTo('App\Models\Block\Thread', 'thread_id');
    }
}
