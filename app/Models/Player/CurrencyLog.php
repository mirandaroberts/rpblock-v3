<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class CurrencyLog extends Model
{
    protected $fillable = [
        'player_id', 'amount', 'reason', 'character_id', 'url'
    ];

    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player');
    }

    public function character()
    {
        return $this->belongsTo('App\Models\Character\Character');
    }
}
