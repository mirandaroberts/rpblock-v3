<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $dates = ['read_at'];
    protected $fillable = [
        'message', 'type', 'url', 'player_id'
    ];

    /**
     * belongs to a sender
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player');
    }
}