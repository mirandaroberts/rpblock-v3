<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'player_conversations';

    /**
     * Fillable values
     * @var array
     */
    protected $fillable = ['player1', 'player2', 'subject'];

    /**
     * Belongs to a sending player
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sendingPlayer()
    {
        return $this->belongsTo('App\Models\Player\Player', 'player1');
    }

    /**
     * Belongs to a receiving player
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receivingPlayer()
    {
        return $this->belongsTo('App\Models\Player\Player', 'player2');
    }

    /**
     * Has many messages
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Player\Message');
    }
}
