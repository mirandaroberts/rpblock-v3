<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'player_conversation_messages';

    /**
     * Fillable Values
     * @var array
     */
    protected $fillable = ['conversation_id', 'player_id', 'body', 'read', 'read_at'];

    /**
     * Dates to be converted to Carbon objects
     * @var array
     */
    protected $dates = ['read_at'];

    /**
     * Belongs to a sending player
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player\Player', 'player_id');
    }

    /**
     * Belongs to a conversation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conversation()
    {
        return $this->belongsTo('App\Models\Player\Conversation', 'conversation_id');
    }
}
