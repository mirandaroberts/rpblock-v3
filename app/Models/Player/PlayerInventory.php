<?php

namespace App\Models\Player;

use App\Models\Block\Item as Item;
use Illuminate\Database\Eloquent\Model;

class PlayerInventory extends Model
{

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'player_inventory';
	protected $fillable = ['player_id', 'item_id'];

    /**
     * Belongs to item data
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function itemData()
    {
		return $this->belongsTo(Item::class, 'item_id');
	}

}
