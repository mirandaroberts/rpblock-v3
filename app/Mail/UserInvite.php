<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserInvite extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $level = 'default';
        $introLines = ['You are receiving this email because you have been invited to host a block on RPBlock\'s premium ropleplay forum software.'];
        $actionText = 'Start building your site now!';
        $actionUrl = 'http://www.rpblock.com/login';
        $outroLines = ['Before you can log in, you will need to reset your password using our forgot password feature. You will log in using this email.'];

        return $this->markdown('email', compact('introLines', 'actionText', 'actionUrl', 'outroLines', 'level'));
    }
}
