<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlayerConfirm extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $level = 'default';
        $introLines = ['You are receiving this email because you have registered to join ' . ucfirst($data['block']) . '. Before you can log in, you must activate your account.'];
        $actionText = 'Activate your account!';
        $actionUrl = 'http://' . $data['block'] . '.rpblock.com/activate/' . $data['code'];
        $outroLines = ['Thank you for registering!'];

        return $this->markdown('email', compact('introLines', 'actionText', 'actionUrl', 'outroLines', 'level', 'data'));
    }
}
