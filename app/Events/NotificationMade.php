<?php

namespace App\Events;

use App\Models\Player\Notification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationMade implements ShouldBroadcast
{
    use SerializesModels;

    public $notice;

    /**
     * NotificationMade constructor.
     * @param Notification $notice
     */
    public function __construct(Notification $notice)
    {
        $this->notice = $notice;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notice.' . $this->notice->player_id);
    }
}