<?php namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;
use App\Models\Block\Block;

class CustomValidation extends Validator {
    /**
     * $attribute Input name
     * $value Input value
     * $parameters Table, field1, host
     */
    public function validateUniqueWith($attribute, $value, $parameters)
    {
        if (!isset($parameters[2])) {
            $host = explode('.', request()->server('HTTP_HOST'))[0];
        } else {
            $host = $parameters[2];
        }

        $host = Block::where('name', $host)->firstOrFail();

        $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters, $host) {
            $query->where($attribute, '=', $value)
                  ->Where($parameters[1], $host->id);
        })->first();

        return $result ? false : true;
    }
}