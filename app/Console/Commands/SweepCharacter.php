<?php namespace App\Console\Commands;

use App\Models\Character\Character;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SweepCharacter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sweep:character';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sweeps characters for daily maintenance.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $characters = Character::with('block')->get();
        foreach ($characters as $character) {
            // Check if character needs to be set inactive
            if ($character->last_post && $character->last_post < Carbon::now()->subDays($character->block->settings->days_until_inactive)) {
                $character->inactive = 1;
                $character->save();
            }

            // Characters get health daily


            // TODO: Check rank activity requirements
        }
    }
}
