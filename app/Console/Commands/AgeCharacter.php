<?php namespace App\Console\Commands;

use App\Http\Classes\Helpers;
use App\Models\Character\Character;
use Illuminate\Console\Command;
use Carbon\Carbon;

class AgeCharacter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sweep:character';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sweeps characters for daily maintenance.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $characters = Character::with('block')->where('dead', 0)->get();
        foreach ($characters as $character) {
            $character->months_old++;

            // Check if character needs to die
            if ($character->months_old >= $character->block->settings->maximum_age) {
                $character->dead = 1;
                $character->health = 0;
                $helper = new Helpers();
                $helper->makeNotice('danger', $character->name . ' has died from old age!',
                    '/character/' . $character->name, $character->player->id);
            }

            $character->save();
        }
    }
}
