<?php namespace App\Console\Commands;

use App\Models\Block\Category;
use App\Models\Block\Thread;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SweepForum extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sweep:forum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sweeps forums for daily maintenance.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Category::all();
        foreach ($categories as $category) {
            // Check if thread needs archived
            foreach($category->threads as $thread) {
                if ($thread->updated_at < Carbon::now()->subDays($category->block->settings->days_until_archive)) {
                    $thread->archived = 1;
                    $thread->locked = 1;
                    $thread->archived_at = Carbon::now();
                    $thread->save();

                    // Remove post and thread count from category
                    $category->thread_count--;
                    $category->post_count -= $thread->post_count;
                    $category->save();
                }
            }
        }
    }
}
