<?php

namespace App\Console;

use App\Console\Commands\SweepCharacter;
use App\Console\Commands\SweepForum;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'sweep:character' => SweepCharacter::class,
        'sweep:forum' => SweepForum::class,
        'char:age' => AgeCharacter::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sweep:character')->daily();
        $schedule->command('sweep:forum')->daily();
        $schedule->command('char:age')->cron('0 0 1,15 * * *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
