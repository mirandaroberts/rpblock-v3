<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['domain' => 'www.rpblock.com'], function () {
    // AJAX processes
    Route::post('/reorder/process/ranks', 'Portal\BlockRankController@processReorder')->name('processReorderRanks');
    Route::post('/reorder/process/categories', 'Portal\BlockCategoryController@processReorder')->name('processReorderCategories');
    Route::post('/reorder/process/pages', 'Portal\GuidebookController@processReorder')->name('processReorderPages');

    // Portal Routes...
    Route::group(['middleware' => 'auth', 'prefix' => 'portal'], function () {
        Route::get('/', 'Portal\DefaultController@dashboard');
        Route::group(['middleware' => 'superuser'], function () {
            // Users...
            Route::group(['prefix' => 'users'], function () {
                Route::get('/', 'Portal\UserController@getUsers');
                Route::get('create', 'Portal\UserController@createUser');
                Route::post('create', 'Portal\UserController@postCreateUser');
                Route::get('update/{id}', 'Portal\UserController@updateUser');
                Route::post('update/{id}', 'Portal\UserController@postUpdateUser');
                Route::get('delete/{id}', 'Portal\UserController@deleteUser');
                Route::post('delete/{id}', 'Portal\UserController@postDeleteUser');
            });

            // Plans...
            Route::group(['prefix' => 'plans'], function () {
                Route::get('/', 'Portal\PlanController@getPlans');
                Route::get('create', 'Portal\PlanController@createPlan');
                Route::post('create', 'Portal\PlanController@postCreatePlan');
                Route::get('update/{id}', 'Portal\PlanController@updatePlan');
                Route::post('update/{id}', 'Portal\PlanController@postUpdatePlan');
                Route::get('delete/{id}', 'Portal\PlanController@deletePlan');
                Route::post('delete/{id}', 'Portal\PlanController@postDeletePlan');
            });

            // Blocks...
            Route::group(['prefix' => 'blocks'], function () {
                Route::get('/', 'Portal\BlockController@getBlocks');
                Route::get('create', 'Portal\BlockController@createBlock');
                Route::post('create', 'Portal\BlockController@postCreateBlock');
                Route::get('update/{id}', 'Portal\BlockController@updateBlock');
                Route::post('update/{id}', 'Portal\BlockController@postUpdateBlock');
                Route::get('delete/{id}', 'Portal\BlockController@deleteBlock');
                Route::post('delete/{id}', 'Portal\BlockController@postDeleteBlock');
                Route::get('{id}', 'Portal\BlockController@getOneBlock');
                Route::post('{id}', 'Portal\BlockController@initialize');

                // Character Creation: Attributes...
                Route::group(['prefix' => 'characters/attributes'], function () {
                    Route::get('/{id}', 'Portal\BlockCharacterAttributeController@show')->name('portalCharacterCreation');
                    Route::get('/{id}/create', 'Portal\BlockCharacterAttributeController@createAttribute')->name('createAttribute');
                    Route::post('/{id}/create', 'Portal\BlockCharacterAttributeController@postCreateAttribute');
                    Route::get('/{block}/update/{id}', 'Portal\BlockCharacterAttributeController@updateAttribute')->name('updateAttribute');
                    Route::post('/{block}/update/{id}', 'Portal\BlockCharacterAttributeController@postUpdateAttribute');
                    Route::get('/{block}/delete/{id}', 'Portal\BlockCharacterAttributeController@deleteAttribute')->name('deleteAttribute');
                    Route::post('/{block}/delete/{id}', 'Portal\BlockCharacterAttributeController@postDeleteAttribute');
                });

                // Character Creation: Traits...
                Route::group(['prefix' => 'characters/traits'], function () {
                    Route::get('/{id}/create', 'Portal\BlockCharacterTraitController@createTrait')->name('createTrait');
                    Route::post('/{id}/create', 'Portal\BlockCharacterTraitController@postCreateTrait');
                    Route::get('/{block}/update/{id}', 'Portal\BlockCharacterTraitController@updateTrait')->name('editTrait');
                    Route::post('/{block}/update/{id}', 'Portal\BlockCharacterTraitController@postUpdateTrait');
                    Route::get('/{block}/delete/{id}', 'Portal\BlockCharacterTraitController@deleteAttribute')->name('deleteTrait');
                    Route::post('/{block}/delete/{id}', 'Portal\BlockCharacterTraitController@postDeleteTrait');
                });

                // Thread Types
                Route::group(['prefix' => 'threadtypes'], function () {
                    Route::get('/{id}', 'Portal\BlockThreadTypeController@show')->name('portalThreadTypes');
                    Route::get('/{id}/create', 'Portal\BlockThreadTypeController@create')->name('createType');
                    Route::post('/{id}/create', 'Portal\BlockThreadTypeController@postCreate');
                    Route::get('/{block}/update/{id}', 'Portal\BlockThreadTypeController@update')->name('editType');
                    Route::post('/{block}/update/{id}', 'Portal\BlockThreadTypeController@postUpdate');
                    Route::get('/{block}/delete/{id}', 'Portal\BlockThreadTypeController@delete')->name('deleteType');
                    Route::post('/{block}/delete/{id}', 'Portal\BlockThreadTypeController@postDelete');
                });

                // Guidebook
                Route::group(['prefix' => 'guidebook/{block}'], function () {
                    Route::get('/', 'Portal\GuidebookController@show')->name('portalGuidebook');
                    Route::get('create', 'Portal\GuidebookController@create')->name('createGuidebookPage');
                    Route::post('create', 'Portal\GuidebookController@postCreate');
                    Route::get('update/{id}', 'Portal\GuidebookController@update')->name('editGuidebookPage');
                    Route::post('update/{id}', 'Portal\GuidebookController@postUpdate');
                    Route::get('delete/{id}', 'Portal\GuidebookController@delete')->name('deleteGuidebookPage');
                    Route::post('delete/{id}', 'Portal\GuidebookController@postDelete');
                    Route::get('reorder/', 'Portal\GuidebookController@reorder')->name('reorderGuidebook');
                });

                // Skins
                Route::group(['prefix' => 'skins/{block}'], function () {
                    Route::get('/', 'Portal\SkinController@show')->name('portalSkins');
                    Route::get('create', 'Portal\SkinController@create')->name('createSkin');
                    Route::post('create', 'Portal\SkinController@postCreate');
                    Route::get('update/{id}', 'Portal\SkinController@update')->name('editSkin');
                    Route::post('update/{id}', 'Portal\SkinController@postUpdate');
                    Route::get('delete/{id}', 'Portal\SkinController@delete')->name('deleteSkin');
                    Route::post('delete/{id}', 'Portal\SkinController@postDelete');
                });

                // Groups
                Route::group(['prefix' => 'groups'], function () {
                    Route::get('/{id}', 'Portal\BlockGroupController@show')->name('portalGroups');
                    Route::get('/{id}/create', 'Portal\BlockGroupController@create')->name('createGroup');
                    Route::post('/{id}/create', 'Portal\BlockGroupController@postCreate');
                    Route::get('/{block}/update/{id}', 'Portal\BlockGroupController@update')->name('editGroup');
                    Route::post('/{block}/update/{id}', 'Portal\BlockGroupController@postUpdate');
                    Route::get('/{block}/delete/{id}', 'Portal\BlockGroupController@delete')->name('deleteGroup');
                    Route::post('/{block}/delete/{id}', 'Portal\BlockGroupController@postDelete');
                });

                // Ranks
                Route::group(['prefix' => 'ranks'], function () {
                    Route::get('/{id}', 'Portal\BlockRankController@show')->name('editRanks');
                    Route::get('/{id}/create/', 'Portal\BlockRankController@create')->name('createRank');
                    Route::post('/{id}/create', 'Portal\BlockRankController@postCreate');
                    Route::get('/{block}/update/{id}', 'Portal\BlockRankController@update')->name('editRank');
                    Route::post('/{block}/update/{id}', 'Portal\BlockRankController@postUpdate');
                    Route::get('/{block}/delete/{id}', 'Portal\BlockRankController@delete')->name('deleteRank');
                    Route::post('/{block}/delete/{id}', 'Portal\BlockRankController@postDelete');
                    Route::get('/{id}/reorder/', 'Portal\BlockRankController@reorder')->name('reorderRanks');
                });

                // Categories
                Route::group(['prefix' => 'categories'], function() {
                    Route::get('/{id}', 'Portal\BlockCategoryController@show')->name('portalCategories');
                    Route::get('/{id}/create/', 'Portal\BlockCategoryController@create')->name('createCategory');
                    Route::post('/{id}/create', 'Portal\BlockCategoryController@postCreate');
                    Route::get('/update/{id}', 'Portal\BlockCategoryController@update')->name('editCategory');
                    Route::post('/update/{id}', 'Portal\BlockCategoryController@postUpdate');
                    Route::get('/delete/{id}', 'Portal\BlockCategoryController@delete')->name('deleteCategory');
                    Route::post('/delete/{id}', 'Portal\BlockCategoryController@postDelete');
                    Route::get('/{id}/reorder/', 'Portal\BlockCategoryController@reorder')->name('reorderCategories');
                });

                // Items.
                Route::group(['prefix' => 'items'], function(){
                    Route::get('/{id}', 'Portal\ItemController@getItems')->name('items');
                    Route::get('/{id}/create', 'Portal\ItemController@getCreateItem')->name('item-create');
                    Route::post('/{id}/create', 'Portal\ItemController@postCreateItem')->name('item-create');
                    Route::get('/update/{id}', 'Portal\ItemController@getEditItem')->name('item-edit');
                    Route::post('/update/{id}', 'Portal\ItemController@postEditItem')->name('item-edit');
                    Route::get('/delete/{id}', 'Portal\ItemController@getDeleteItem')->name('item-delete');
                    Route::post('/delete/{id}', 'Portal\ItemController@postDeleteItem')->name('item-delete');
                });
            });

            // Widgets...
            Route::group(['prefix' => 'widgets'], function () {
                Route::get('/', 'Portal\WidgetController@getWidgets');
                Route::get('/create', 'Portal\WidgetController@createWidget');
                Route::post('/create', 'Portal\WidgetController@postCreateWidget');
                Route::get('/update/{id}', 'Portal\WidgetController@updateWidget');
                Route::post('/update/{id}', 'Portal\WidgetController@postUpdateWidget');
                Route::get('/delete/{id}', 'Portal\WidgetController@deleteWidget');
                Route::post('/delete/{id}', 'Portal\WidgetController@postDeleteWidget');
            });
        });
    });

    // Authentication Routes...
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login')->name('logout');
    Route::get('/logout', 'Auth\LoginController@logout');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('portal.password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/sitelist', function() {
        $blocks = App\Block::where('active', 1)->get();
        return view('blocks', compact('blocks'));
    });

    Route::get('/about', function() {
        return view('about');
    });
});

// Block Routes...
Route::group(['domain' => '{block}.rpblock.com', 'middleware' => 'block'], function ($route) {
    $route->get('/', 'Block\DefaultController@index');
    $route->get('/inactive', 'Block\DefaultController@inactive')->name('inactive');
    $route->post('/changeskin', 'Block\DefaultController@changeSkin');
	$route->post('/sitesearch', 'Block\DefaultController@postSiteSearch');
	$route->get('/archives/{filter?}', 'Block\DefaultController@archives');

    // Ajax Calls...
    $route->post('/process/traits', 'Block\CharacterController@getTraits');

    // Authentication Routes...
    $route->post('login', 'Auth\PlayerLoginController@login');
    $route->get('logout', 'Auth\PlayerLoginController@logout');

    // Registration Routes...
    $route->get('register', 'Auth\RegisterController@showRegisterForm');
    $route->post('register', 'Auth\RegisterController@register');

    // Activate Account Routes...
    $route->get('activate/{code?}', 'Block\DefaultController@activate')->name('activate');
    $route->post('activate/{code?}', 'Block\DefaultController@postActivate')->name('postActivate');
    $route->get('resendcode', 'Block\DefaultController@resendCode')->name('resend');

    // Password Reset Routes...
    $route->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $route->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $route->post('password/reset', 'Auth\PasswordController@reset');

    // Authenticated Routes...
    $route->group(['middleware' => 'auth:player'], function() use ($route) {
        $route->get('dashboard', 'Block\DefaultController@dashboard');
        $route->get('notifications', 'Block\NotificationController@show');
        $route->get('readNotice/{id}', 'Block\NotificationController@read')->name('readNotice');
        $route->get('inventory/{user?}', 'Block\ShopController@inventory');
        $route->get('/settings', 'Block\PlayerController@settings');

        // Shop
        $route->group(['prefix' => 'shop'], function () use ($route) {
            $route->get('/', 'Block\ShopController@show');
            $route->post('/buy/{id}', 'Block\ShopController@buy');
        });

        // Private Messaging
        $route->group(['prefix' => 'conversations'], function () use ($route) {
            $route->get('/', 'Block\ConversationController@index');
            $route->get('send/{username?}', 'Block\ConversationController@getSend');
            $route->post('send/{username?}', 'Block\ConversationController@postSend');
            $route->get('/{id}', 'Block\ConversationController@showConversation');
            $route->post('/{id}', 'Block\ConversationController@postMessage');
        });

        // Table Saver
        $route->group(['prefix' => 'tables'], function () use ($route) {
            $route->get('/create', 'Block\TableController@getCreate');
            $route->post('/create', 'Block\TableController@postCreate');
            $route->get('/{name?}', 'Block\TableController@index');
            $route->get('/edit/{id}', 'Block\TableController@getEdit');
            $route->post('/edit/{id}', 'Block\TableController@postEdit');
        });
    });

    // Character Routes
    $route->get('characters/{name?}', 'Block\CharacterController@show')->name('showCharacters');
    $route->get('character/create', 'Block\CharacterController@create')->name('createCharacter')->middleware('auth:player');
    $route->post('character/create', 'Block\CharacterController@postCreate')->middleware('auth:player');
    $route->get('character/{name}', 'Block\CharacterController@profile')->name('characterProfile');
    $route->get('character/{name}/edit', 'Block\CharacterController@edit')->middleware('auth:player');
    $route->post('character/{name}/edit', 'Block\CharacterController@postEdit')->middleware('auth:player');

    // Player Routes...
    $route->get('/players', 'Block\PlayerController@show')->name('showPlayers');

    $route->group(['prefix' => 'player'], function () use ($route) {
        $route->get('/{name}', 'Block\PlayerController@profile')->name('playerProfile');
        $route->get('/{name}/edit', 'Block\PlayerController@edit')->middleware('auth:player');
        $route->post('/{name}/edit', 'Block\PlayerController@postEdit')->middleware('auth:player');
        $route->get('/{name}/{currency}', 'Block\PlayerController@currencyLog')->middleware('auth:player');
        $route->post('/{name}/{currency}', 'Block\PlayerController@editCurrencyLog')->middleware('auth:player');
    });

    // Modbox Routes...
    $route->group(['prefix' => 'modbox'], function () use ($route) {
        $route->get('/', 'Block\ModboxController@show')->name('modbox');
        $route->get('/ticket/{id}/accept', 'Block\ModboxController@accept');
        $route->get('/ticket/{id}', 'Block\ModboxController@showTicket')->name('ticket');
        $route->post('/ticket/{id}', 'Block\ModboxController@postComment');
        $route->get('/comment/{id}/edit', 'Block\ModboxController@editComment');
        $route->post('/comment/{id}/edit', 'Block\ModboxController@postEditComment');
        $route->get('/comment/{id}/delete', 'Block\ModboxController@deleteComment');
        $route->post('/comment/{id}/delete', 'Block\ModboxController@postDeleteComment');
    });

    // Guidebook Routes...
    $route->group(['prefix' => 'guidebook'], function () use ($route) {
        $route->get('/{page?}', 'Block\GuidebookController@index')->name('guidebook');
    });

    // Tags
    $route->group(['prefix' => 'tags'], function () use ($route) {
        $route->get('/self/{id}', 'Block\TagController@tagInto')->middleware('auth:player');
        $route->get('/{method}/{id}', 'Block\TagController@show')->middleware('auth:player');
    });

    // Categories
    $route->group(['prefix' => 'category'], function () use ($route) {
        $route->get('{id}', 'Forum\CategoryController@fetch')->name('category.show');
        $route->delete('{id}', ['as' => 'delete', 'uses' => 'Forum\CategoryController@destroy']);
        $route->patch('{id}/enable-threads', ['as' => 'enable-threads', 'uses' => 'Forum\CategoryController@enableThreads']);
        $route->patch('{id}/disable-threads', ['as' => 'disable-threads', 'uses' => 'Forum\CategoryController@disableThreads']);
        $route->patch('{id}/make-public', ['as' => 'make-public', 'uses' => 'Forum\CategoryController@makePublic']);
        $route->patch('{id}/make-private', ['as' => 'make-private', 'uses' => 'Forum\CategoryController@makePrivate']);
        $route->patch('{id}/move', ['as' => 'move', 'uses' => 'Forum\CategoryController@move']);
        $route->patch('{id}/rename', ['as' => 'rename', 'uses' => 'Forum\CategoryController@rename']);
        $route->patch('{id}/reorder', ['as' => 'reorder', 'uses' => 'Forum\CategoryController@reorder']);
    });

    // Threads
    $route->group(['prefix' => 'thread'], function () use ($route) {
        $route->get('{category}/create', 'Forum\ThreadController@newForm')->name('thread.create.form')->middleware('auth:player');
        $route->post('{category}/create', 'Forum\ThreadController@store')->name('thread.create')->middleware('auth:player');
        $route->get('{id}', 'Forum\ThreadController@fetch')->name('thread');
        $route->post('{id}', 'Forum\ThreadController@quickReply')->middleware('auth:player');
        $route->get('{id}/newpost', 'Forum\ThreadController@replyForm')->middleware('auth:player');
        $route->post('{id}/newpost', 'Forum\ThreadController@reply')->middleware('auth:player')->name('post.create');
        $route->get('{id}/exit', 'Forum\ThreadController@exitForm')->middleware('auth:player');
        $route->post('{id}/exit', 'Forum\ThreadController@exit')->middleware('auth:player');

        /**$route->get('/', ['as' => 'index', 'uses' => 'Forum\ThreadController@index']);
        $route->get('new', ['as' => 'index-new', 'uses' => 'Forum\ThreadController@indexNew']);
        $route->patch('new', ['as' => 'mark-new', 'uses' => 'Forum\ThreadController@markNew']);
        $route->post('/', ['as' => 'store', 'uses' => 'Forum\ThreadController@store']);
        $route->get('{id}', ['as' => 'fetch', 'uses' => 'Forum\ThreadController@fetch']);
        $route->delete('{id}', ['as' => 'delete', 'uses' => 'Forum\ThreadController@destroy']);
        $route->patch('{id}/restore', ['as' => 'restore', 'uses' => 'Forum\ThreadController@restore']);
        $route->patch('{id}/move', ['as' => 'move', 'uses' => 'Forum\ThreadController@move']);
        $route->patch('{id}/lock', ['as' => 'lock', 'uses' => 'Forum\ThreadController@lock']);
        $route->patch('{id}/unlock', ['as' => 'unlock', 'uses' => 'Forum\ThreadController@unlock']);
        $route->patch('{id}/pin', ['as' => 'pin', 'uses' => 'Forum\ThreadController@pin']);
        $route->patch('{id}/unpin', ['as' => 'unpin', 'uses' => 'Forum\ThreadController@unpin']);
        $route->patch('{id}/rename', ['as' => 'rename', 'uses' => 'Forum\ThreadController@rename']);**/
    });

    // Posts
    $route->group(['prefix' => 'post'], function () use ($route) {
        $route->get('/edit/{id}', 'Forum\PostController@getEdit');
        $route->post('/edit/{id}', 'Forum\PostController@postEdit');
    });

    // Groups
    $route->group(['prefix' => 'groups'], function () use ($route) {
        $route->get('/', 'Block\GroupController@show');
        $route->get('/{id}', 'Block\GroupController@showOne')->name('group');
        $route->post('/{id}/claim', 'Block\GroupController@claim');
        $route->get('/{id}/rules', 'Block\GroupController@rules');
        $route->post('/{id}/rules', 'Block\GroupController@newRule');
        $route->get('/{id}/rules/{rule}', 'Block\GroupController@editRule');
        $route->post('/{id}/rules/{rule}', 'Block\GroupController@postEditRule');
        $route->get('/{id}/news', 'Block\GroupController@news');
        $route->get('/{id}/editnews', 'Block\GroupController@editNews');
        $route->post('/{id}/editnews', 'Block\GroupController@postEditNews');
        $route->get('/{id}/ranks', 'Block\GroupController@editRanks');
        $route->post('/{id}/invite', 'Block\GroupController@invite');
        $route->post('/{id}/remove', 'Block\GroupController@remove');
        $route->get('/acceptinvite/{id}', 'Block\GroupController@acceptInvite');
        $route->get('/declineinvite/{id}', 'Block\GroupController@declineInvite');
    });
});