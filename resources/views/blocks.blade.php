@extends('layouts.app')
@section('title', 'Blocks')

@section('content')
    <br><br>
    <div class="container">
        <div class="text-center">RPBlock currently hosts <b>{{ $blocks->count() }}</b> active blocks!</div>

        <hr>

        @foreach ($blocks as $block)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <a href="http://{{ $block->name }}.rpblock.com">
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ ucfirst($block->name) }}</h4><br>
                                <small>{{ $block->players->count() }} players</small>
                                <br><br>
                                {{ $block->settings->description }}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach

        <footer>
            <p>&copy; Nightowl Workshop LLC 2017</p>
        </footer>
    </div> <!-- /container -->
@endsection