@extends('layouts.app')
@section('title', 'About RPBlock')

@section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <h1>About RPBlock</h1>
            <h5>Modernizing Forum Roleplay</h5>

            <div class="col-md-8 col-md-offset-2">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60"
                         aria-valuemin="0" aria-valuemax="100" style="width:60%">
                        <span class="sr-only">60% Complete</span>
                    </div>
                </div>
                <small><b>Estimated release date: Oct 1st or sooner.</b> Core system and functions have been completed. All that remains is tying up loose ends and coding some additional "bells and whistles."</small>
            </div>
        </div>

            <hr>

        <div class="row">
            <div class="col-md-12">
                <p class="text-justify">RPBlock is an all-in-one forum software and hosting specifically designed to cater to roleplay systems. When you create a block with us you will get complete access to all of our features as well as a free subdomain (yoursite.rpblock.com) on our dedicated server. This page is a work in progress and will go over all of the major features as well as how to best use RPBlock to create fun and dynamic roleplay forums.</p>

                <h2>Core Features</h2>
                <ul>
                    <li>Mobile friendly UI.</li>
                    <li>Modbox with Ticket support</li>
                    <li>Single-click Character acceptance</li>
                    <li>Unique "tagging" system that will keep track of who is involved in a
                        thread</li>
                    <li>Notifications when you are tagged, or a thread you are tagged in is updated</li>
                    <li>Auto archiving for dead threads and threads that all tagged characters have exited.</li>
                    <li>Auto Points and Fight logs.</li>
                    <li>Currency system where players can earn and spend currency.</li>
                    <li>Auto aging for characters</li>
                    <li>Automated random events that will target characters based off of PvP setting</li>
                    <li>Pre-built and easily customizable achievements</li>
                    <li>Robust group management (prides, packs, clans etc) that includes automatic census with the ability for leaders to manage their groups effectively.</li>
                    <li>Auto-enforcement and group disbands of post requirements for group leaders and regular characters</li>
                    <li>Table saver, which will eliminate the need to "code" every post. Simply code it once and then select the table you want to use in your post.</li>
                    <li>No-coding-required Skins</li>
                    <li>In-house chat system included</li>
                    <li>Robust staff controls makes managing characters and players a breeze.</li>
                </ul>

                <hr>

                <h2>Widgets</h2>

                <p class="text-justify">
                    Widgets are optional features that can be disabled and enabled with a click of a button and can offer your site much more robust functionality. Right now RPBlock has nine widgets planned to further expand your block.

                    <ol>
                        <li><b>Character Lineages</b> - Lineages that are managed by the site. Simply add the name of the mother and father of a character, and the site will populate their siblings, aunt/uncles, even cousins! This widget also features a fully configurable offspring roller that allows the site to roll conceived children using the genetic traits of the parents.</li>
                        <li><b>Tasks</b> - Create task-like quests for characters to complete in threads. When a character exits a thread, they may mark that a task has been completed and it will be sent to the Modbox for staff approval. When staff approve, awards for the task will be delivered automatically to the character. Tasks can be repeatable or set to be redeemable a set amount of times.</li>
                        <li><b>Skills</b> - An extension of the task widget. Skills allows block admins to create different skills for the character to master (e.g. strength, speed, etc.) and gives the character a level. Characters gain experience and level up by completing tasks and posting in threads. When they level up, they may assign a skill point to a skill of their choice. Tasks can also be set to award skill points on completion. For example a character that completes a stealth based task can receive a +1 to their "stealth" skill. Skills are displayed in a chart on the character's profile.</li>
                        <li><b>Fight System</b> - This widget allows characters to declare fights on each other. It requires skills, tasks, and PvP widgets to run. Admins will configure fight types and challenges and the parameters in which they can be declared and how many rounds they last. Each fight will have a default timer that is started automatically between rounds. Staff determine the fight result and rewards/logs are automatically updated. Each character will be given a percentage health bar and health will be diminished between fight rounds at the discretion of staff. PvP classifications will protect characters that don't want major harm, but any character that reaches 0% health dies.</li>
                        <li><b>Random Events</b> - Random events that are configured by admins and have a chance to show up on threads. Staff can also generate a random event for a specific thread dynamically to further customize the experience. If PvP is enabled as well, events can be set to target or exclude certain PvP classifications. If the fight system is in place, characters can lose health from these random events.</li>
                        <li><b>Prisoner Rank</b> - Allows groups to take prisoners. Prisoners are a rank added only by staff (to prevent abuse) to a character and it prevents them from posting in threads they aren't tagged in and prevents them from being tagged in threads outside of the group's territory unless by a group's leader/coleaders. A prisoner may attempt to escape (results determined by staff) or fight for their freedom if the Fight System widget is active.</li>
                        <li><b>Dynamic Weather</b> - Set the weather patterns of your block and the weather will be changed daily within the pre-set parameters. If the fight system is active, the weather at the time of the fight start will be attached to the thread to aid in judging.</li>
                        <li><b>Group Inventories</b> - Admins can set commodities for each group that are "harvetable" in threads within that group's territory (or universally). Harvested commodities are added to that group's inventory and are depleted over time or by site staff. For instance "Group A" might have the commodity "Food" that gets depleted over time and depletes faster the more members it has and the commodity "Iron" that only get depleted when it is used to make something in a thread. Commodities may be traded by leaders and coleaders between groups.</li>
                        <li><b>PvP Ranks</b> - Classifies characters in one of four ranks. Mostly used in other widgets to give varying degrees of intensity in gameplay experience.</li>
                    </ol>
                </p>

                <hr>

                <h2>Tagging Explained</h2>

                <p class="text-justify">
                    RPBlock uses a tagging system to keep track of characters involved in a thread.
                    If you have a character that has been "tagged" in a thread, it means that the
                    player who started that thread indicated that your character is involved. Characters are also automatically tagged into threads should they post to it.
                    <br><br>
                    Once a character has been tagged, they can only by untagged by posting an exit post or by being removed by staff. This is so
                    characters cannot dodge involvement in things like fight threads. If a character
                    has been incorrectly tagged, it's very easy for staff to untag that character.
                    Players can keep track of threads that they have been tagged in from the
                    "Thread Tags" tab in their dashboards. Notifications will also be delivered to
                    all tagged players when a new post has been made a thread they have been tagged in. The only way to "untag" your character is to post with the "exit" option checked, indicating your character has exited the thread.
                </p>

                <h4>Example uses</h4>
                <ol>
                    <li>Character "A" want to fight character "B" in a thread. All "A" has to do is open the thread actions window and select that they want to fight. They will be given a list of fights they can choose from based on the fights that staff have made, and then they select from characters that are tagged into the thread. They select "B" and the fight is automatically initiated.</li>
                    <li>Character "A" wants a private thread with "B." All "A" has to do is create a
                        new thread, mark it as "private" and then tag "B" in it on creation. Only
                        tagged characters can post in a private thread, so now they can't be bothered!</li>
                    <li>All characters have chosen to exit the thread and thus it is "empty." The system will consider the thread finished and archive it automatically!</li>
                    <li>Leader of Group "Alpha" wants to host a group meeting. So he creates a thread and tags the group "Alpha" in the thread. Every member of that group will subsequently be tagged into the group meeting!</li>
                </ol>
            </div>
        </div>

        <hr>

        <footer>
            <p>&copy; Nightowl Workshop LLC 2017</p>
        </footer>
    </div> <!-- /container -->
@endsection