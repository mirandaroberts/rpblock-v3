@extends('layouts.block')

@section('title')
    Inventory
@endsection

@section('breadcrumbs')
    Inventory
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            @if ($p == $player)
                Your Inventory
            @else
                {{ $p }}'s Inventory
            @endif
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            A list of items
        </h1>

        <h4>OOC Items</h4>
        @foreach ($items as $item)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <div class="caption">
                        <div class="text-center">
                            <h4 class="inline">{{ ucfirst($item->itemData->name) }}</h4><br>
                            <img src="{{ $item->itemData->icon }}" width="100" height="100" alt="{{ $item->itemData->name }}"><br>
                            <b>{{ strtoupper($item->itemData->type) }} Item</b><br>
                            {{ $item->itemData->description }}<br><br>
                            Quantity: <b>{{ $item->total }}</b>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        @if(!$items->count())
            You have no OOC items.
        @endif

        <hr>

        @foreach ($player->characters as $character)
            <div class="row">
                <h4>{{ $character->name }}'s Items</h4>
                @foreach ($character->items as $item)
                    <div class="col-md-4 text-center">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="text-center">
                                    <h4 class="inline">{{ ucfirst($item->itemData->name) }}</h4><br>
                                    <img src="{{ $item->itemData->icon }}" width="100" height="100" alt="{{ $item->itemData->name }}"><br>
                                    <b>{{ strtoupper($item->itemData->type) }} Item</b><br>
                                    {{ $item->itemData->description }}<br><br>
                                    Quantity: <b>{{ $item->total }}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                @if(!$character->items->count())
                    {{ $character->name }} has no items.
                @endif
            </div>

            <hr>
        @endforeach
    </div>
@endsection