@extends('layouts.block')

@section('title')
    Player - {{ $p->username }}
@endsection

@section('breadcrumbs')
    <a href="/players">Players</a>
    Player Profile
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ $p->username }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            {{ $p->role }}

            <span class="pull-right">
                @if (isset($player))
                    <a href="/report/player/{{ $p->id }}"><i class="fa fa-flag"> Report</i></a>

                    @if ($p->id == $player->id || $player->isStaff())
                        &nbsp;&nbsp; <a href="/player/{{ $p->username }}/edit"><i class="fa fa-pencil"> Edit</i></a>
                    @endif
                @endif
            </span>
        </h1>

        <div class="row">
            <div class="col-md-4 text-center">
                <b>{{ $p->username }}</b>
                <br>

                <img src="{{ $p->icon }}" class="avatar" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}">

                @if (isset($player))
                    <br><a href="/conversations/send/{{ $p->username }}"><i class="fa fa-envelope"></i> Send Private Message</a>
                @endif

                <div class="table-responsive text-left">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><b>OOC Posts</b></td>
                                <td>{{ $p->oocPosts() }}</td>
                            </tr>
                            <tr>
                                <td><b>IC Posts</b></td>
                                <td>{{ $p->icPosts() }}</td>
                            </tr>
                            <tr>
                                <td><b>Achievements</b></td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td><b>Last Active</b></td>
                                <td>
                                    @if ($p->last_active)
                                        {{ $p->last_active->diffForHumans() }}
                                    @else
                                        Never
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Joined</b></td>
                                <td>{{ $p->created_at->format('m-d-Y') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-8">
                {!! $p->bio !!}
            </div>
        </div>
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Characters
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">{{ $p->username }} has {{ $p->characters->count() }} characters</h1>

        @foreach ($p->characters as $character)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <a href="{{ route('characterProfile', ['block' => $block->name, 'name' => $character->name]) }}">

                        <img src="{{ $character->icon }}" class="avatar" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}">
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ $character->fullName() }}</h4>
                                <small>{{ $character->getActiveStatus() }}</small>

                                <h1 class="group-type">{{ $character->groupName() }}</h1>

                                <div class=".table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b>Age</b></td>
                                                <td>{{ $character->age() }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Sex</b></td>
                                                <td>{{ ucfirst($character->gender) }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Posts</b></td>
                                                <td>{{ $character->post_count }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Last Posted</b></td>
                                                <td>
                                                    @if ($character->last_post)
                                                        {{ $character->last_post->diffForHumans() }}
                                                    @else
                                                        Never
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>

    @if (isset($player))
        @if ($p->id != $player->id && $player->isStaff())
            <div class="col-md-12 forum-category rounded top lpad">
                <div class="col-md-10">
                    Notes
                </div>
            </div>

            <div class="col-md-12 toggleview normal lpad">
                <h1 class="inset">Viewable to staff only</h1>

                {!! $p->notes !!}
            </div>
        @endif
    @endif
@endsection
