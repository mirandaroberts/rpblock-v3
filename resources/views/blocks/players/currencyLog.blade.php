@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->currency_name) }} Log
@endsection

@section('breadcrumbs')
    <a href="/players">Players</a>
    <a href="/player/{{ $p->username }}">{{ $p->username }}</a>
    {{ ucfirst($block->settings->currency_name) }} Log
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            @if ($p == $player)
                Your {{ ucfirst($block->settings->currency_name) }} Log
            @else
                {{ $p->username }}'s {{ ucfirst($block->settings->currency_name) }}
            @endif
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Ordered by most recently earned</h1>
        <div class="text-center">
            @if ($p == $player)
                You have <b>{{ $p->currency }}</b> {{ $block->settings->currency_name }}
            @else
                {{ $p->username }} has <b>{{ $p->currency }}</b> {{ $block->settings->currency_name }}
            @endif
        </div>

        <table class="table-responsive table table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Reason</th>
                <th>Character</th>
                <th>URL</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($logs as $log)
                <tr>
                    <td>{{ $log->created_at->format('m-d-Y') }}</td>
                    <td>{{ $log->amount }}</td>
                    <td>{{ $log->reason }}</td>
                    <td>
                        @if ($log->character_id)
                            <a href="/character/{{ $log->character->name }}">{{ $log->character->name }}</a>
                        @else
                            None
                        @endif
                    </td>
                    <td>
                        @if ($log->url)
                            <a href="{{ $log->url }}">[ Link ]</a>
                        @else
                            None
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $logs->links() }}
    </div>

    @if ($player->isStaff() && $p->id != $player->id)
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                Staff Tools
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">Add or remove {{ $block->settings->currency_name }}</h1>

            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('amt') ? ' has-error' : '' }}">
                    <label for="amt" class="col-md-4 control-label">Amount</label>

                    <div class="col-md-6">
                        <input id="amt" type="number" class="form-control" name="amt" value="{{ old('amt', 0) }}" required autofocus>
                        <small>Set a negative amount to deduct points</small>

                        @if ($errors->has('amt'))
                            <span class="help-block">
                                <strong>{{ $errors->first('amt') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                    <label for="reason" class="col-md-4 control-label">Reason</label>

                    <div class="col-md-6">
                        <input id="reason" type="text" class="form-control" name="reason" value="{{ old('reason') }}" required autofocus>

                        @if ($errors->has('reason'))
                            <span class="help-block">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                    <label for="url" class="col-md-4 control-label">URL (optional)</label>

                    <div class="col-md-6">
                        <input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}" autofocus>

                        @if ($errors->has('url'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('character') ? ' has-error' : '' }}">
                    <label for="character" class="col-md-4 control-label">Character</label>

                    <div class="col-md-6">
                        <select name="character" class="form-control">
                            <option value="ooc" @if (old('character') == 'ooc') selected @endif>OOC (No Character)</option>
                            @foreach ($p->characters as $character)
                                <option value="{{ $character->id }}" @if (old('character') == $character->id) selected @endif>{{ $character->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('character'))
                            <span class="help-block">
                                <strong>{{ $errors->first('character') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    @endif
@endsection