@extends('layouts.block')

@section('title')
    Players
@endsection

@section('breadcrumbs')
    Players
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Players
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">All Players</h1>

        <div class="text-center">
            There are <b>{{ $block->players->count() }}</b> total players.<br>
            {{ $players->links() }}
        </div>

        @foreach ($players as $p)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <a href="{{ route('playerProfile', ['block' => $block->name, 'name' => $p->username]) }}">

                        <img src="{{ $p->icon }}" class="avatar" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}">
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ $p->username }}</h4>
                                <small>{{ $p->role }}</small>

                                <div class=".table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b>Characters</b></td>
                                                <td>{{ $p->characters->count() }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Posts</b></td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td><b>Last Active</b></td>
                                                <td>
                                                    @if ($p->last_active)
                                                        {{ $p->last_active->diffForHumans() }}
                                                    @else
                                                        Never
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection