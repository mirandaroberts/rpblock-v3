@extends('layouts.block')

@section('title')
    Edit Player
@endsection

@section('breadcrumbs')
    <a href="/players">Players</a>
    Edit Player
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Edit Player
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Editing {{ $p->username }}

            <span class="pull-right">
                <a href="/player/{{ $p->username }}/points"><i class="fa fa-bar-chart"></i> Point Log</a>
                <a href="/player/{{ $p->username }}/inventory"><i class="fa fa-shopping-cart"></i> Inventory</a>
            </span>
        </h1>

        <div class="row">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                @if ($player->isStaff() && $p->id != $player->id)
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Username</label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ $p->username }}" required autofocus>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                        <label for="notes" class="col-md-4 control-label">Notes</label>

                        <div class="col-md-6">
                            <textarea id="notes" class="form-control" name="notes">{{ $p->notes }}</textarea>

                            @if ($errors->has('notes'))
                                <span class="help-block">
                            <strong>{{ $errors->first('notes') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>

                    @if ($player->role == 'ADMIN')
                        <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                                <select name="role" class="form-control">
                                    <option value="PLAYER" @if ($p->role == 'PLAYER') selected @endif>Player</option>
                                    <option value="STAFF" @if ($p->role == 'STAFF') selected @endif>Staff</option>
                                    <option value="ADMIN" @if ($p->role == 'ADMIN') selected @endif>Admin</option>
                                    <option value="BANNED" @if ($p->role == 'BANNED') selected @endif>Banned</option>
                                </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    @endif

                    <div class="form-group{{ $errors->has('title_unlocked') ? ' has-error' : '' }}">
                        <label for="title_unlocked" class="col-md-4 control-label">Title Unlocked</label>

                        <div class="col-md-6">
                            <select name="title_unlocked" class="form-control">
                                <option value="0" @if (!$p->title_unlocked) selected @endif>No</option>
                                <option value="1" @if ($p->title_unlocked) selected @endif>Yes</option>
                            </select>

                            @if ($errors->has('title_unlocked'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title_unlocked') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('vista_unlocked') ? ' has-error' : '' }}">
                        <label for="vista_unlocked" class="col-md-4 control-label">Vista Unlocked</label>

                        <div class="col-md-6">
                            <select name="vista_unlocked" class="form-control">
                                <option value="0" @if (!$p->vista_unlocked) selected @endif>No</option>
                                <option value="1" @if ($p->vista_unlocked) selected @endif>Yes</option>
                            </select>

                            @if ($errors->has('vista_unlocked'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('vista_unlocked') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <hr>
                @endif

                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                    <label for="icon" class="col-md-4 control-label">Icon</label>

                    <div class="col-md-6">
                        <input id="icon" type="text" class="form-control" name="icon" value="{{ $p->getOriginal('icon') }}" autofocus>

                        @if ($errors->has('icon'))
                            <span class="help-block">
                            <strong>{{ $errors->first('icon') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                @if ($p->title_unlocked)
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ $p->title }}" autofocus>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                @if ($p->vista_unlocked)
                    <div class="form-group{{ $errors->has('vista') ? ' has-error' : '' }}">
                        <label for="vista" class="col-md-4 control-label">Vista</label>

                        <div class="col-md-6">
                            <input id="vista" type="text" class="form-control" name="vista" value="{{ $p->vista }}" autofocus>

                            @if ($errors->has('vista'))
                                <span class="help-block">
                                <strong>{{ $errors->first('vista') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Email</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $p->email }}" autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                    <label for="bio" class="col-md-4 control-label">Bio</label>

                    <div class="col-md-6">
                        <textarea id="bio" class="form-control" name="bio">{{ $p->bio }}</textarea>

                        @if ($errors->has('bio'))
                            <span class="help-block">
                            <strong>{{ $errors->first('bio') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('hidden') ? ' has-error' : '' }}">
                    <label for="hidden" class="col-md-4 control-label">Privacy</label>

                    <div class="col-md-6">
                        <select name="hidden" class="form-control">
                            <option value="0" @if ($p->role == 'public') selected @endif>Public</option>
                            <option value="1" @if ($p->role == 'hidden') selected @endif>Hidden</option>
                        </select>

                        @if ($errors->has('hidden'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('hidden') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection