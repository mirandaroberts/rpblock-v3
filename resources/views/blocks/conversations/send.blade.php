@extends('layouts.block')

@section('title')
    Start Conversation
@endsection

@section('breadcrumbs')
    <a href="/conversations">Conversations</a>
    Create Conversation
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Create Conversation
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Start a conversation with another player</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                    <label for="subject">Subject</label>
                    <input type="text" name="subject" id="subject" value="{{ old('subject') }}" class="form-control">

                    @if ($errors->has('subject'))
                        <span class="help-block">
                        <strong>{{ $errors->first('subject') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="recipient" class="control-label">Recipient</label>
                    <select id="recipient" name="recipient" class="form-control autocomplete">
                        @foreach ($block->players as $user)
                            @if ($user->id != $player->id)
                                <option value="{{ $user->id }}" @if ($recipient && $user->id == $recipient->id) selected @endif>{{ $user->username }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group{{ $errors->has('private') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea name="content" class="wysiwyg form-control">{{ old('content') }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" name="method" value="submit" class="btn btn-primary pull-right">Create</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.autocomplete').select2({
                placeholder: "Select a player to converse with!",
            });
        });
    </script>
@endsection