@extends('layouts.block')

@section('title')
    Conversations
@endsection

@section('breadcrumbs')
    Conversations
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Conversations
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Showing your Conversations</h1>

        <div class="col-md-12 table-responsive">
            <p>Conversations that have had no replies within the last 30 days will be purged.</p>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Player</th>
                    <th>Subject</th>
                    <th>Messages</th>
                    <th>Last Updated</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($conversations as $conversation)
                        <tr>
                            <td>
                                @if ($conversation->player1 == $player->id)
                                    <a href="/player/{{ $conversation->receivingPlayer->username }}">{{ $conversation->receivingPlayer->username }}</a>
                                @else
                                    <a href="/player/{{ $conversation->sendingPlayer->username }}">{{ $conversation->sendingPlayer->username }}</a>
                                @endif
                            </td>
                            <td><a href="/conversations/{{ $conversation->id }}">{{ $conversation->subject }}</a></td>
                            <td>{{ $conversation->messages->count() }}</td>
                            <td>{{ $conversation->updated_at->diffForHumans() }}</td>
                            <td>
                                <a href=""><i class="fa fa-flag"></i> Report</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection