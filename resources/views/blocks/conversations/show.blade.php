@extends('layouts.block')

@section('title')
    Conversation with {{ $conversation->sendingPlayer->username }}
@endsection

@section('breadcrumbs')
    <a href="/conversations">Conversations</a>
    {{ $conversation->subject }}
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ $conversation->subject }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Conversation with
            @if ($conversation->player1 == $player->id)
                <a href="/player/{{ $conversation->receivingPlayer->username }}">{{ $conversation->receivingPlayer->username }}</a>
            @else
                <a href="/player/{{ $conversation->sendingPlayer->username }}">{{ $conversation->sendingPlayer->username }}</a>
            @endif
        </h1>

        <div class="col-md-12">
            <div id="messages">
                <ul>
                    @foreach ($messages as $message)
                        <li class="clearfix">
                            <div class="message-data @if ($message->player->id == $player->id) align-left @else align-right @endif">
                                <span class="message-data-name" >
                                    <a href="/player/{{ $message->player->username }}">{{ $message->player->username }}</a> || {{ $message->created_at->diffForHumans() }}
                                </span>
                            </div>
                            <div class="message @if ($message->player->id == $player->id) my-message float-left @else other-message float-right @endif">
                                {!! $message->body !!}
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea name="content" class="wysiwyg form-control">{{ old('content') }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" name="method" value="submit" class="btn btn-primary pull-right">Reply</button>
            </form>
        </div>
    </div>
@endsection