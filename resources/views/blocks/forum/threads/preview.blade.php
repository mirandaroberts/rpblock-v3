@extends('layouts.block')

@section('title')
    Preview Thread
@endsection

@section('breadcrumbs')
    <a href="/category/{{ $category->id }}">{{ $category->title }}</a>
    Preview Thread
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Preview
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">This is how your thread will appear once posted.</h1>

        @if ($category->type == 'ooc')
            <table class="post">
                <tr>
                    <td class="mini" style="
                    @if ($player->vista_unlocked)
                            background-image: url({{ $player->vista }});
                    @endif">
                        @include('blocks.partials.miniprofile_ooc', ['block' => $block, 'player' => $player])
                    </td>
                    <td style="vertical-align: top;">
                        @if ($table) {!! $table->header !!} @endif
                        @if ($table) {!! str_replace(['{', '}'], [$table->pre, $table->post], htmlspecialchars_decode($data['content'])) !!} @else {!! htmlspecialchars_decode($data['content']) !!} @endif
                        @if ($table) {!! $table->footer !!} @endif
                    </td>
                </tr>
            </table>
        @else
            <table class="post">
                <tr>
                    <td class="mini" style="
                    @if ($character->vista_unlocked)
                            background-image: url({{ $character->vista }});
                    @endif">
                        @include('blocks.partials.miniprofile_ic', ['block' => $block, 'character' => $character])
                    </td>
                    <td style="vertical-align: top;">
                        @if ($table) {!! $table->header !!} @endif
                        @if ($table) {!! str_replace(['{', '}'], [$table->pre, $table->post], htmlspecialchars_decode($data['content'])) !!} @else {!! htmlspecialchars_decode($data['content']) !!} @endif
                        @if ($table) {!! $table->footer !!} @endif
                    </td>
                </tr>
            </table>
        @endif
    </div>


    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Preview Thread
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Create new thread in {{ $category->title }}</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('thread.create', ['block' => $block->name, 'category' => $category->id]) }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('private') ? ' has-error' : '' }}">
                    <label for="title">Title</label>
                    <input type="text" name="title" value="{{ old('title', $data['title']) }}" class="form-control">

                    @if ($errors->has('title'))
                        <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>

                @if ($category->type == 'ic')
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select name="type" class="form-control">
                            <option value="0">None</option>
                            @foreach ($block->types as $type)
                                <option value="{{ $type->id }}" @if ($data['type'] == $type->id) selected @endif>{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="character_id">Character</label>
                        <select name="character_id" class="form-control">
                            @foreach ($player->characters as $character)
                                @if ($character->accepted)
                                    <option value="{{ $character->id }}" @if ($data['character_id'] == $character->id) selected @endif>{{ $character->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @else
                    <input type="hidden" name="character_id" value="0">
                @endif

                <div class="form-group">
                    <label for="table_id">Table</label>
                    <select name="table_id" class="form-control">
                        <option value="0" @if ($data['table_id'] == 0) selected @endif>No Table</option>
                        @foreach ($player->tables as $table)
                            <option value="{{ $table->id }}" @if ($data['table_id'] == $table->id) selected @endif>{{ $table->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="title" class="control-label">
                        Tag
                        @if ($category->type == 'ic')
                            Characters</label>

                    <select id="tags" name="tags[]" class="form-control autocomplete" multiple="multiple">
                        @foreach ($block->groups as $group)
                            <option value="g{{ $group->id }}">[{{ $group->name }}]</option>
                        @endforeach
                        @foreach ($block->characters as $character)
                            <option value="{{ $character->id }}">{{ $character->name }}</option>
                        @endforeach
                    </select>
                    @else
                        Players</label>

                        <select id="tags" name="tags[]" class="form-control autocomplete" multiple="multiple">
                            <option value="staff" @if ($data['tags'] && in_array('staff', $data['tags'])) selected @endif>[Staff]</option>
                            @foreach ($block->players as $user)
                                @if ($user->id != $player->id)
                                    <option value="{{ $user->id }}" @if ($data['tags'] && in_array($user->id, $data['tags'])) selected @endif>{{ $user->username }}</option>
                                @endif
                            @endforeach
                        </select>
                    @endif
                </div>

                @if ($category->type == 'ic')
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="mature" value="1" @if ($data['mature'] == 1) checked @endif> Mature Thread
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="private" value="1" @if ($data['private'] == 1) checked @endif> Private Thread
                            </label>
                        </div>
                    </div>
                @endif

                <div class="form-group{{ $errors->has('private') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea name="content" class="wysiwyg form-control">{{ old('content', $data['content']) }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" name="method" value="submit" class="btn btn-primary pull-right">Create</button>
                <button type="submit" name="method" value="preview" class="btn btn-default pull-right">Preview</button>
                <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.autocomplete').select2({
                placeholder: "Select players or characters to tag!",
            });
        });
    </script>
@endsection