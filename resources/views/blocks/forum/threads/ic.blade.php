<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        {{ $thread->title }}
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    <h1 class="inset">Thread Information</h1>

    Here is where general information will go such as any random events that might exist, tagged characters, or fight information.
</div>

@foreach ($thread->posts as $post)
    <div id="post-{{ $post->sequence }}" class="{{ $post->trashed() ? 'deleted' : '' }}" class="post-body">
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                {{ $thread->title }}
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">
                Posted {{ $post->created_at->format('m-d-Y G:i') }}

                <span class="pull-right">
                    <a href=""><i class="fa fa-flag"></i> Report</a>

                    @if ($player && ($post->character->player_id == $player->id || $player->isStaff()))
                        <a href="/post/edit/{{ $post->id }}"><i class="fa fa-pencil"></i> Edit</a>
                    @endif
                </span>
            </h1>

            <table class="post" id="pid{{ $post->id }}">
                <tr>
                    <td class="mini" style="
                    @if ($post->character->vista_unlocked)
                            background-image: url({{ $post->character->vista }});
                    @endif">
                        @include('blocks.partials.miniprofile_ic', ['block' => $block, 'character' => $post->character])
                    </td>
                    <td style="vertical-align: top;">
                        @if ($post->table) {!! $post->table->header !!} @endif
                        @if ($post->table) {!! str_replace(['{', '}'], [$post->table->pre, $post->table->post], htmlspecialchars_decode($post->content)) !!} @else {!! htmlspecialchars_decode($post->content) !!} @endif
                        @if ($post->table) {!! $post->table->footer !!} @endif

                        @if ($post->edited_by)
                            <small class="pull-right">
                                Edited {{ $post->edited_at->diffForHumans() }} by <a href="/player/{{ $post->editedPlayer->username }}">{{ $post->editedPlayer->username }}</a>: <i>{{ $post->edited_reason }}</i>
                                @if ($player && $player->isStaff())
                                    <br><a href="">View Original Content</a>
                                @endif
                            </small>
                        @endif
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endforeach