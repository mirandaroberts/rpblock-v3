@extends('layouts.block')

@section('title')
    Exit Thread
@endsection

@section('breadcrumbs')
    <a href="/category/{{ $thread->category->id }}">{{ $thread->category->title }}</a>
    <a href="/thread/{{ $thread->id }}">{{ $thread->title }}</a>
    Exit Thread
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Exit Thread {{ $thread->title }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Choose a character to exit this thread</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="tag">Character</label>
                    <select name="tag" class="form-control">
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}" @if (old('tag') == $tag->id) selected @endif>{{ $tag->character->name }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" name="method" class="btn btn-primary pull-right">Exit</button>
                <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
@endsection