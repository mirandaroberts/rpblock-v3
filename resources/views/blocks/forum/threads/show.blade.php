@extends('layouts.block')

@section('title')
    {{ $thread->title }}
@endsection

@section('breadcrumbs')
    @if ($thread->category->parent)
        @if ($thread->category->parent->parent)
            <a href="/category/{{ $thread->category->parent->parent->id }}">{{ $thread->category->parent->parent->title }}</a>
        @endif
        <a href="/category/{{ $thread->category->parent->id }}">{{ $thread->category->parent->title }}</a>
    @endif

    {{ $thread->title }}
@endsection

@section('content')
    <h2>
        @if ($thread->trashed())
            <span class="label label-danger">Deleted</span>
        @endif

        @if ($thread->locked)
            <span class="label label-warning">Locked</span>
        @endif

        @if ($thread->pinned)
            <span class="label label-info">Pinned</span>
        @endif

        @if ($thread->archived)
            <span class="label label-default">Archived</span>
        @endif

        {{ $thread->title }}
    </h2>

    @if (isset($player))
        <div class="btn-group pull-right" role="group">
            @if (!$thread->locked && !$thread->archive)
                <a href="/thread/{{ $thread->id }}/newpost" class="btn btn-primary">Post Reply</a>
                @if ($thread->category->type == 'ooc')
                    <a href="#quick-reply" class="btn btn-primary">Quick Reply</a>
                @endif
                <a href="/tags/thread/{{ $thread->id}}" class="btn btn-primary">Show Tags</a>
                @if ($thread->category->type == 'ic' && $player->checkAllTags($thread->id) == true)
                    <a href="/thread/{{ $thread->id}}/exit" class="btn btn-primary">Exit Thread</a>
                @endif
            @endif
        </div>
    @endif

    <br class="clear">

    @if ($thread->category->type == 'ooc')
        @include('blocks.forum.threads.ooc', ['thread' => $thread, 'player' => $player])
    @else
        @if ($thread->mature)
            <span class="prefix" title="mature">M</span>
        @endif

        @if ($thread->private)
            <span class="prefix" title="private">P</span>
        @else
            <span class="prefix" title="all welcome">AW</span>
        @endif

        @include('blocks.forum.threads.ic', ['thread' => $thread, 'player' => $player])
    @endif

    @if (isset($player) && $thread->category->threads_enabled && !$thread->locked && !$thread->archived)
        <div class="text-right">
            <a href="/thread/{{ $thread->id }}/newpost" class="btn btn-primary">Post Reply</a>
        </div>

        @if ($thread->category->type == 'ooc')
            <h3>Quick Reply</h3>
            <div id="quick-reply">
                <form method="POST" action="">
                    {!! csrf_field() !!}
                    <input type="hidden" name="character_id" value="0">
                    <input type="hidden" name="table_id" value="0">

                    <div class="form-group">
                        <textarea name="content" class="wysiwyg form-control">{{ old('content') }}</textarea>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary pull-right">Reply</button>
                    </div>
                </form>
            </div>
        @endif
    @endif
@endsection