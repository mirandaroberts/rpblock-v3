@extends('layouts.block')

@section('title')
    Archives
@endsection

@section('breadcrumbs')
    Archives
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Archives

            @if ($filter)
                 - {{ $filter->title }}
            @endif
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Ordered by date archived</h1>
        <div class="text-center">
            @if ($filter)
                Filtering by Category: {{ $filter->title }}<br><br>
            @endif

            <form class="form-horizontal" role="form" method="GET" action="">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="filter" class="col-md-4 control-label">Filter by Category</label>

                    <div class="col-md-6">
                        <select name="filter" id="filter" class="form-control">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Filter Archives
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <table class="table-responsive table table-striped">
            <thead>
            <tr>
                <th>Thread</th>
                <th>Location</th>
                <th>Post Count</th>
                <th>Started By</th>
                <th>Started At</th>
                <th>Archived At</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($archives as $thread)
                <tr>
                    <td><a href="/thread/{{ $thread->id }}">{{ $thread->title }}</a></td>
                    <td>{{ $thread->category->title }}</td>
                    <td>{{ $thread->reply_count }}</td>
                    <td><a href="/character/{{ $thread->character->name }}">{{ $thread->character->name }}</a></td>
                    <td>{{ $thread->created_at->format('m-d-Y') }}</td>
                    <td>{{ $thread->archived_at->format('m-d-Y') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $archives->links() }}
    </div>
@endsection