@extends('layouts.block')

@section('title')
    {{ $category->title }}
@endsection

@section('breadcrumbs')
    @if ($category->parent)
        @if ($category->parent->parent)
            <a href="/category/{{ $category->parent->parent->id }}">{{ $category->parent->parent->title }}</a>
        @endif
        <a href="/category/{{ $category->parent->id }}">{{ $category->parent->title }}</a>
    @endif

    {{ $category->title }}
@endsection

@section('content')
    @if ($category->children->count() > 0)
        <div class="col-md-12 col-sm-12 col-xs-12 forum-category rounded top lpad">
            <div class="col-md-10 col-sm-10 col-xs-10">
                Subcategories
            </div>
        </div>

        <div class="toggleview">
            <div class="col-md-12 col-sm-12 col-xs-12 forum-head">
                <div class="col-md-7 col-sm-5 col-xs-5 lpad">Topic</div>
                <div class="col-md-1 col-sm-2 col-xs-2 lpad">Threads</div>
                <div class="col-md-1 col-sm-2 col-xs-2 lpad">Posts</div>
                <div class="col-md-3 col-sm-3 col-xs-3 lpad">Freshness</div>
            </div>

            @foreach ($category->children as $sub)
                @if ($category->type == 'ooc')
                    @if (!$sub->private || ($player && $player->isStaff()))
                        <div class="col-md-12 col-sm-12 col-xs-12 forum-topic">
                            <div class="col-md-1 col-sm-1 hidden-xs lpad forum-icon">
                                <i class="fa fa-{{ $sub->getImg() }}"></i>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 lpad">
                            <span class="overflow-control">
                                <a href="/category/{{ $sub->id }}">{{ $sub->title }}</a>
                            </span>
                                <span>
                                {{ $sub->description }}
                            </span>
                            </div>

                            <div class="col-md-1 col-sm-1 col-xs-1 lpad">
                                <span class="center">{{ $sub->thread_count }}</span>
                            </div>

                            <div class="col-md-1 col-sm-1 col-xs-1 lpad">
                                <span class="center">{{ $sub->post_count }}</span>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 lpad">
                                @if ($sub->latestThread)
                                    <span><a href="/thread/{{ $sub->latestThread->id }}">{{ $sub->latestThread->title }}</a> by
                                    @if ($sub->latestThread->lastPost)
                                        <a href="/player/{{ $sub->latestThread->lastPost->player->username }}">{{ $sub->latestThread->lastPost->player->username }}</a></span>
                                    @else
                                        <a href="/player/{{ $sub->latestThread->player->username }}">{{ $sub->latestThread->player->username }}</a></span>
                                    @endif
                                    <span>{{ $sub->latestThread->updated_at->diffForHumans() }}</span>
                                @else
                                    <span>None</span>
                                @endif
                            </div>
                        </div>
                    @endif
                @else
                    <div class="col-md-12 col-sm-12 col-xs-12 forum-topic">
                        <div class="col-md-7 col-sm-12 col-xs-12 forum-img">
                            <img src="{{ $sub->getImg() }}" width="675" height="71" class="cat-img">
                            <span><a href="/category/{{ $sub->id }}">{{ $sub->title }}</a></span>

                            <br>
                        </div>

                        <div class="col-md-1 col-sm-4 col-xs-4 lpad">
                            <span class="center">{{ $sub->thread_count }}</span>
                        </div>

                        <div class="col-md-1 col-sm-4 col-xs-4 lpad">
                            <span class="center">{{ $sub->post_count }}</span>
                        </div>

                        <div class="col-md-3 col-sm-4 col-xs-4 lpad">
                            @if ($sub->latestThread)
                                <span><a href="/thread/{{ $sub->latestThread->id }}">{{ $sub->latestThread->title }}</a> by <a href="/character/{{ $sub->latestThread->lastPost->character->name }}">{{ $sub->latestThread->lastPost->character->name }}</a></span>
                                <span>{{ $sub->latestThread->updated_at->diffForHumans() }}</span>
                            @else
                                <span>None</span>
                            @endif
                        </div>
                    </div>

                    <div class="sub-bar col-md-12 col-sm-12 col-xs-12">
                        @foreach ($sub->children as $child)
                            <a href="/category/{{ $child->id }}" class="sub-button underline">{{ $child->title }}</a>
                        @endforeach
                    </div>
                @endif
            @endforeach
        </div>
    @endif

    @if ($category->enable_threads)
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                {{ $category->title }}
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">{{ $category->description }}</h1>

            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Thread Title</th>
                        <th>Started By</th>
                        <th>Started Date</th>
                        <th>Replies</th>
                        <th>Views</th>
                        <th>Last Post</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($category->threads as $thread)
                        @if (!$thread->archived && !$thread->deleted)
                            <tr>
                                <td><a href="/thread/{{ $thread->id }}">{{ $thread->title }}</a></td>
                                <td>
                                    @if ($category->type == 'ooc')
                                        <a href="/player/{{ $thread->player->username }}">{{ $thread->player->username }}</a>
                                    @else
                                        <a href="/character/{{ $thread->character->name }}">{{ $thread->character->name }}</a>
                                    @endif
                                </td>
                                <td>{{ $thread->created_at->format('m-d-Y') }}</td>
                                <td>{{ $thread->reply_count }}</td>
                                <td>0</td>
                                <td>
                                    @if ($thread->lastPost)
                                        @if ($category->type == 'ooc')
                                            By <a href="/player/{{ $thread->lastPost->player->username }}">{{ $thread->lastPost->player->username }}</a> {{ $thread->lastPost->created_at->diffForHumans() }}
                                        @else
                                            By <a href="/character/{{ $thread->lastPost->character->name }}">{{ $thread->lastPost->character->name }}</a> {{ $thread->lastPost->created_at->diffForHumans() }}
                                        @endif
                                    @else
                                        No Replies
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        @if (isset($player))
            <div class="text-right">
                <a href="/thread/{{ $category->id }}/create" class="btn btn-primary">Create Thread</a>
            </div>
        @endif
    @else
        <div class="text-center"><i>Threads are disabled.</i></div>
    @endif
@endsection