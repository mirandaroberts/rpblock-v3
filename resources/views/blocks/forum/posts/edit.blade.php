@extends('layouts.block')

@section('title')
    Edit Reply
@endsection

@section('breadcrumbs')
    <a href="/category/{{ $post->thread->category->id }}">{{ $post->thread->category->title }}</a>
    <a href="/thread/{{ $post->thread->id }}">{{ $post->thread->title }}</a>
    Edit Reply
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Edit Reply
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Editing reply in {{ $post->thread->title }}</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                @if ($post->thread->category->type == 'ic')
                    <div class="form-group">
                        <label for="character_id">Character</label>
                        <select name="character_id" class="form-control">
                            @foreach ($post->player->characters as $character)
                                @if ($character->accepted)
                                    <option value="{{ $character->id }}" @if ($post->character_id == $character->id) selected @endif>{{ $character->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @else
                    <input type="hidden" name="character_id" value="0">
                @endif

                <div class="form-group">
                    <label for="table_id">Table</label>
                    <select name="table_id" class="form-control">
                        <option value="0" @if ($post->table_id == 0) selected @endif>No Table</option>
                        @foreach ($post->player->tables as $table)
                            <option value="{{ $table->id }}" @if ($post->table_id == $table->id) selected @endif>{{ $table->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ $errors->has('birth_month') ? ' has-error' : '' }}">
                    <label for="reason">Reason (Required)</label>
                    <input type="text" name="reason" id="reason" class="form-control" value="{{ old('reason') }}">

                    @if ($errors->has('reason'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reason') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea name="content" class="wysiwyg form-control">{{ old('content', $post->content) }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" name="method" value="submit" class="btn btn-primary pull-right">Edit</button>
                <a href="/thread/{{ $post->thread->id }}" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
@endsection