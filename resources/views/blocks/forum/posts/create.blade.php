@extends('layouts.block')

@section('title')
    Post Reply
@endsection

@section('breadcrumbs')
    <a href="/category/{{ $thread->category->id }}">{{ $thread->category->title }}</a>
    <a href="/thread/{{ $thread->id }}">{{ $thread->title }}</a>
    Post Reply
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Post Reply
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Create new post in {{ $thread->title }}</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                @if ($thread->category->type == 'ic')
                    <div class="form-group">
                        <label for="character_id">Character</label>
                        <select name="character_id" class="form-control">
                            @foreach ($player->characters as $character)
                                @if ($character->accepted)
                                    <option value="{{ $character->id }}">{{ $character->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @else
                    <input type="hidden" name="character_id" value="0">
                    <input type="hidden" name="table_id" value="0">
                @endif

                <div class="form-group">
                    <label for="table_id">Table</label>
                    <select name="table_id" class="form-control">
                        <option value="0" @if (old('table_id') == 0) selected @endif>No Table</option>

                        <optgroup label="OOC/Global Tables">
                            @foreach ($player->tables as $table)
                                @if ($table->character_id == 0)
                                    <option value="{{ $table->id }}" @if (old('table_id') == $table->id) selected @endif>{{ $table->name }}</option>
                                @endif
                            @endforeach
                        </optgroup>
                        @foreach ($player->characters as $character)
                            @if ($character->accepted && !$character->inactive)
                                <optgroup label="{{ $character->name }}">
                                    @foreach ($player->tables as $table)
                                        @if ($table->character_id == $character->id)
                                            <option value="{{ $table->id }}" @if (old('table_id') == $table->id) selected @endif>{{ $table->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea name="content" class="wysiwyg form-control">{{ old('content') }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" name="method" value="submit" class="btn btn-primary pull-right">Create</button>
                <button type="submit" name="method" value="preview" class="btn btn-default pull-right">Preview</button>
                <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>

    @foreach($thread->postHistory as $post)
        <div id="post-{{ $post->sequence }}" class="{{ $post->trashed() ? 'deleted' : '' }} post-body">
            <div class="col-md-12 forum-category rounded top lpad">
                <div class="col-md-10">
                    {{ $thread->title }}
                </div>
            </div>

            <div class="col-md-12 toggleview normal lpad">
                <h1 class="inset">
                    Posted {{ $post->created_at->format('m-d-Y G:i') }}

                    <span class="pull-right">
                    <a href=""><i class="fa fa-flag"></i> Report</a>

                        @if ($player && ($post->character->player_id == $player->id || $player->isStaff()))
                            <a href="/post/edit/{{ $post->id }}"><i class="fa fa-pencil"></i> Edit</a>
                        @endif
                </span>
                </h1>

                <table class="post" id="pid{{ $post->id }}">
                    <tr>
                        <td class="mini" style="
                        @if ($post->character->vista_unlocked)
                                background-image: url({{ $post->character->vista }});
                        @endif">
                            @include('blocks.partials.miniprofile_ic', ['block' => $block, 'character' => $post->character])
                        </td>
                        <td style="vertical-align: top;">
                            @if ($post->table) {!! $post->table->header !!} @endif
                            @if ($post->table) {!! str_replace(['{', '}'], [$post->table->pre, $post->table->post], htmlspecialchars_decode($post->content)) !!} @else {!! htmlspecialchars_decode($post->content) !!} @endif
                            @if ($post->table) {!! $post->table->footer !!} @endif

                            @if ($post->edited_by)
                                <small class="pull-right">
                                    Edited {{ $post->edited_at->diffForHumans() }} by <a href="/player/{{ $post->editedPlayer->username }}">{{ $post->editedPlayer->username }}</a>: <i>{{ $post->edited_reason }}</i>
                                    @if ($player && $player->isStaff())
                                        <br><a href="">View Original Content</a>
                                    @endif
                                </small>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    @endforeach
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.autocomplete').select2({
                placeholder: "Select players or characters to tag!",
            });
        });
    </script>
@endsection