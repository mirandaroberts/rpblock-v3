@import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Open+Sans+Condensed:300,700);

body {
    position: relative;
    margin: 0;
    padding-bottom: 6rem;
    min-height: 100%;
    font-family: 'Open Sans', sans-serif;
    background-color: <?php echo $skin->bg_color; ?>
    color: <?php echo $skin->font_color; ?>;
}

header {
    background-image: url('<?php echo $skin->header_bg; ?>');
    background-color: <?php echo $skin->secondary_color; ?>;
    margin: 0;
    border-bottom: 3px solid <?php echo $skin->accent_color; ?>;
}

a {
    display: inline-block;
    color: <?php echo $skin->link_color; ?>;
    text-decoration: none;
}

a:hover {
    color: <?php echo $skin->link_hover; ?>;
}

a:visited {
    color: <?php echo $skin->link_visited; ?>;
}

a:active {
    color: <?php echo $skin->link_active; ?>;
}

html {
    height: 100%;
    box-sizing: border-box;
}

*,
*:before,
*:after {
    box-sizing: inherit;
}

footer {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 15px 10px 10px 10px;
    background-color: <?php echo $skin->secondary_color; ?>;
    border-top: 3px solid <?php echo $skin->accent_color; ?>;
    height: 50px;
    color: <?php echo $skin->secondary_font; ?>;
    font-size: 12px;
}

footer a {
    color: <?php echo $skin->accent_color; ?>;
}

.lpad {
    padding: 15px;
}

.banner {
    background-image: url('<?php echo $skin->banner; ?>');
    background-size: 100%;
    height: 500px;
    border-radius: 3px;
    margin-top: 20px;
}

#searchform {
    display:inline;
    font-size:1em;
    border-radius: 8em;
    border:0.1em solid <?php echo $skin->accent_color; ?>;
    box-shadow:0 0 0.3em rgba(60,60,60,0.4);
    padding:0.3em;
    background: transparent;
}

#s {
    transition:all 0.2s ease-out;
    width:1px;
    border-radius:0;
    box-shadow:none;
    outline: none;
    padding:0;
    border:0;
    background-color: transparent;
    opacity:0;
    margin-top: 13px;
}

#s:focus {
    width:8em;
    opacity:1;
}

#searchform label {
    padding-left: 1px;
    display:inline-block;
    margin-top:0.3em;
    color: rgba(70, 187, 226, 1);
    text-shadow:0 0 0.1em <?php echo $skin->accent_colot; ?>;
    position: relative;
    left:0.1em;
}

.navbar-brand {
    color: <?php echo $skin->secondary_font; ?>;
    font-size: 18px;
    text-transform: lowercase;
    font-weight: 400;
}

.navbar-default {
    background-color: transparent;
    border: none;
    padding: 0;
    margin: 0;
}

.navbar-default .navbar-nav>.active>a,
.navbar-default .navbar-nav>.active>a:hover,
.navbar-default .navbar-nav>.active>a:focus {
    color: <?php echo $skin->secondary_font; ?>;
    background-color: transparent;
}

.navbar .navbar-default a {
    margin: 0 7px;
    color: <?php echo $skin->navbar_links; ?>;
    font-size: 16px;
    font-weight: 600;
    text-decoration: none;
}

#nav > ul > li > a:hover {
    color: <?php echo $skin->navbar_hover; ?>;
}

.navbar .navbar-default a:last-child {
    margin: 7px 0 7px 7px;
}

.navbar .navbar-default a.current {
    color: <?php echo $skin->navbar_hover; ?>;
}

.top-msg {
    border-bottom: <?php echo $skin->breadcrumbs_border; ?>;
    color: <?php echo $skin->breadcrumbs; ?>;
    font-size: 14px;
    font-weight: 300;
    margin: 0;
    padding: 10px;
}

.row {
    margin-left: 20px;
    margin-right: 20px;
}

.breadcrumb a {
    transition: color .5s;
}

.breadcrumb a:hover {
    color: <?php echo $skin->breadcrumbs_hover; ?>;
    text-decoration: none;
}

.breadcrumb a:after {
    content: '\00a0\00a0\002F\00a0\00a0';
    color: <?php echo $skin->breadcrumbs_links; ?>;
}

a.primary {
    color: <?php echo $skin->navbar_links; ?>;
    transition: color .5s;
}

a.primary:hover {
    color: <?php echo $skin->accent_color; ?>;
}

a.underline {
    color: <?php echo $skin->navbar_links; ?>;
    text-decoration: none;
}

a.underline:hover:after {
    content: '';
    display: block;
    width: inherit;
    height: 2px;
    background-color: <?php echo $skin->accent_color; ?>;
    margin-bottom: -2px;
    animation: link .9s ease;
}

@keyframes link {
    from { width: 0; }
    to   { width: 100%;}
}

.table a {
    transition: color .5s;

    color: <?php echo $skin->accent_color; ?>;
    font-weight: 600;
}

.table a:hover {
    color: <?php echo $skin->accent_dark; ?>;
}

/* Topic */
.rounded {
    overflow: hidden;
}

.rounded.top {
    border-radius: 4px 4px 0 0;
}

.rounded.all {
    border-radius: 4px;
}

.forum-category {
    background-color: <?php echo $skin->accent_color; ?>;
    margin-top: 20px;
    color: <?php echo $skin->accent_font; ?>;
    font-weight: 600;
    font-size: 13px;
    text-shadow: 0 1px 1px <?php echo $skin->accent_dark; ?>;
}

.forum-head {
    background-color: <?php echo $skin->secondary_color; ?>;
    border-right: 1px solid <?php echo $skin->secondary_color; ?>;
    border-left: 1px solid <?php echo $skin->secondary_color; ?>;

    color: <?php echo $skin->secondary_font; ?>;
    font-weight: 300;
    font-size: 12px;
    text-shadow: 0 1px 1px hsl(0, 0%, 0%);
    text-align: center;
}

.forum-head:first-child {
    text-align: left;
    border-left: none;
}

.forum-head:last-child {
    border-right: none;
}

.forum-topic {
    min-height: 71px;
    max-height: 71px;
    background-color: <?php echo $skin->catgory_color; ?>;
    border-bottom: 1px solid <?php echo $skin->category_border; ?>;
    color: <?php echo $skin->category_font; ?>;
    font-size: 12px;
}

.forum-icon {
    color: <?php echo $skin->category_icon; ?>;
    font-size: 30px;
    text-align: center;
}

.forum-topic:last-child {
    text-align: left;
}

.forum-topic a {
    transition: color .5s;

    color: <?php echo $skin->accent_color; ?>;
    font-weight: 600;
}

.forum-topic a:hover {
    color: <?php echo $skin->accent_darker; ?>;
}

.forum-topic span {
    display: block;
    margin: 0 0 2px 0;
}

.overflow-control {
    white-space: nowrap;
    overflow: hidden;
    text-overflow:ellipsis;
}

.forum-topic:nth-child(2) span.overflow-control {
    width: 70%;
    height: 15px;
}

.forum-topic span.center {
    padding-top: 10px;
}

.normal {
    background-color: <?php echo $skin->category_color; ?>;
    color: <?php echo $skin->category_font; ?>;
    font-size: 12px;
}

.normal h1.inset {
    background-color: <?php echo $skin->secondary_color; ?>;
    margin: -20px -15px 20px -15px;

    color: <?php echo $skin->secondary_font; ?>;
    padding: 15px 20px 15px 20px;
    font-size: 12px;
    font-weight: 300;
}

.normal p {
    margin: 0 0 40px 0;
    line-height: 20px;
}

.normal p:last-child {
    margin: 0;
}

.forum-img {
    margin-left: -30px;
    margin-right: 30px;
    position: relative;
}

.forum-img span {
    position: absolute;
    top: 10px;
    left: 30px;
    color: #fff;
    background-color: rgba(0,0,0,0.5);
    padding: 5px;
}

#top-button {
    display: none;
    width: 27px;
    background-color: <?php echo $skin->secondary_color; ?>;
    position: fixed;
    right: 20px;
    bottom: 20px;
    border-radius: 3px;
    cursor: pointer;
    z-index: 999 !important;

    padding: 5px;
    text-align: center;
    color: hsl(0, 0%, 100%);
}

#top-button.show {
    display: block;
}

.group-list {
    padding: 0;
    list-style-type: none;
}

.group-list li a {
    color: #fff;
    width: 100%;
}

.group-list li a:hover {
    color: #fff;
    text-decoration: none;
}

.group-list li {
    color: #fff;
    font-size: 16px;
    padding: 15px;
    display: block;
}

.group-list li:first-child {
    border-radius: 3px 3px 0 0;
}

.group-list li:last-child {
    border-radius: 0 0 3px 3px;
}

.btn {
    padding: 14px 24px;
    border: 0 none;
    font-weight: 700;
    letter-spacing: 1px;
    text-transform: uppercase;
}

.btn:focus, .btn:active:focus, .btn.active:focus {
    outline: 0 none;
}

.btn-primary {
    background: <?php echo $skin->accent_color; ?>;
    color: <?php echo $skin->accent_font; ?>;
}

.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
    background: <?php echo $skin->accent_dark; ?>;
}

.btn-primary:active, .btn-primary.active {
    background: <?php echo $skin->secondary_dark; ?>;
    box-shadow: none;
}

.sub-bar {
    width: 100%;
    height: 35px;
    background-color: <?php echo $skin->category_color; ?>;
}

.sub-button {
    padding: 7px 5px 5px 5px;
    color: <?php echo $skin->secondary_font; ?> !important;
    margin-right: 20px;
}

.sub-button:last-child {
    margin-right: 0;
}

.top-content {
    margin-top: 30px !important;
}

.blocks {
    width: 100%;
    height: 500px;
    max-width: 100%;
    max-height: 100%;
}

.user-stats {
    flex: 1;
    text-align: center;
    margin-top: 10px;
}

.user-stats strong {
    display: block;
    float: left;
    clear: both;
    width: 100%;
    color: #B3B1B2;
    font-size: 12px;
    font-weight: 100;
    letter-spacing: -0.2px;
}

.user-stats span {
    font-size: 18px;
    color: #5E5E5E;
    padding: .18em 0;
    display: inline-block;
}

/* Fancy box links in player dashboard */
[class^="link-"] {
display: inline-block;
margin: 2em
}

.panel-link ul {
margin:0px;
padding:0px;
list-style-type:none;
-webkit-backface-visibility: hidden; backface-visibility: hidden;
}

.var_nav {
position:relative;
background:#f1f1f1;
height:70px;
margin-bottom:5px;
}

.link_bg {
width:70px;
height:70px;
position:absolute;
background:hsl(206, 35%, 13%);
color:#636b6f;
z-index:2;
}

.link_bg i {
position:relative;
}

.link_title {
position:absolute;
width:100%;
z-index:3;
color:#636b6f;
}

.link_title:hover .icon {
-webkit-transform:rotate(360deg);
-moz-transform:rotate(360deg);
-o-transform:rotate(360deg);
-ms-transform:rotate(360deg);
transform:rotate(360deg);
}

.var_nav:hover .link_bg {
width:100%;
background:hsl(206, 35%, 13%);
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}

.var_nav:hover a {
font-weight: bold;
color: #fff;
-webkit-transition: all .5s ease-in-out;
-moz-transition: all .5s ease-in-out;
-o-transition: all .5s ease-in-out;
-ms-transition: all .5s ease-in-out;
transition: all .5s ease-in-out;
}

.panel-link .icon {
position: relative;
width: 70px;
height: 70px;
text-align: center;
color: #fff;
-webkit-transition: all .5s ease-in-out;
-moz-transition: all .5s ease-in-out;
-o-transition: all .5s ease-in-out;
-ms-transition: all .5s ease-in-out;
float: left;
transition: all .5s ease-in-out;
float: left;
}

.panel-link .icon i{top:22px;position:relative;}

.panel-link a {
display:block;
position:absolute;
float:left;
font-family:arial;
color:#636b6f;
text-decoration:none;
width:100%;
height:70px;
text-align:center;
}

.panel-link span {
margin-top:25px;
display:block;
}

.theme-switcher {
background-color: transparent;
border: 1px solid #0099cc;
color: #ffffff;
}

.progress {
width: 80%;
margin: auto;
}

.avatar {
height: auto;
width: auto;
max-width: 200px;
max-height: 300px;
}

img {
max-width: 100% !important;
}

.thumbnail a:hover, .thumbnail:hover {
text-decoration: none;
}

@media only screen and (max-width: 768px) {

}
