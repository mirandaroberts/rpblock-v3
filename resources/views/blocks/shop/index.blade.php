@extends('layouts.block')

@section('title')
    Shop
@endsection

@section('breadcrumbs')
    Shop
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Shop
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Spend your {{ $block->settings->currency_name }} here!
        </h1>

        @foreach ($items as $item)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <form action="/shop/buy/{{ $item->id }}" method="post">
                        {{ csrf_field() }}
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ ucfirst($item->name) }}</h4><br>
                                <img src="{{ $item->icon }}" width="100" height="100" alt="{{ $item->name }}"><br>
                                <b>{{ strtoupper($item->type) }} Item</b><br>
                                {{ $item->description }}<br><br>
                                @if ($item->type == 'ic')
                                    <label for="character">Select Character</label>
                                    <select name="character_id" id="character" class="form-control">
                                        @foreach ($player->characters as $character)
                                            <option value="{{ $character->id }}">{{ $character->name }}</option>
                                        @endforeach
                                    </select><br>
                                @endif
                                <button type="submit" class="btn btn-success">Buy ({{ $item->cost }} {{ $block->settings->currency_name }})</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection