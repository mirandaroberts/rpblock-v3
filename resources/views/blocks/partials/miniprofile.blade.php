<div class="sidebar-miniprofile">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-sm-12 text-center">
            <a href="/player/{{ $player->username }}">
                <img src="{{ $player->icon }}" alt="Your Avatar" class="avatar">
            </a>
        </div>

        <div class="col-md-12 col-sm-12 col-sm-12 text-left">
            <table width="100%" class="table table-striped">
                @if ($block->settings->enable_currency)
                    <tr>
                        <td><b>{{ ucfirst($block->settings->currency_name) }}</b></td>
                        <td>{{ $player->currency }}</td>
                    </tr>
                @endif

                <tr>
                    <td><b>OOC Posts</b></td>
                    <td>{{ $player->oocPosts() }}</td>
                </tr>

                <tr>
                    <td><b>IC Posts</b></td>
                    <td>{{ $player->icPosts() }}</td>
                </tr>

                <tr>
                    <td><b>Characters</b></td>
                    <td>{{ $player->characters->count() }}/{{ $player->character_slots }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="user-stats">
                <strong>Messages</strong>
                <span><a href="/conversations"><i class="fa fa-envelope"></i> 0</a></span>
            </div>
        </div>

        <div class="col-md-4">
            <div class="user-stats">
                <strong>Notices</strong>
                <span><a href="/notifications"><i class="fa fa-flag"></i> {{ $player->unreadNotifications->count() }}</a></span>
            </div>
        </div>

        <div class="col-md-4">
            <div class="user-stats">
                <strong>Requests</strong>
                <span><i class="fa fa-question"></i> 0</span>
            </div>
        </div>
    </div>
</div>
