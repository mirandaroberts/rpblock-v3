@foreach ($categories as $category)
    @if ($category->type == 'ic' && !$category->parent)
        <div class="col-md-12 col-sm-12 col-xs-12 forum-category rounded top lpad">
            <div class="col-md-10 col-sm-10 col-xs-10">
                {{ $category->title }}
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                <a data-connect>
                    <i class="fa fa-caret-down"></i>
                </a>
            </div>
        </div>

        @foreach ($category->children as $sub)
            <div class="toggleview">
                <div class="col-md-12 col-sm-12 col-xs-12 forum-head">
                    <div class="col-md-7 hidden-sm hidden-xs lpad">Area</div>
                    <div class="col-md-1 col-sm-4 col-xs-4 lpad">Threads</div>
                    <div class="col-md-1 col-sm-4 col-xs-4 lpad">Posts</div>
                    <div class="col-md-3 col-sm-4 col-xs-4 lpad">Freshness</div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 forum-topic">
                    <div class="col-md-7 col-sm-12 col-xs-12 forum-img">
                        <img src="{{ $sub->getImg() }}" width="675" height="71" class="cat-img">
                        <span><a href="/category/{{ $sub->id }}">{{ $sub->title }}</a></span>

                        <br>
                    </div>

                    <div class="col-md-1 col-sm-4 col-xs-4 lpad">
                        <span class="center">{{ $sub->threadCount() }}</span>
                    </div>

                    <div class="col-md-1 col-sm-4 col-xs-4 lpad">
                        <span class="center">{{ $sub->postCount() }}</span>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-4 lpad">
                        @if ($sub->latestThread())
                            <span><a href="/thread/{{ $sub->latestThread()->id }}">{{ $sub->latestThread()->title }}</a> by <a href="/character/{{ $sub->latestThread()->lastPost->character->name }}">{{ $sub->latestThread()->lastPost->character->name }}</a></span>
                            <span>{{ $sub->latestThread()->updated_at->diffForHumans() }}</span>
                        @else
                            <span>None</span>
                        @endif
                    </div>
                </div>

                <div class="sub-bar col-md-12 col-sm-12 col-xs-12">
                    @foreach ($sub->children as $child)
                        <a href="/category/{{ $child->id }}" class="sub-button underline">{{ $child->title }}</a>
                    @endforeach
                </div>
            </div>
        @endforeach
    @endif
@endforeach