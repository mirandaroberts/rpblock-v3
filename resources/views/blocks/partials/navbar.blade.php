<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">{{ ucfirst($block->name) }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form role="search" id="searchform" action="/sitesearch" method="post">
						{!! csrf_field() !!}
                        <label for="s">
                            <i class="fa fa-search"></i>
                        </label>
                        <input type="text" name="q" value="" placeholder="search" class="" id="s" />
                    </form>
                </li>
                <li><a href="/" class="underline">Forum</a></li>
                <li><a href="/players" class="underline">Players</a></li>
                <li><a href="{{ route('showCharacters', ['block' => $block->name, 'name' => NULL]) }}" class="underline">Characters</a></li>
                <li><a href="/groups" class="underline">{{ ucfirst($block->settings->group_name) }}s</a></li>
                <li><a href="/guidebook" class="underline">Guidebook</a></li>

                @if (isset($player))
                    @if ($block->settings->enable_currency)<li><a href="/shop" class="underline">Shop</a></li>@endif
                    <li><a href="/dashboard" class="underline">Dashboard</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>