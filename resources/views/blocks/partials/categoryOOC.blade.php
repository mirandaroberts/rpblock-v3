@foreach ($categories as $category)
    @if ($category->type == 'ooc' && !$category->parent)
        <div class="col-md-12 col-sm-12 col-xs-12 forum-category rounded top lpad">
            <div class="col-md-10 col-sm-10 col-xs-10">
                {{ $category->title }}
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                <a data-connect>
                    <i class="fa fa-caret-down"></i>
                </a>
            </div>
        </div>

        <div class="toggleview">
            <div class="col-md-12 col-sm-12 col-xs-12 forum-head">
                <div class="col-md-7 col-sm-5 col-xs-5 lpad">Topic</div>
                <div class="col-md-1 col-sm-2 col-xs-2 lpad">Threads</div>
                <div class="col-md-1 col-sm-2 col-xs-2 lpad">Posts</div>
                <div class="col-md-3 col-sm-3 col-xs-3 lpad">Freshness</div>
            </div>

            @foreach ($category->children as $sub)
                @if (!$sub->private || ($player && $player->isStaff()))
                    <div class="col-md-12 col-sm-12 col-xs-12 forum-topic">
                        <div class="col-md-1 col-sm-1 hidden-xs lpad forum-icon">
                            <i class="fa fa-{{ $sub->getImg() }}"></i>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 lpad">
                            <span class="overflow-control">
                                <a href="/category/{{ $sub->id }}">{{ $sub->title }}</a>
                            </span>
                            <span>
                                {{ $sub->description }}
                            </span>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-1 lpad">
                            <span class="center">{{ $sub->threadCount() }}</span>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-1 lpad">
                            <span class="center">{{ $sub->postCount() }}</span>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3 lpad">
                            @if ($sub->latestThread())
                                <span><a href="/thread/{{ $sub->latestThread()->id }}">{{ $sub->latestThread()->title }}</a> by <a href="/player/{{ $sub->latestThread()->lastPost->player->username }}">{{ $sub->latestThread()->lastPost->player->username }}</a></span>
                                <span>{{ $sub->latestThread()->updated_at->diffForHumans() }}</span>
                            @else
                                <span>None</span>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
@endforeach