<div class="col-md-12 forum-category rounded top lpad">
    <span>Forum Statistics</span>
</div>
<div class="col-md-12 normal lpad">
    <h1 class="inset">Who's online</h1>
    <p>
        <b>{{ $block->onlinePlayers->count() }}</b> Player(s) Online Now:<br>
        @foreach ($block->onlinePlayers as $i => $p)
            @if ($i == $block->onlinePlayers->count() - 1)
                @if ($p->role == 'ADMIN')
                    <i class="fa fa-star"></i>
                @elseif ($p->role == 'STAFF')
                    <i class="fa fa-star-o"></i>
                @endif

                <a href="/player/{{ $p->username }}">{{ $p->username }}</a>
            @else
                @if ($p->role == 'ADMIN')
                    <i class="fa fa-star"></i>
                @elseif ($p->role == 'STAFF')
                    <i class="fa fa-star-o"></i>
                @endif

                <a href="/player/{{ $p->username }}">{{ $p->username }}</a>,
            @endif
        @endforeach

        <br>

        <b>{{ $block->recentPlayers->count() }}</b> Player(s) Online Today:<br>
        @foreach ($block->recentPlayers as $i => $p)
            @if ($i == $block->recentPlayers->count() - 1)
                @if ($p->role == 'ADMIN')
                    <i class="fa fa-star"></i>
                @elseif ($p->role == 'STAFF')
                    <i class="fa fa-star-o"></i>
                @endif

                <a href="/player/{{ $p->username }}">{{ $p->username }}</a>
            @else
                @if ($p->role == 'ADMIN')
                    <i class="fa fa-star"></i>
                @elseif ($p->role == 'STAFF')
                    <i class="fa fa-star-o"></i>
                @endif

                <a href="/player/{{ $p->username }}">{{ $p->username }}</a>,
            @endif
        @endforeach
    </p>
    <table width="100%">
        <tr>
            <td width="60%"><h1 class="inset">Affiliates</h1></td>
            <td width="40%"><h1 class="inset">Chatbox</h1></td>
        </tr>
        <tr>
            <td valign="top">
                <p>
                    <a href="http://baraenor-rpg.com/" target="_blank">
                        <img src="https://i.imgur.com/2yKJ7pZ.png" title="Baraenor" alt="Baraenor">
                    </a>
                    <span title="Legends of Tomorrow RPG!">
                        <a href="https://legendsoftomorrow.icyboards.net/index.php" target="_blank">
                            <img src="https://images2.imgbox.com/41/0d/mlhM3VZO_o.png">
                        </a>
                    </span>
                    <a href="" target="_blank">
                        <img src="https://images2.imgbox.com/f4/2d/PKD1aSVJ_o.png" title="Open Sister" alt="Open Sister">
                    </a>
                </p>
            </td>
            <td valign="top" style="border-left: 1px solid #000;">
                <!-- BEGIN CBOX - www.cbox.ws - v4.3 -->
                <div id="cboxdiv" style="position: relative; margin: 0 auto; width: 520px; max-width:100%; font-size: 0; line-height: 0;">
                    <div style="position: relative; height: 225px; overflow: auto; overflow-y: auto; -webkit-overflow-scrolling: touch; border: 0px solid;">
                        <iframe src="http://www1.cbox.ws/box/?boxid=1081571&boxtag=kbmt4j&sec=main" marginheight="0" marginwidth="0" frameborder="0" width="100%" height="100%" scrolling="auto" allowtransparency="yes" name="cboxmain1-1081571" id="cboxmain1-1081571"></iframe>
                    </div>
                    <div style="position: relative; height: 100px; overflow: hidden; border: 0px solid; border-top: 0px;">
                        <iframe src="http://www1.cbox.ws/box/?boxid=1081571&boxtag=kbmt4j&sec=form" marginheight="0" marginwidth="0" frameborder="0" width="100%" height="100%" scrolling="no" allowtransparency="yes" name="cboxform1-1081571" id="cboxform1-1081571"></iframe>
                    </div>
                </div>
                <!-- END CBOX -->
            </td>
        </tr>
    </table>
</div>