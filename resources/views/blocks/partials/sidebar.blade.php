<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        Welcome to {{ ucfirst($block->name) }}
    </div>
    <div class="col-md-2 text-right">
        <a data-connect>
            <i class="fa fa-caret-down"></i>
        </a>
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    @if (isset($player) != NULL)
        <h1 class="inset">
            Welcome, {{ $player->username }}
            <span class="pull-right">
                <a href="/logout">Logout</a>
            </span>
        </h1>
        @include('blocks.partials.miniprofile')
    @else
        <h1 class="inset">Login or Register</h1>

        <form action="/login" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" required="required" name="email" value="{{ old('email') }}" placeholder="email" class="form-control">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" id="password" required="required" name="password" placeholder="password" class="form-control">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Sign In</button>
                <a href="/register" class="btn btn-primary">Register</a>
                <br>
                <small><a href="/resetpassword">Forgot Password?</a></small>
            </div>
        </form>
    @endif
</div>

<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        Statistics
    </div>
    <div class="col-md-2 text-right">
        <a data-connect>
            <i class="fa fa-caret-down"></i>
        </a>
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    <h1 class="inset">Forum Statistics</h1>
    <div class="text-center">
        <b>Site Date:</b> @if (isset($date)) {{ $date }} @endif<br>
        {{ \Carbon\Carbon::now()->format('g:i A') }} PST
    </div>
    <hr>
    <table class="table">
        <tr>
            <td><i class="fa fa-users"></i></td>
            <td><b>Players</b></td>
            <td>{{ $block->players->count() }}</td>
        </tr>
        <tr>
            <td><i class="fa fa-user-circle"></i></td>
            <td><b>Characters</b></td>
            <td>{{ $block->characters->count() }}</td>
        </tr>
        <tr>
            <td><i class="fa fa-mars"></i></td>
            <td><b>Males</b></td>
            <td>{{ $block->males->count() }}</td>
        </tr>
        <tr>
            <td><i class="fa fa-venus"></i></td>
            <td><b>Females</b></td>
            <td>{{ $block->females->count() }}</td>
        </tr>
        <tr>
            <td><i class="fa fa-paperclip"></i></td>
            <td><b>Threads</b></td>
            <td>{{ $block->threadCount() }}</td>
        </tr>
        <tr>
            <td><i class="fa fa-pencil"></i></td>
            <td><b>Posts</b></td>
            <td>{{ $block->postCount() }}</td>
        </tr>
    </table>

    <div class="text-center">
        <a href="/archives">View Forum Archives</a>
    </div>
</div>

<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        {{ ucfirst($block->settings->group_name) }}s
    </div>
    <div class="col-md-2 text-right">
        <a data-connect>
            <i class="fa fa-caret-down"></i>
        </a>
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    <h1 class="inset">Census</h1>

    <div class="text-center">
        <small>Out of <b>{{ $block->characters->count() }}</b> characters, <b>{{ $block->groupChars->count() }}</b> belong to a(n) {{ $block->settings->group_name }}</small>
    </div>

    <ul class="group-list">
        @foreach ($block->groups as $group)
            @if ($group->leader && $group->defaultRank)
                <li style="background-color: {{ $group->hex }};">
                    <a href="/groups/{{ $group->id }}">
                        {{ $group->name }}

                        <small class="pull-right">
                            <i class="fa fa-mars" aria-hidden="true"></i> {{ $group->males->count() }} / <i class="fa fa-venus" aria-hidden="true"></i> {{ $group->females->count() }}
                        </small>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</div>

<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        Recent Activity
    </div>
    <div class="col-md-2 text-right">
        <a data-connect>
            <i class="fa fa-caret-down"></i>
        </a>
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    <h1 class="inset">5 most recent posts</h1>

    <table class="table">
        @foreach ($block->fiveLatest() as $t)
            <tr>
                <td><a href="/thread/{{ $t->id }}">{{ $t->title }}</a> by <a href="/character/{{ $t->lastPost->character->name }}">{{ $t->lastPost->character->name }}</a></td>
                <td>{{ $t->updated_at->diffForHumans() }}</td>
            </tr>
        @endforeach
    </table>
</div>