<div class="text-center">
    @if ($player->last_active > Carbon\Carbon::now()->subMinutes(5))
        <i class="fa fa-circle online" aria-hidden="true"></i>
    @else
        <i class="fa fa-circle" aria-hidden="true"></i>
    @endif
    <h4 class="inline"><a href="/player/{{ $player->username }}">{{ $player->username }}</a></h4>

    <br><br><img src="{{ $player->icon }}" class="avatar"><br>
    <small>{{ $player->role }}</small>

    @if ($player->title_unlocked)
        {!! $player->title !!}
    @endif

    <table class="table table-striped">
        <tr>
            <td><b>Posts</b></td>
            <td>{{ $player->post_count }}</td>
        </tr>
        @if ($block->settings->enable_currency)
            <tr>
                <td><b>{{ ucfirst($block->settings->currency_name) }}</b></td>
                <td>{{ $player->currency }}</td>
            </tr>
        @endif
        <tr>
            <td><b>Characters</b></td>
            <td>{{ $player->characters->count() }}</td>
        </tr>
    </table>
</div>