<style id="skin">
    body {
        background-color: <?php echo $skin->bg_color; ?>;
        color: <?php echo $skin->font_color; ?>;
    }

    body.pushable>.pusher, .sidebar {
        background: <?php echo $skin->bg_color; ?>;
    }

    header {
        background-image: url('<?php echo $skin->header_bg; ?>');
        background-color: <?php echo $skin->secondary_color; ?>;
        border-bottom: 3px solid <?php echo $skin->accent_color; ?>;
    }

    a {
        color: <?php echo $skin->link_color; ?>;
    }

    a:hover {
        color: <?php echo $skin->link_hover; ?> !important;
    }

    a:visited {
        color: <?php echo $skin->link_visited; ?>;
    }

    a:active {
        color: <?php echo $skin->link_active; ?>;
    }

    footer {
        background-color: <?php echo $skin->secondary_color; ?>;
        border-top: 3px solid <?php echo $skin->accent_color; ?>;
        color: <?php echo $skin->secondary_font; ?>;
    }

    footer a {
        color: <?php echo $skin->accent_color; ?>;
    }

    .banner {
        background-image: url('<?php echo $skin->banner; ?>');
    }

    #searchform {
        border:0.1em solid <?php echo $skin->accent_color; ?>;
    }

    #searchform label {
        color: <?php echo $skin->accent_color; ?>;
        text-shadow:0 0 0.1em <?php echo $skin->accent_color; ?>;
    }

    .navbar-brand {
        color: <?php echo $skin->secondary_font; ?>;
    }

    .navbar-default .navbar-nav>.active>a,
    .navbar-default .navbar-nav>.active>a:hover,
    .navbar-default .navbar-nav>.active>a:focus {
        color: <?php echo $skin->secondary_font; ?>;
    }

    .navbar .navbar-default a {
        color: <?php echo $skin->navbar_links; ?>;
    }

    #nav > ul > li > a:hover {
        color: <?php echo $skin->navbar_hover; ?>;
    }

    .navbar .navbar-default a.current {
        color: <?php echo $skin->navbar_hover; ?>;
    }

    .top-msg {
        border-bottom: <?php echo $skin->breadcrumbs_border; ?>;
        color: <?php echo $skin->breadcrumbs; ?>;
    }

    .breadcrumb a:hover {
        color: <?php echo $skin->breadcrumbs_hover; ?>;
    }

    .breadcrumb a:after {
        color: <?php echo $skin->breadcrumbs_links; ?>;
    }

    a.primary {
        color: <?php echo $skin->navbar_links; ?>;
    }

    a.primary:hover {
        color: <?php echo $skin->accent_color; ?>;
    }

    a.underline {
        color: <?php echo $skin->navbar_links; ?> !important;
    }

    a.underline:hover:after {
        background-color: <?php echo $skin->accent_color; ?>;
    }

    .table a {
        color: <?php echo $skin->accent_color; ?>;
    }

    .table a:hover {
        color: <?php echo $skin->accent_dark; ?>;
    }

    .forum-category {
        background-color: <?php echo $skin->accent_color; ?>;
        color: <?php echo $skin->accent_font; ?>;
        text-shadow: 0 1px 1px <?php echo $skin->accent_dark; ?>;
    }

    .forum-head {
        background-color: <?php echo $skin->secondary_color; ?>;
        border-right: 1px solid <?php echo $skin->secondary_color; ?>;
        border-left: 1px solid <?php echo $skin->secondary_color; ?>;
        color: <?php echo $skin->secondary_font; ?>;
    }

    .forum-topic {
        background-color: <?php echo $skin->catgory_color; ?>;
        border-bottom: 1px solid <?php echo $skin->category_border; ?>;
        color: <?php echo $skin->category_font; ?>;
    }

    .forum-icon {
        color: <?php echo $skin->category_icon; ?>;
    }

    .forum-topic a {
        color: <?php echo $skin->accent_color; ?>;
    }

    .forum-topic a:hover {
        color: <?php echo $skin->accent_darker; ?>;
    }

    .normal {
        background-color: <?php echo $skin->category_color; ?>;
        color: <?php echo $skin->category_font; ?>;
    }

    .normal h1.inset {
        background-color: <?php echo $skin->secondary_color; ?>;
        color: <?php echo $skin->secondary_font; ?>;
    }

    #top-button {
        background-color: <?php echo $skin->secondary_color; ?>;
    }

    .btn-primary {
        background: <?php echo $skin->accent_color; ?>;
        color: <?php echo $skin->accent_font; ?>;
    }

    .btn-primary:hover, .btn-primary:focus,
    .btn-primary:active, .btn-primary.active,
    .open > .dropdown-toggle.btn-primary {
        background: <?php echo $skin->accent_dark; ?>;
    }

    .btn-primary:active, .btn-primary.active {
        background: <?php echo $skin->secondary_dark; ?>;
    }

    .sub-bar {
        background-color: <?php echo $skin->category_color; ?>;
    }

    .sub-button {
        color: <?php echo $skin->secondary_font; ?> !important;
    }

    .theme-switcher {
        border: 1px solid <?php echo $skin->accent_color; ?>;
    }
</style>