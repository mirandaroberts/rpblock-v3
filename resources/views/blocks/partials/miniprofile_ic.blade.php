<div class="text-center">
    @if ($character->player->last_active > Carbon\Carbon::now()->subMinutes(5))
        <i class="fa fa-circle online" aria-hidden="true"></i>
    @else
        <i class="fa fa-circle" aria-hidden="true"></i>
    @endif
    <h4 class="inline"><a href="/character/{{ $character->name }}">{{ $character->name }}</a></h4>

    <br><br><img src="{{ $character->icon }}" class="avatar"><br>
        <small>Played by <a href="/player/{{ $character->player->username }}">{{ $character->player->username }}</a></small>

    @if ($character->title_unlocked)
        {!! $character->title !!}
    @endif

    <h1 class="group-type">{{ $character->groupName() }}</h1>

    {{ $character->rankName() }}

    <br>Level {{ $character->level }}

    @if ($block->hasWidget(9))
        {!! $character->pvpRank() !!}
    @endif
    <br>

    @if ($block->hasWidget(4))
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{ $character->health }}%">
                {{ $character->health }}/100
            </div>
        </div>
    @endif

    <table class="table table-striped">
        <tr>
            <td><b>Posts</b></td>
            <td>{{ $character->post_count }}</td>
        </tr>
        <tr>
            <td><b>Sex</b></td>
            <td>
                {{ ucfirst($character->gender) }}
                @if ($character->gender == 'male')
                    <i class="fa fa-mars" aria-hidden="true"></i>
                @else
                    <i class="fa fa-venus" aria-hidden="true"></i>
                @endif
            </td>
        </tr>
        <tr>
            <td><b>Age</b></td>
            <td>{{ $character->age() }}</td>
        </tr>
        <tr>
            <td><b>Birth Month</b></td>
            <td>{{ $character->birthMonthString() }}</td></td>
        </tr>
    </table>
</div>