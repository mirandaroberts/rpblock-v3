@extends('layouts.block')

@section('title')
    Dashboard
@endsection

@section('breadcrumbs')
    Dashboard
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Player Dashboard
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Welcome, {{ $player->username }}</h1>

        <div class="col-md-6 panel-link">
            <ul>
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                        </div>
                        <a href="{{ route('showCharacters', ['block' => $block->name, 'name' => $player->username]) }}"><span>My Characters</span></a>
                    </div>
                </li>
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-code fa-2x" aria-hidden="true"></i>
                        </div>
                        <a href="/tables"><span>My Tables</span></a>
                    </div>
                </li>
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-clipboard fa-2x" aria-hidden="true"></i>
                        </div>
                        <a href="/tags/my/{{ $player->id }}"><span>Thread Tags</span></a>
                    </div>
                </li>
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                        </div>
                        <a href="/inventory"><span>Inventory</span></a>
                    </div>
                </li>
                @if ($block->settings->enable_currency)
                    <li class="var_nav">
                        <div class="link_bg"></div>
                        <div class="link_title">
                            <div class="icon">
                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                            </div>
                            <a href="/player/{{ $player->username }}/{{ $block->settings->currency_name }}"><span>{{ ucfirst($block->settings->currency_name) }} Log</span></a>
                        </div>
                    </li>
                @endif
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
                        </div>
                        <a href="/settings"><span>Account Settings</span></a>
                    </div>
                </li>
                <li class="var_nav">
                    <div class="link_bg"></div>
                    <div class="link_title">
                        <div class="icon">
                            <i class="fa fa-sign-out fa-2x" aria-hidden="true"></i>
                        </div>

                        <a href="/logout">
                            <span>Logout</span>
                        </a>

                        <form id="logout-form" action="/logout" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="hQFbTIwUMFZUY3ATqJPvLnWmNJfPTB3906UE0Blt">
                        </form>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col-md-6">
            <table class="table table-responsive table-striped">
                <thead class="thead-inverse">
                    <tr>
                        <th colspan="2">General Statistics</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Join Date</b></td>
                        <td>{{ $player->created_at->format('m-d-Y') }}</td>
                    </tr>
                    @if ($block->settings->enable_currency)
                        <tr>
                            <td><b>Current {{ ucfirst($block->settings->currency_name) }}</b></td>
                            <td>{{ $player->currency }}</td>
                        </tr>

                        <tr>
                            <td><b>{{ ucfirst($block->settings->currency_name) }} Earned</b></td>
                            <td>{{ $player->currencyEarned() }}</td>
                        </tr>

                        <tr>
                            <td><b>{{ ucfirst($block->settings->currency_name) }} Spent</b></td>
                            <td>{{ $player->currencySpent() }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td><b>Total Characters</b></td>
                        <td>{{ $player->characters->count() }}</td>
                    </tr>
                    <tr>
                        <td><b>Active Characters</b></td>
                        <td>{{ $player->activeCharacters->count() }}</td>
                    </tr>
                    <tr>
                        <td><b>Inactive Characters</b></td>
                        <td>{{ $player->inactiveCharacters->count() }}</td>
                    </tr>
                    <tr>
                        <td><b>OOC Posts</b></td>
                        <td>{{ $player->oocPosts() }}</td>
                    </tr>
                    <tr>
                        <td><b>IC Posts</b></td>
                        <td>{{ $player->icPosts() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection