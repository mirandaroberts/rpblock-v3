@extends('layouts.block')

@section('title')
    Edit Table
@endsection

@section('breadcrumbs')
    <a href="/tables">Tables</a>
    Edit Table
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Edit Table
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            You are editing the table {{ $table->name }}
        </h1>

        <div class="row">
            <div class="text-center">
                {!! $table->header !!}
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum tortor vitae velit
                    ullamcorper, nec efficitur ante vehicula. Curabitur interdum mi gravida, egestas turpis consectetur,
                    pharetra purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                    Proin enim diam, elementum vel auctor sed, rutrum sed metus. Etiam ac tristique dui. Etiam luctus
                    dictum neque a interdum. Nullam eu volutpat leo. Ut ipsum sem, mollis in velit at, varius posuere felis.
                    Duis scelerisque ipsum vitae augue placerat, quis accumsan purus bibendum. Sed eget imperdiet diam.
                    Quisque id justo luctus, rutrum ligula in, faucibus purus. Quisque pellentesque nulla quis urna
                    dapibus, laoreet euismod augue suscipit. {!! $table->pre !!}"Pellentesque porttitor, leo quis finibus sagittis,
                    nunc dui posuere libero, ac imperdiet turpis lacus nec leo."{!! $table->post !!} Maecenas justo tellus, ultrices nec
                    dignissim eu, vulputate id odio. Duis lacinia mattis ornare.
                </p>
                {!! $table->footer !!}
            </div>

            <hr>

            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $table->name) }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('character_id') ? ' has-error' : '' }}">
                    <label for="character_id" class="col-md-4 control-label">Character</label>

                    <div class="col-md-6">
                        <select id="character_id" class="form-control" name="character">
                            <option value="0" @if ($table->character_id == 0) selected @endif>None (OOC or not Character-specific)</option>
                            @foreach ($player->characters as $character)
                                <option value="{{ $character->id }}" @if ($table->character_id == $character->id) selected @endif>{{ $character->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('character_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('character_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('header') ? ' has-error' : '' }}">
                    <label for="header" class="col-md-4 control-label">Header</label>

                    <div class="col-md-6">
                        <textarea name="header" id="header" class="form-control">{{ old('header', $table->header) }}</textarea>

                        @if ($errors->has('header'))
                            <span class="help-block">
                            <strong>{{ $errors->first('header') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('footer') ? ' has-error' : '' }}">
                    <label for="footer" class="col-md-4 control-label">Footer</label>

                    <div class="col-md-6">
                        <textarea name="footer" id="footer" class="form-control">{{ old('footer', $table->footer) }}</textarea>

                        @if ($errors->has('footer'))
                            <span class="help-block">
                            <strong>{{ $errors->first('footer') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('pre') ? ' has-error' : '' }}">
                    <label for="pre" class="col-md-4 control-label">Pre-speech</label>

                    <div class="col-md-6">
                        <input id="pre" type="text" class="form-control" name="pre" value="{{ old('pre', $table->pre) }}" required autofocus>

                        @if ($errors->has('pre'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pre') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
                    <label for="post" class="col-md-4 control-label">Post-speech</label>

                    <div class="col-md-6">
                        <input id="post" type="text" class="form-control" name="post" value="{{ old('post', $table->post) }}" required autofocus>

                        @if ($errors->has('post'))
                            <span class="help-block">
                            <strong>{{ $errors->first('post') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection