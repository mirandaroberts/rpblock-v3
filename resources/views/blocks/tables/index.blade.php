@extends('layouts.block')

@section('title')
    Tables
@endsection

@section('breadcrumbs')
    Tables
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Tables
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Your Tables

            <span class="pull-right">
                <a href="/tables/create"><i class="fa fa-plus"></i> New Table</a>
            </span>
        </h1>

        <h3>OOC/Global Tables</h3>

        <ol>
            @foreach ($tables as $table)
                @if (!$table->character_id)
                    <li><a href="/tables/edit/{{ $table->id }}">{{ $table->name }}</a></li>
                @endif
            @endforeach
        </ol>

        @foreach ($player->characters as $character)
            <h3>{{ $character->name }}'s Tables</h3>

            <ol>
                @foreach ($tables as $table)
                    @if ($table->character_id == $character->id)
                        <li><a href="/tables/edit/{{ $table->id }}">{{ $table->name }}</a></li>
                    @endif
                @endforeach
            </ol>
        @endforeach
    </div>
@endsection