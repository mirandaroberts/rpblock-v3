@extends('layouts.block')

@section('title')
    Create Table
@endsection

@section('breadcrumbs')
    <a href="/tables">Tables</a>
    Create Table
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Create Table
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Create a table for your character's posts
        </h1>

        <div class="row">
            <p class="text-center">
                Tables are HTML code that can be applied to a character's post on the fly.<br>
                <b>To use your speech code in your posts, surround your speech in curly brackets {}.</b>
            </p>

            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('character') ? ' has-error' : '' }}">
                    <label for="character" class="col-md-4 control-label">Character</label>

                    <div class="col-md-6">
                        <select id="character" class="form-control" name="character">
                            <option value="0">None (OOC or not Character-specific)</option>
                            @foreach ($player->characters as $character)
                                <option value="{{ $character->id }}">{{ $character->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('character'))
                            <span class="help-block">
                            <strong>{{ $errors->first('character') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('header') ? ' has-error' : '' }}">
                    <label for="header" class="col-md-4 control-label">Header</label>

                    <div class="col-md-6">
                        <textarea name="header" id="header" class="form-control">{{ old('header') }}</textarea>

                        @if ($errors->has('header'))
                            <span class="help-block">
                            <strong>{{ $errors->first('header') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('footer') ? ' has-error' : '' }}">
                    <label for="footer" class="col-md-4 control-label">Footer</label>

                    <div class="col-md-6">
                        <textarea name="footer" id="footer" class="form-control">{{ old('footer') }}</textarea>

                        @if ($errors->has('footer'))
                            <span class="help-block">
                            <strong>{{ $errors->first('footer') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('pre') ? ' has-error' : '' }}">
                    <label for="pre" class="col-md-4 control-label">Pre-speech</label>

                    <div class="col-md-6">
                        <input id="pre" type="text" class="form-control" name="pre" value="{{ old('pre') }}" required autofocus>

                        @if ($errors->has('pre'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pre') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
                    <label for="post" class="col-md-4 control-label">Post-speech</label>

                    <div class="col-md-6">
                        <input id="post" type="text" class="form-control" name="post" value="{{ old('post') }}" required autofocus>

                        @if ($errors->has('post'))
                            <span class="help-block">
                            <strong>{{ $errors->first('post') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection