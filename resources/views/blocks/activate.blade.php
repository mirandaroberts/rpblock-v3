@extends('layouts.block')

@section('title')
    Activate Your Account
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Activate Your Account
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Use the code sent to your email to activate your account.</h1>

        <form action="" method="post">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label" for="email">Email <span class="required">*</span></label>
                <input type="email" id="email" required="required" name="email" value="{{ old('email', $player->email) }}" class="form-control">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label class="control-label" for="username">Code <span class="required">*</span></label>
                <input type="text" id="code" required="required" name="username" value="{{ old('code', $code) }}" class="form-control">

                @if ($errors->has('code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right">Activate</button>
                <a href="/resendcode" class="btn btn-default pull-right">Resend Activation Code</a>
            </div>
        </form>
    </div>
@endsection