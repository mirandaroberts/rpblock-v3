@extends('layouts.block')

@section('title')
    Welcome
@endsection

@section('content')
    @include('blocks.partials.banner')
    <div id="ooc-catas">
        @include('blocks.partials.categoryOOC', ['categories' => $categories])
    </div>

    <div id="ic-catas">
        @include('blocks.partials.categoryIC', ['categories' => $categories])
    </div>
@endsection