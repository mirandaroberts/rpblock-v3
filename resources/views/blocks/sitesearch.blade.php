@extends('layouts.block')

@section('title')
    Site Search
@endsection

@section('breadcrumbs')
    Site Search
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Site Search
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Your search results
        </h1>

        <div class="text-center">We have found <b>{{ $results->count() }}</b> result(s) for "{{ $q }}"</div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Result</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($results as $result)
                    <tr>
                        @if ($result->username)
                            <td>Player</td>
                            <td><a href="/player/{{ $result->username }}">{{ $result->username }}</a></td>
                        @elseif ($result->surname)
                            <td>Character</td>
                            <td><a href="/character/{{ $result->name }}">{{ $result->name }} {{ $result->surname }}</a></td>
                        @elseif ($result->name && $result->body)
                            <td>Guidebook Page</td>
                            <td><a href="/guidebook/{{ $result->id }}">{{ $result->name }}</a></td>
                        @elseif ($result->author_id)
                            <td>Thread</td>
                            <td><a href="/thread/{{ $result->id }}">{{ $result->title }}</a></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection