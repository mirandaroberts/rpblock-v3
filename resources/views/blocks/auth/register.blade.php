@extends('layouts.block')

@section('title')
    Register
@endsection

@section('header')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Register
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Create an account on {{ $block->name }}</h1>

        <form action="" method="post">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label" for="email">Email <span class="required">*</span></label>
                <input type="email" id="email" required="required" name="email" value="{{ old('email') }}" class="form-control">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label class="control-label" for="username">Username <span class="required">*</span></label>
                <input type="text" id="username" required="required" name="username" value="{{ old('username') }}" class="form-control">

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                <label class="control-label" for="dob">Date of Birth <span class="required">*</span></label>
                <input type="date" id="dob" required="required" name="dob" value="{{ old('dob') }}" class="form-control">

                @if ($errors->has('dob'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dob') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label" for="password">Password <span class="required">*</span></label>
                <input type="password" id="password" required="required" name="password" class="form-control">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="control-label" for="password-confirmation">Repeat Password <span class="required">*</span></label>
                <input type="password" id="password-confirmation" required="required" name="password_confirmation" class="form-control">

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <!--<div class="form-group">
                <div class="g-recaptcha" data-sitekey="6Le8eB0UAAAAABWZpjAzJxpGrRrJASLQcGuPwq5z"></div>
            </div>-->

            <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right">Register!</button><br>
            </div>
        </form>
    </div>
@endsection