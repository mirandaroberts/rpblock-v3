@extends('layouts.block')

@section('title')
    Edit Ticket Comment
@endsection

@section('breadcrumbs')
    <a href="/modbox">Modbox Tickets</a>
    <a href="/modbox/ticket/{{ $comment->ticket->id }}">Ticket #{{ $comment->ticket->id }}</a>
    Edit Comment
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Edit Comment
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Ticket #{{ $comment->ticket->id }}</h1>

        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('private') ? ' has-error' : '' }}">
                    <div class="col-md-4 control-label"></div>

                    <div class="col-md-6">
                        <input type="hidden" name="private" value="0">
                        <input type="checkbox" id="private" name="private" value="1" @if ($comment->private) checked @endif>
                        <small>Comment will be hidden from the sender.</small>

                        @if ($errors->has('body'))
                            <span class="help-block">
                                <strong>{{ $errors->first('private') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body" class="col-md-4 control-label">Body</label>

                    <div class="col-md-6">
                        <textarea id="body" class="form-control" name="body">{{ $comment->body }}</textarea>

                        @if ($errors->has('body'))
                            <span class="help-block">
                                <strong>{{ $errors->first('body') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection