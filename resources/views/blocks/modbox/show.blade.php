@extends('layouts.block')

@section('title')
    Modbox
@endsection

@section('breadcrumbs')
    Modbox
@endsection

@section('content')
    @if (isset($player))
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                Your Tickets
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">
                Your open modbox tickets

                <span class="pull-right">
                    <a href="/modbox/ticket/new"><i class="fa fa-plus"></i> New Modbox Ticket</a>
                </span>
            </h1>

            <div class="text-center">You have <b>{{ $player->tickets->count() }}</b> tickets.</div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Ticket #</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($player->tickets as $ticket)
                            <tr>
                                <td>{{ $ticket->id }}</td>
                                <td><a href="{{ route('ticket', ['block' => $block->name, 'id' => $ticket->id]) }}">{{ $ticket->title }}</a></td>
                                <td>{{ $ticket->type }}</td>
                                <td>{{ $ticket->status }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @if ($player->isStaff())
            <div class="col-md-12 forum-category rounded top lpad">
                <div class="col-md-10">
                    Open Tickets
                </div>
            </div>

            <div class="col-md-12 toggleview normal lpad">
                <h1 class="inset">Open tickets on site</h1>

                <div class="text-center">There are <b>{{ $tickets->count() }}</b> tickets.</div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Ticket #</th>
                            <th>IP</th>
                            <th>Player</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $ticket)
                                <tr>
                                    <td>{{ $ticket->id }}</td>
                                    <td>{{ $ticket->ip }}</td>
                                    <td>
                                        @if ($ticket->player)
                                            <a href="/player/{{ $ticket->player->username }}">{{ $ticket->player->username }}</a>
                                        @else
                                            {{ $ticket->email }}
                                        @endif
                                    </td>
                                    <td><a href="{{ route('ticket', ['block' => $block->name, 'id' => $ticket->id]) }}">{{ $ticket->title }}</a></td>
                                    <td>{{ $ticket->type }}</td>
                                    <td>{{ $ticket->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        <div class="pull-right">
            <form action="/modbox/closed">
                <button type="submit" class="btn btn-primary">See Closed Tickets</button>
            </form>
        </div>
    @else
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                Your Tickets
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">
                Your open modbox tickets

                <span class="pull-right">
                    <a href="/modbox/ticket/new"><i class="fa fa-plus"></i> New Modbox Ticket</a>
                </span>
            </h1>

            <div class="text-center">You have <b>{{ $tickets->count() }}</b> tickets. Because you are not logged in, we are pulling tickets using your IP address.</div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Ticket #</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($tickets as $ticket)
                        <tr>
                            <td>{{ $ticket->id }}</td>
                            <td><a href="{{ route('ticket', ['block' => $block->name, 'id' => $ticket->id]) }}">{{ $ticket->title }}</a></td>
                            <td>{{ $ticket->type }}</td>
                            <td>{{ $ticket->status }}</td>
                            <td>
                                <form action="/modbox/ticket/{{ $ticket->id }}/close">
                                    <button type="submit" class="btn btn-default">Close Ticket</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection