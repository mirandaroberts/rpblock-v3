@extends('layouts.block')

@section('title')
    Delete Ticket Comment
@endsection

@section('breadcrumbs')
    <a href="/modbox">Modbox Tickets</a>
    <a href="/modbox/ticket/{{ $comment->ticket->id }}">Ticket #{{ $comment->ticket->id }}</a>
    Delete Comment
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Delete Comment
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Ticket #{{ $comment->ticket->id }}</h1>

        <div class="col-md-12">
            <p class="text-center">You are about to delete a modbox ticket comment. Are you sure you wish to continue?</p>
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Yes
                        </button>

                        <a href="/modbox/ticket/{{ $comment->ticket_id }}" class="btn btn-default">
                            Cancel
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection