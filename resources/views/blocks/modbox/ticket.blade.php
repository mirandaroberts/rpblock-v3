@extends('layouts.block')

@section('title')
    Modbox Ticket
@endsection

@section('breadcrumbs')
    <a href="/modbox">Modbox Tickets</a>
    Ticket #{{ $ticket->id }}
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Ticket #{{ $ticket->id }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">{{ $ticket->type }}</h1>

        <div class="col-md-6">
            <h3>{{ $ticket->title }}</h3>
        </div>

        @if ($ticket->type == 'Character Audition' && $ticket->status != 'Accepted')
            @if ($ticket->sender_id != $player->id && $player->isStaff())
                <div class="pull-right">
                    <form action="/modbox/ticket/{{ $ticket->id }}/accept">
                        <button type="submit" class="btn btn-primary">Accept Character</button>
                    </form>
                </div>
            @endif
        @endif

        <div class="col-md-12">
            <h5>Ticket Info</h5>
            <table class="table">
                <tr>
                    <td><b>IP Address</b></td>
                    <td>{{ $ticket->ip }}</td>
                </tr>
                <tr>
                    <td><b>Player</b></td>
                    <td>
                        @if ($ticket->player)
                            <a href="/player/{{ $ticket->player->username }}">{{ $ticket->player->username }}</a>
                        @else
                            {{ $ticket->email }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b>Type</b></td>
                    <td>{{ $ticket->type }}</td>
                </tr>
                <tr>
                    <td><b>URL</b></td>
                    <td>
                        @if ($ticket->url)
                            <a href="{{ $ticket->url }}">[link]</a>
                        @else
                            None
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><b>Status</b></td>
                    <td>{{ $ticket->status }}</td>
                </tr>
                <tr>
                    <td><b>Reported</b></td>
                    <td>
                        @if ($ticket->reported())
                            {!! $ticket->reported() !!}
                        @else
                            None
                        @endif
                    </td>
                </tr>
            </table>
        </div>


        @if ($ticket->type == 'Character Audition')
            <div class="col-md-6">
                <div class="text-center">
                    <img src="{{ $ticket->reportedChar->icon }}" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}" alt="Avatar">
                </div>

                <h5>Basic Info</h5>

                <table class="table">
                    <tr>
                        <td><b>Full Name</b></td>
                        <td>{{ $ticket->reportedChar->fullName() }}</td>
                    </tr>
                    <tr>
                        <td><b>Gender</b></td>
                        <td>{{ ucfirst($ticket->reportedChar->gender) }}</td>
                    </tr>
                    <tr>
                        <td><b>Age</b></td>
                        <td>{{ $ticket->reportedChar->age() }}</td>
                    </tr>
                    <tr>
                        <td><b>Cost</b></td>
                        <td>{{ $ticket->reportedChar->cost }}</td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <h5>Traits</h5>

                <ul class="list-group">
                    @foreach ($ticket->reportedChar->traits as $trait)
                        <li class="list-group-item">
                            <b>{{ ucfirst($trait->data->attribute->name) }}</b> {{ ucfirst($trait->data->name) }}

                            @if ($trait->value) - {{ ucfirst($trait->value) }} @endif

                            @if ($block->settings->enable_currency)
                                <span class="badge">{{ $trait->data->cost }} {{ $block->settings->curreny_name }}</span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        @else
            <h5>Body</h5>
        @endif
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Comments
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            There are

            @if ($player->id == $ticket->player->id)
                {{ $ticket->privateComments->count() }}
            @else
                {{ $ticket->comments->count() }}
            @endif

            comments on this ticket
        </h1>

@foreach ($ticket->comments as $comment)
    @if (($player->id == $ticket->player->id && !$comment->private) || $player->isStaff())
        <div class="row comment">
            <div class="col-md-3 text-center">
                <h3 class="text-center"><a href="/player/{{ $comment->player->username }}">{{ $comment->player->username }}</a></h3>

                <img src="{{ $comment->player->icon }}" class="avatar">
                <br>
                <small>
                    {{ $comment->created_at->diffForHumans() }}

                    @if ($comment->private)
                        <i class="fa fa-circle"></i> Private
                    @endif

                    @if ($comment->player->id == $player->id)
                        <br><a href="/modbox/comment/{{ $comment->id }}/edit">Edit</a>
                         <i class="fa fa-circle"></i> <a href="/modbox/comment/{{ $comment->id }}/delete">Delete</a>
                    @endif
                </small>
            </div>

            <div class="col-md-9">
                {!! $comment->body !!}
            </div>
        </div>

        <hr>
    @endif
@endforeach
</div>

@if (!$ticket->closed_by)
<div class="col-md-12 forum-category rounded top lpad">
    <div class="col-md-10">
        New Comment
    </div>
</div>

<div class="col-md-12 toggleview normal lpad">
    <h1 class="inset">Create a new comment</h1>

    <form class="form-horizontal" role="form" method="POST" action="">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('private') ? ' has-error' : '' }}">
            <div class="col-md-4 control-label"></div>

            @if ($player->isStaff() && $player->id != $ticket->player->id)
                <div class="col-md-6">
                    <input type="hidden" name="private" value="0">
                    <input type="checkbox" id="private" name="private" value="1">
                    <small>Comment will be hidden from the sender.</small>

                    @if ($errors->has('private'))
                        <span class="help-block">
                            <strong>{{ $errors->first('private') }}</strong>
                        </span>
                    @endif
                </div>
            @endif
        </div>

        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
            <label for="body" class="col-md-4 control-label">Body</label>

            <div class="col-md-6">
                <textarea id="body" class="form-control" name="body">{{ old('body') }}</textarea>

                @if ($errors->has('body'))
                    <span class="help-block">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>
@endif
@endsection