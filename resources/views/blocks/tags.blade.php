@extends('layouts.block')

@section('title')
    Tags
@endsection

@section('breadcrumbs')
    @if ($method == 'player')
        <a href="/player/{{ $tagged->username }}">{{ $tagged->username }}</a>
    @elseif ($method == 'character')
        <a href="/character/{{ $tagged->name }}">{{ $tagged->name }}</a>
    @elseif ($method == 'thread')
        @if ($thread->category->parent)
            @if ($thread->category->parent->parent)
                <a href="/category/{{ $thread->category->parent->parent->id }}">{{ $thread->category->parent->parent->title }}</a>
            @endif
            <a href="/category/{{ $thread->category->parent->id }}">{{ $thread->category->parent->title }}</a>
        @endif

        <a href="/thread/{{ $thread->id }}">{{ $thread->title }}</a>
    @else
        <a href="/dashboard">Dashboard</a>
    @endif

    Tags
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            @if ($method == 'player')
                {{ $tagged->username }}'s Tags
            @elseif ($method == 'character')
                {{ $tagged->name }}'s Tags
            @elseif ($method == 'thread')
                @if ($thread->category->type == 'ooc')
                    Players tagged in {{ $thread->title }}
                @else
                    Characters tagged in {{ $thread->title }}
                @endif
            @else
                Tags
            @endif
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            @if ($method == 'thread')
                Ordered alphabetically

                @if ($thread->category->type == 'ooc')
                    <span class="pull-right">
                        <a href="/tags/self/{{ $thread->id }}"><i class="fa fa-plus"></i> Tag Yourself</a>
                    </span>
                @endif
            @else
                Ordered by most recently updated
            @endif
        </h1>

        @if ($method == 'player' || $method == 'character')
            @if ($tagged->tags)
                <table class="table-responsive table table-striped">
                    <thead>
                        <tr>
                            <th>Thread</th>
                            <th>Posts</th>
                            <td>Last Post</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tagged->tags as $tag)
                            <tr>
                                <td><a href="/thread/{{ $tag->thread->id }}">{{ $tag->thread->title }}</a></td>
                                <td>{{ $tag->thread->reply_count }}</td>
                                <td>
                                    @if ($tag->thread->lastPost)
                                        @if ($tag->thread->category->type == 'ooc')
                                            By <a href="/player/{{ $tag->thread->lastPost->player->username }}">{{ $tag->thread->lastPost->player->username }}</a> {{ $tag->thread->lastPost->created_at->diffForHumans() }}
                                        @else
                                            By <a href="/character/{{ $tag->thread->lastPost->character->name }}">{{ $tag->thread->lastPost->character->name }}</a> {{ $tag->thread->lastPost->created_at->diffForHumans() }}
                                        @endif
                                    @else
                                        No Replies
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="text-center">No tags to show!</div>
            @endif
        @elseif ($method == 'thread')
            @if ($thread->category->type == 'ooc')
                <table class="table-responsive table table-striped">
                    @foreach ($thread->tags as $tag)
                        <tr>
                            <td><a href="/player/{{ $tag->player->username }}">{{ $tag->player->username }}</a></td>
                        </tr>
                    @endforeach
                </table>
            @else
                <table class="table-responsive table table-striped">
                    @foreach ($thread->tags as $tag)
                        <tr>
                            <td><a href="/character/{{ $tag->character->username }}">{{ $tag->character->username }}</a></td>
                        </tr>
                    @endforeach
                </table>
            @endif
        @else
            @if ($player->allTags)
                <table class="table-responsive table table-striped">
                    <thead>
                    <tr>
                        <th>Thread</th>
                        <th>Posts</th>
                        <td>Last Post</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($player->allTags as $tag)
                        <tr>
                            <td><a href="/thread/{{ $tag->thread->id }}">{{ $tag->thread->title }}</a></td>
                            <td>{{ $tag->thread->reply_count }}</td>
                            <td>
                                @if ($tag->thread->lastPost)
                                    @if ($tag->thread->category->type == 'ooc')
                                        By <a href="/player/{{ $tag->thread->lastPost->player->username }}">{{ $tag->thread->lastPost->player->username }}</a> {{ $tag->thread->lastPost->created_at->diffForHumans() }}
                                    @else
                                        By <a href="/character/{{ $tag->thread->lastPost->character->name }}">{{ $tag->thread->lastPost->character->name }}</a> {{ $tag->thread->lastPost->created_at->diffForHumans() }}
                                    @endif
                                @else
                                    No Replies
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="text-center">You have no tags.</div>
            @endif
        @endif
    </div>
@endsection