@extends('layouts.block')

@section('title')
    Notifications
@endsection

@section('breadcrumbs')
    Notifications
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Your Notifications
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Showing all notifications
        </h1>

        <div class="text-center">You have <b>{{ $player->unreadNotifications->count() }}</b> notifications.</div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><input type="checkbox" id="check-all"></th>
                    <th>Notification</th>
                    <th>Date</th>
                    <th>URL</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($player->unreadNotifications as $notice)
                    <tr>
                        <th><input type="checkbox" value="{{ $notice->id }}"></th>
                        <td>{{ $notice->message }}</td>
                        <td>{{ $notice->created_at->format('m-d-Y') }}</td>
                        <td><a href="{{ $notice->url }}">[link]</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection