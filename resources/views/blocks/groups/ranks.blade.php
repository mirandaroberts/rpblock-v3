@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    <a href="/groups/{{ $group->id }}">{{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}</a>
    Ranks
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Modify Ranks</h1>

        @if ($player->canGroupPromote($group->id) || $player->canGroupDemote($group->id))
            <h4>Modify Members</h4>
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <thead>
                        <tr>
                            <th>Character</th>
                            <th>Rank</th>
                            <th>Posts this Week</th>
                            <th>Change Rank</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($group->characters as $character)
                            <tr>
                                <td><a href="/character/{{ $character->name }}">{{ $character->name }}</a></td>
                                <td>{{ $character->rankName() }}</td>
                                <td>{{ $character->postsThisWeek() }}</td>
                                <td>
                                    @if ($character->rank->id == $group->leader->id)
                                        Primary Leader must be adjusted by staff.
                                    @else
                                        <form method="post" action="/groups/{{ $group->id }}/invite">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="character" value="{{ $character->id }}">
                                            <label for="rank_id">Select a Rank</label>
                                            <select name="rank_id" id="rank_id" class="form-control">
                                                @foreach ($group->ranks as $rank)
                                                    @if ($rank->id != $group->leader->id)
                                                        <option value="{{ $rank->id }}">{!! $rank->name() !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <button class="btn btn-success btn-sm">Change Rank</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if ($player->canGroupInvite($group->id))
            <hr>
            <h4>Invite Character</h4>
            <form method="post" action="/groups/{{ $group->id }}/invite">
                {{ csrf_field() }}
                <label for="invite_character">Select a Character</label>
                <select name="invite_character" id="invite_character" class="form-control autocomplete">
                    @foreach ($block->characters as $character)
                        @if ($character->group_id != $group->id)
                            <option value="{{ $character->id }}">{{ $character->name }}</option>
                        @endif
                    @endforeach
                </select>
                <button class="btn btn-success">Invite Character</button>
            </form>
        @endif

        @if ($player->canGroupRemove($group->id))
            <hr>
            <h4>Remove Character</h4>
            <form method="post" action="/groups/{{ $group->id }}/remove">
                {{ csrf_field() }}
                <label for="remove_character">Select a Character</label>
                <select name="remove_character" id="remove_character" class="form-control autocomplete">
                    @foreach ($group->characters as $character)
                        <option value="{{ $character->id }}">{{ $character->name }}</option>
                    @endforeach
                </select>
                <button class="btn btn-success">Remove Character</button>
            </form>
        @endif
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.autocomplete').select2({
                placeholder: "Select a character!",
            });
        });
    </script>
@endsection