@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    <a href="/groups/{{ $group->id }}">{{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}</a>
    Rules
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Modify Rules</h1>

        <h4>Current Rules</h4>
        <ol>
            @foreach ($group->rules as $rule)
                <li>{{ $rule->rule }} <a href="/groups/{{ $group->id }}/rules/{{ $rule->id }}" class="btn-sm btn-primary">Edit</a> <a href="" class="btn-sm btn-default">Delete</a></li>
            @endforeach
        </ol>

        <h4>Create New Rule</h4>
        <form method="post" action="">
            {{ csrf_field() }}
            <label for="rule">Rule</label>
            <input type="text" name="rule" class="form-control" id="rule" required>
            <button class="btn btn-success">Create Rule</button>
        </form>
    </div>
@endsection