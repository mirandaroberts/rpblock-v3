@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('content')
    @if ($invites && $invites->count())
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                {{ ucfirst($group->name) }}
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">Special Alert</h1>

            @foreach ($invites as $invite)
                <div class="row">
                    <p>
                        <b>{{ $invite->character->name }}</b> has been invited to join {{ $group->name }}.<br>
                        <a href="/groups/acceptinvite/{{ $invite->id }}" class="btn btn-sm btn-success">Accept</a>
                        <a href="/groups/declineinvite/{{ $invite->id }}" class="btn btn-sm btn-default">Decline</a>
                    </p>
                </div>
            @endforeach
        </div>
    @endif

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            General Info

            <span class="pull-right">
                @if (isset($player))
                    @if ($player->canEditGroupRules($group->id) || $player->isStaff())
                        <a href="/groups/{{ $group->id }}/rules"><i class="fa fa-pencil"></i> Edit Rules</a>
                    @endif
                @endif
            </span>
        </h1>

        <div class="col-md-5">
            <img src="http://via.placeholder.com/400x500">
        </div>

        <div class="col-md-7">
            @if (!$group->currentLeader)
                <h4>This {{ $block->settings->group_name }} is open for claim!</h4>

                <p>
                    To claim {{ $group->name }}, you must have an accepted character that fits the requirements of the
                    leader rank. For this {{ $block->settings->group_name }}, this mean you must have a character that
                    meets the following criteria (please note that there may be additional criteria to claiming not
                    covered by this list. Be sure to consult the <a href="/guidebook">Guidebook</a>):
                </p>
                <ol>
                    @if ($group->leader->name_male && !$group->leader->name_female)<li>Is male</li>@endif
                    @if (!$group->leader->name_male && $group->leader->name_female)<li>Is female</li>@endif
                    @if ($group->leader->post_reqs)<li>Has at least {{ $group->leader->post_reqs }} posts within the last week</li>@endif
                    @if ($group->leader->posts_needed)<li>Has at least {{ $group->leader->posts_needed }} posts overall</li>@endif
                    @if ($group->leader->min_age)<li>Is at least {{ $group->leader->min_age / 12 }} years old</li>@endif
                    @if ($group->leader->max_age)<li>Is under {{ $group->leader->max_age / 12 }} years old</li>@endif
                </ol>

                @if (isset($player))
                    <hr>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <form method="post" action="/groups/{{ $group->id }}/claim">
                                {{ csrf_field() }}
                                <label for="character">Select character</label>
                                <select name="character" id="character" class="form-control">
                                    <option disabled selected>Choose a character to claim {{ $group->name }}</option>
                                    @foreach ($player->characters as $character)
                                        @if ($character->fitsRank($group->leader->id))
                                            <option value="{{ $character->id }}">{{ $character->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <button class="btn btn-success">
                                    Claim {{ $group->name }}
                                    @if ($group->leader->challenge_amt && $block->settings->enable_currency)
                                         ({{ $group->leader->challenge_amt }} {{ $block->settings->currency_name }})
                                    @endif
                                </button>
                            </form>
                        </div>
                    </div>
                @endif
            @else
                <h4>News</h4>
                <small>
                    Last updated:
                    @if (!$group->news)
                        Never
                    @else
                        {{ $group->news->created_at->diffForHumans() }}
                    @endif
                </small><br>
                <p>
                    @if ($group->news)
                        {{ $group->news->content }}
                    @else
                        No news yet.
                    @endif
                </p>
                <div class="pull-right">
                    <a href="/groups/{{ $group->id }}/news">See older news <i class="fa fa-arrow-right"></i></a>

                    @if (isset($player) && ($player->canEditGroupNews($group->id) || $player->isStaff()))
                         || <a href="/groups/{{ $group->id }}/editnews">Edit News <i class="fa fa-arrow-right"></i></a>
                    @endif
                </div>
                <br><br>
                <hr>

                <h4>Rules</h4>

                @if ($group->rules->count())
                    <ol>
                        @foreach ($group->rules as $rule)
                            <li>{{ $rule->rule }}</li>
                        @endforeach
                    </ol>
                @else
                    No rules have been made yet.
                @endif
            @endif
        </div>
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Ranks

            <span class="pull-right">
                @if (isset($player))
                    @if ($player->canEditGroupRanks($group->id) || $player->isStaff())
                        <a href="/groups/{{ $group->id }}/ranks"><i class="fa fa-pencil"></i> Manage Members</a>
                    @endif
                @endif
            </span>
        </h1>

        <div class="text-center">{{ ucfirst($group->name) }} has <b>{{ $group->characters->count() }}</b> total characters.</div>
        <table class="table" width="100%">
            @foreach ($group->ranks as $rank)
                <tr>
                    <td>
                        <div class="pull-left"><h5>{!! $rank->name() !!}</h5></div>
                        <div class="pull-right badge badge-primary">
                            @if ($rank->max_amount)
                                {{ $rank->characters->count() }}/{{ $rank->max_amount }}
                            @else
                                {{ $rank->characters->count() }}/&infin;
                            @endif
                        </div>
                        <table class="table-striped table-sm" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Posts</th>
                                <th>Posts this week</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($rank->characters as $character)
                                <tr>
                                    <td><a href="{{ route('characterProfile', ['block' => $block->name, 'name' => $character->name]) }}">{{ $character->name }}</a></td>
                                    <td>{{ $character->age() }}</td>
                                    <td>{{ ucfirst($character->gender) }}</td>
                                    <td>{{ number_format($character->post_count) }}</td>
                                    <td>{{ $character->postsThisWeek() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Affiliations

            <span class="pull-right">
                @if (isset($player))
                    @if ($player->canEditGroupAffiliations($group->id) || $player->isStaff())
                        <a href="/groups/{{ $group->name }}/affiliates"><i class="fa fa-pencil"></i> Manage Affiliations</a>
                    @endif
                @endif
            </span>
        </h1>

        @foreach ($groups as $g)
            <b style="color:{{ $g->hex }};">{{ ucfirst($g->name) }}</b> - {{ $group->relationWith($g->id) }}<br>
        @endforeach
    </div>
@endsection