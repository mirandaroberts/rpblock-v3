@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    <a href="/groups/{{ $group->id }}">{{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}</a>
    News
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">News</h1>
        @if (!$news->count()) No news has been created yet. @endif
        @foreach ($news as $n)
            <p>{{ $n->content }}</p>
            <small>Posted {{ $n->created_at->format('m-d-Y') }}</small>
            <hr>
        @endforeach
    </div>
@endsection