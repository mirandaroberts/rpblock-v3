@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    <a href="/groups/{{ $group->id }}">{{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}</a>
    Rules
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Create News Post</h1>

        <p>Be aware that for continuity purposes, news posts cannot be edited after creation. Be sure to proof-read
        your news post carefully before posting.</p>

        <form method="post" action="">
            {{ csrf_field() }}
            <label for="content">News Body</label>
            <textarea name="content" class="form-control" id="content" required>{{ old('content') }}</textarea>
            <button class="btn btn-success">Create News Post</button>
        </form>
    </div>
@endsection