@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}
@endsection

@section('breadcrumbs')
    <a href="/groups/{{ $group->id }}">{{ ucfirst($block->settings->group_name) }}s - {{ ucfirst($group->name) }}</a>
    Rules
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($group->name) }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Modify Rules</h1>

        <h4>Edit Rule</h4>
        <form method="post" action="">
            {{ csrf_field() }}
            <label for="rule">Rule</label>
            <input type="text" name="rule" class="form-control" id="rule" required value="{{ $rule->rule }}">
            <button class="btn btn-success">Edit Rule</button>
        </form>
    </div>
@endsection