@extends('layouts.block')

@section('title')
    {{ ucfirst($block->settings->group_name) }}s
@endsection

@section('breadcrumbs')
    {{ ucfirst($block->settings->group_name) }}s
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ ucfirst($block->settings->group_name) }}s
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Showing all {{ $block->settings->group_name }}s
        </h1>

        @foreach ($groups as $group)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <a href="{{ route('group', ['block' => $block->name, 'group' => $group->id]) }}">
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ ucfirst($group->name) }}</h4>
                                <div style="width:100%;height:30px;background-color:{{ $group->hex }};"></div>
                                <br>

                                @if (!$group->currentLeader)
                                    <small>{{ ucfirst($group->name) }} is open for claim!</small>
                                @endif

                                <div class=".table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b>Current Leader</b></td>
                                                <td>
                                                    @if (!$group->currentLeader)
                                                        <i>None</i>
                                                    @else
                                                        <a href="{{ route('characterProfile', ['block' => $block->name, 'name' => $group->currentLeader->character->name]) }}">{{ $group->currentLeader->character->name }}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Males</b></td>
                                                <td>{{ $group->males->count() }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Females</b></td>
                                                <td>{{ $group->females->count() }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection