@extends('layouts.block')

@section('title')
    Guidebook
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Guidebook
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Select a guidebook page to view</h1>

        <div class="col-md-4 panel-link">
            <ul>
                @foreach ($block->guidebookPages as $page)
                    <li class="var_nav">
                        <div class="link_bg"></div>
                        <div class="link_title">
                            <div class="icon">
                                <i class="fa fa-circle fa-2x" aria-hidden="true"></i>
                            </div>
                            <a href="/guidebook/{{ $page->id }}"><span>{{ $page->name }}</span></a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-md-8">
            <p class="text-justify">
                Please be sure to read all the guiebook pages <b>before</b> creating a character on this forum. Sites built off of <a href="www.rpblock.com">RPBlock</a> do not require extra account for characters. Instead, make sure to register a single master "ooc" account and create characters underneath it.
            </p>
        </div>
    </div>
@endsection