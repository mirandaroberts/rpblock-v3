@extends('layouts.block')

@section('title')
    Guidebook
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Guidebook
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Viewing page {{ $page->name }}</h1>

        <div class="col-md-4 panel-link">
            <ul>
                @foreach ($block->guidebookPages as $p)
                    <li class="var_nav">
                        <div class="link_bg"></div>
                        <div class="link_title">
                            <div class="icon">
                                @if ($p->id == $page->id)
                                    <i class="fa fa-circle-o fa-2x"></i>
                                @else
                                    <i class="fa fa-circle fa-2x" aria-hidden="true"></i>
                                @endif
                            </div>
                            <a href="/guidebook/{{ $p->id }}"><span>{{ $p->name }}</span></a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-md-8">
            {!! $page->body !!}
        </div>
    </div>
@endsection