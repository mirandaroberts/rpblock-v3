@extends('layouts.block')

@section('title')
    Create Character
@endsection

@section('breadcrumbs')
    <a href="/characters">Characters</a>
    Create Character
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Create Character
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Create a character to submit for approval
        </h1>

        <div class="row">
            <p class="text-justify">Use the form below to create a new character. Once the forum is submitted, any {{ $block->settings->currency_name }} required to make it will be removed from your account and the character will be sent to the <a href="/modbox">modbox</a> for staff approval. You may edit any aspect of the character before acceptance, but some traits may be locked once the character is approved. Be sure to thoroughly read the guidebook before submitting a character for approval.</p>

            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">First Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label for="surname" class="col-md-4 control-label">Surname</label>

                    <div class="col-md-6">
                        <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" autofocus>

                        @if ($errors->has('surname'))
                            <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                    <label for="gender" class="col-md-4 control-label">Sex</label>

                    <div class="col-md-6">
                        <select id="gender" class="form-control" name="gender">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>

                        @if ($errors->has('gender'))
                            <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                    <label for="age" class="col-md-4 control-label">Years Old</label>

                    <div class="col-md-6">
                        <input id="age" type="text" class="form-control" name="age" value="{{ old('age') }}" placeholder="Number value only!" required autofocus>

                        @if ($errors->has('age'))
                            <span class="help-block">
                            <strong>{{ $errors->first('age') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('birth_month') ? ' has-error' : '' }}">
                    <label for="birth_month" class="col-md-4 control-label">Birth Month</label>

                    <div class="col-md-6">
                        <select id="birth_month" class="form-control" name="birth_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>

                        @if ($errors->has('birth_month'))
                            <span class="help-block">
                            <strong>{{ $errors->first('birth_month') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                @foreach ($attributes as $attribute)
                    @if ($attribute->multi_attribute)
                        <div class="form-group{{ $errors->has('attributes[]') ? ' has-error' : '' }}">
                            <label for="attributes[]" class="col-md-4 control-label">{{ $attribute->name }}</label>

                            <div class="col-md-6">
                                <button type="button" class="@if ($attribute->value) add-value @else add-trait @endif btn btn-primary btn-xs" id="{{ $attribute->id }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $attribute->name }}
                                </button>

                                @if ($errors->has('attributes[]'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('attributes[]') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="form-group{{ $errors->has('attributes[]') ? ' has-error' : '' }}">
                            <label for="attributes[]" class="col-md-4 control-label">{{ $attribute->name }}</label>

                            <div class="col-md-6">
                                <select id="attributes[]" class="form-control" name="attributes[]">
                                    @foreach ($attribute->traits as $trait)
                                        <option value="{{ $trait->id }}">
                                            {{ $trait->name }}
                                            @if ($trait->cost)
                                                ({{ $trait->cost}} {{ $block->settings->currency_name }})
                                            @endif
                                        </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('attributes[]'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('attributes[]') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    @endif
                @endforeach

                <hr>

                @if ($block->hasWidget(1))
                    <div class="form-group{{ $errors->has('mother') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Mother</label>

                        <div class="col-md-6">
                            <input id="mother" type="text" class="form-control" name="mother" value="{{ old('mother') }}" placeholder="Leave empty if not born on site" autofocus>

                            @if ($errors->has('mother'))
                                <span class="help-block">
                                <strong>{{ $errors->first('mother') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('father') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Father</label>

                        <div class="col-md-6">
                            <input id="father" type="text" class="form-control" name="father" value="{{ old('father') }}" placeholder="Leave empty if not born on site" autofocus>

                            @if ($errors->has('father'))
                                <span class="help-block">
                                <strong>{{ $errors->first('father') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <hr>

                <div class="form-group{{ $errors->has('appearance') ? ' has-error' : '' }}">
                    <label for="appearance" class="col-md-4 control-label">Appearance</label>

                    <div class="col-md-6">
                        <textarea id="appearance" class="form-control" name="appearance">{{ old('appearance') }}</textarea>

                        @if ($errors->has('appearance'))
                            <span class="help-block">
                            <strong>{{ $errors->first('appearance') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('personality') ? ' has-error' : '' }}">
                    <label for="personality" class="col-md-4 control-label">Personality</label>

                    <div class="col-md-6">
                        <textarea id="personality" class="form-control" name="personality">{{ old('personality') }}</textarea>

                        @if ($errors->has('personality'))
                            <span class="help-block">
                            <strong>{{ $errors->first('personality') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('history') ? ' has-error' : '' }}">
                    <label for="history" class="col-md-4 control-label">History</label>

                    <div class="col-md-6">
                        <textarea id="history" class="form-control" name="history">{{ old('history') }}</textarea>

                        @if ($errors->has('history'))
                            <span class="help-block">
                            <strong>{{ $errors->first('history') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('relationships') ? ' has-error' : '' }}">
                    <label for="relationships" class="col-md-4 control-label">Relationships</label>

                    <div class="col-md-6">
                        <textarea id="relationships" class="form-control" name="relationships">{{ old('relationships') }}</textarea>

                        @if ($errors->has('relationships'))
                            <span class="help-block">
                            <strong>{{ $errors->first('relationships') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                @if ($block->hasWidget(9))
                    <div class="form-group{{ $errors->has('pvp') ? ' has-error' : '' }}">
                        <label for="pvp" class="col-md-4 control-label">PVP</label>

                        <div class="col-md-6">
                            <select name="pvp" id="pvp" class="form-control" aria-required="true">
                                <option value="Closed" @if (old('pvp') == 'Closed') selected @endif>Closed</option>
                                <option value="Optional" @if (old('pvp') == 'Optional') selected @endif>Optional</option>
                                <option value="Open" @if (old('pvp') == 'Open') selected @endif>Open</option>
                                <option value="Hardcore" @if (old('pvp') == 'Hardcore') selected @endif>Hardcore</option>
                            </select>

                            @if ($errors->has('pvp'))
                                <span class="help-block">
                                <strong>{{ $errors->first('pvp') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit for Approval
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.add-trait').on('click', function() {
                var data = {
                    _token: '{{ csrf_token() }}',
                    attr: $(this).attr('id')
                };

                var that = $(this);

                $.ajax({
                    type: "POST",
                    url: '/process/traits',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type === 'error') {
                            alert(data.message);
                        } else {
                            console.log(data);
                            that.parent().append('<div class="trait"><select id="attributes[]" class="form-control" ' +
                                ' name="attributes[]"> ' + data['traits'] +
                                '</select><span class="remove">Remove Trait</span></div>');
                        }
                    },
                    error: function(data) {
                        // Error...
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            });

            $('.add-value').on('click', function() {
                var data = {
                    _token: '{{ csrf_token() }}',
                    attr: $(this).attr('id')
                };

                var that = $(this);

                $.ajax({
                    type: "POST",
                    url: '/process/traits',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type === 'error') {
                            alert(data.message);
                        } else {
                            that.parent().append('<div class="trait"><select id="attributes[]" class="form-control val" ' +
                                ' name="attributes[]"> ' + data['traits'] + '</select>' +
                                '<input type="text" name="values[' + data['attr'] + '][]" placeholder="Type a value here" class="form-control">' +
                                '<span class="remove">Remove Trait</span></div>');
                        }
                    },
                    error: function(data) {
                        // Error...
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            });

            $(document).on('click', '.remove', function() {
                $(this).parent().remove();
            });

            // Change value name
            $(document).on('change', '.val', function() {
                var element = $(this).next();
                element.attr('name', 'values[' + $(this).val() + '][]');
                console.log('Trait Changed: ' + element.attr('name'));
            });
        });
    </script>
@endsection