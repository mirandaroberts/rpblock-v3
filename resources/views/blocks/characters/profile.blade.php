@extends('layouts.block')

@section('title')
    Character - {{ $character->name }}
@endsection

@section('breadcrumbs')
    Characters
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            {{ $character->fullName() }}
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            {{ $character->getActiveStatus() }}

            <span class="pull-right">
                @if (isset($player))
                    <a href="/report/character/{{ $character->id }}"><i class="fa fa-flag"> Report</i></a>

                    @if ($character->player->id == $player->id || $player->isStaff())
                        &nbsp;&nbsp; <a href="/character/{{ $character->name }}/edit"><i class="fa fa-pencil"> Edit</i></a>
                    @endif
                @endif
            </span>
        </h1>

        <div class="row">
            <div class="col-md-4 text-center">
                <b>{{ $character->name }} {{ $character->surname }}</b>
                <br>

                <img src="{{ $character->icon }}" class="avatar" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}">

                <br>Level {{ $character->level }}

                @if ($block->hasWidget(9))
                    {!! $character->pvpRank() !!}
                @endif
                <br>

                @if ($block->hasWidget(4))
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{ $character->health }}%">
                            {{ $character->health }}/100
                        </div>
                    </div>
                @endif

                <h1 class="group-type">{{ $character->groupName() }}</h1>

                {{ $character->rankName() }}
            </div>

            <div class="col-md-8">
                <h4>Traits</h4>
                <ul class="list-group">
                    @foreach ($character->traits as $trait)
                        <li class="list-group-item">
                            <b>{{ ucfirst($trait->data->attribute->name) }}</b> {{ ucfirst($trait->data->name) }}

                            @if ($trait->value) - {{ ucfirst($trait->value) }} @endif

                            @if ($block->settings->enable_currency)
                                <span class="badge">{{ $trait->data->cost }} {{ $block->settings->curreny_name }}</span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <table class="table">
            <thead>
            <tr>
                <td><b>Posts</b></td>
                <td><b>Age</b></td>
                <td><b>Birth Month</b></td>
                <td><b>Sex</b></td>
                <td><b>Posted</b></td>
                <td><b>Played By</b></td>
                <td><b>Created</b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $character->post_count }}</td>
                <td>{{ $character->age() }}</td>
                <td>{{ $character->birthMonthString() }}</td>
                <td>
                    {{ ucfirst($character->gender) }}
                    @if ($character->gender == 'male')
                        <i class="fa fa-mars" aria-hidden="true"></i>
                    @else
                        <i class="fa fa-venus" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if ($character->last_post)
                        {{ $character->last_post->diffForhumans() }}
                    @else
                        Never
                    @endif
                </td>
                <td><a href="/player/{{ $character->player->username }}">{{ $character->player->username }}</a></td>
                <td>{{ $character->created_at->format('m-d-Y') }}</td>
            </tr>
            </tbody>
        </table>
        <div class="text-center">
            <a href="/character/{{ $character->name }}/threads" class="btn btn-primary"><span><i class="fa fa-comment" aria-hidden="true"></i> Thread Log</span></a>
            @if ($block->hasWidget(4))
                <a href="/character/{{ $character->name }}/fights" class="btn btn-primary"><span><i class="fa fa-bomb" aria-hidden="true"></i> Fight Log</span></a>
            @endif
            @if ($block->hasWidget(3))
                <a href="/character/{{ $character->name }}/tasks" class="btn btn-primary"><span><i class="fa fa-clipboard" aria-hidden="true"></i> Task Log</span></a>
            @endif
            <a href="/character/{{ $character->name }}/achievements" class="btn btn-primary"><span><i class="fa fa-trophy" aria-hidden="true"></i> Achievements</span></a>
            @if ($block->hasWidget(1))
                <a href="/character/{{ $character->name }}/lineage" class="btn btn-primary"><span><i class="fa fa-code-fork" aria-hidden="true"></i> Lineage</span></a>
            @endif
        </div>
    </div>

    @if ($block->hasWidget(3))
        <div class="col-md-12 forum-category rounded top lpad">
            <div class="col-md-10">
                {{ $character->fullName() }} Skills
            </div>
        </div>

        <div class="col-md-12 toggleview normal lpad">
            <h1 class="inset">Skills and Tasks</h1>

        </div>
    @endif

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Appearance
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Appearance</h1>

        {!! $character->appearance !!}
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Personality
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Personality</h1>

        {!! $character->personality !!}
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            History
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">History</h1>

        {!! $character->history !!}
    </div>

    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Relationships
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">Relationships</h1>

        {!! $character->relationships !!}
    </div>

    @if (isset($player))
        @if ($character->player->id != $player->id && $player->isStaff())
            <div class="col-md-12 forum-category rounded top lpad">
                <div class="col-md-10">
                    Notes
                </div>
            </div>

            <div class="col-md-12 toggleview normal lpad">
                <h1 class="inset">Viewable to staff only</h1>

                {!! $character->notes !!}
            </div>
        @endif
    @endif
@endsection
