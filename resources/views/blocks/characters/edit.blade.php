@extends('layouts.block')

@section('title')
    Edit Character
@endsection

@section('breadcrumbs')
    <a href="/characters">Characters</a>
    Edit Character
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Edit Character
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            Editing {{ $character->name }}
        </h1>

        <div class="row">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{ csrf_field() }}

                @if ($player->isStaff() || !$character->accepted)
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">First Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $character->name }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Icon</label>

                    <div class="col-md-6">
                        <input id="icon" type="text" class="form-control" name="icon" value="{{ $character->icon }}" autofocus>
                        <small>{{ $block->settings->avatar_width }} x {{ $block->settings->avatar_height }}</small>

                        @if ($errors->has('icon'))
                            <span class="help-block">
                                <strong>{{ $errors->first('icon') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label for="surname" class="col-md-4 control-label">Surname</label>

                    <div class="col-md-6">
                        <input id="surname" type="text" class="form-control" name="surname" value="{{ $character->surname }}" autofocus>

                        @if ($errors->has('surname'))
                            <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                @if ($player->isStaff() || !$character->accepted)
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <label for="gender" class="col-md-4 control-label">Sex</label>

                        <div class="col-md-6">
                            <select id="gender" class="form-control" name="gender">
                                <option value="male" @if ($character->gender == 'male') selected @endif>Male</option>
                                <option value="female" @if ($character->gender == 'female') selected @endif>Female</option>
                            </select>

                            @if ($errors->has('gender'))
                                <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                        <label for="age" class="col-md-4 control-label">Years Old</label>

                        <div class="col-md-6">
                            <input id="age" type="text" class="form-control" name="age" value="{{ floor($character->months_old / 12) }}" required autofocus>

                            @if ($errors->has('age'))
                                <span class="help-block">
                                <strong>{{ $errors->first('age') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('birth_month') ? ' has-error' : '' }}">
                        <label for="birth_month" class="col-md-4 control-label">Birth Month</label>

                        <div class="col-md-6">
                            <select id="birth_month" class="form-control" name="birth_month">
                                <option value="1" @if ($character->birth_month == 1) selected @endif>January</option>
                                <option value="2" @if ($character->birth_month == 2) selected @endif>February</option>
                                <option value="3" @if ($character->birth_month == 3) selected @endif>March</option>
                                <option value="4" @if ($character->birth_month == 4) selected @endif>April</option>
                                <option value="5" @if ($character->birth_month == 5) selected @endif>May</option>
                                <option value="6" @if ($character->birth_month == 6) selected @endif>June</option>
                                <option value="7" @if ($character->birth_month == 7) selected @endif>July</option>
                                <option value="8" @if ($character->birth_month == 8) selected @endif>August</option>
                                <option value="9" @if ($character->birth_month == 9) selected @endif>September</option>
                                <option value="10" @if ($character->birth_month == 10) selected @endif>October</option>
                                <option value="11" @if ($character->birth_month == 11) selected @endif>November</option>
                                <option value="12" @if ($character->birth_month == 12) selected @endif>December</option>
                            </select>

                            @if ($errors->has('birth_month'))
                                <span class="help-block">
                                <strong>{{ $errors->first('birth_month') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                @foreach ($attributes as $attribute)
                    @if (!$attribute->fixed || !$character->accepted|| ($player->isStaff() && $character->player->id != $player->id))
                        @if ($attribute->multi_attribute)
                            <div class="form-group{{ $errors->has('attributes[]') ? ' has-error' : '' }}">
                                <label for="attributes[]" class="col-md-4 control-label">{{ $attribute->name }}</label>

                                <div class="col-md-6">
                                    <button type="button" class="@if ($attribute->value) add-value @else add-trait @endif  btn btn-primary btn-xs" id="{{ $attribute->id }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $attribute->name }}
                                    </button>

                                    @foreach ($character->traits as $trait)
                                        @if ($trait->data->attribute->id == $attribute->id)
                                            <div class="trait">
                                                <select id="attributes[]" class="form-control" name="attributes[]">
                                                    @foreach ($trait->data->attribute->traits as $t)
                                                        <option value="{{ $t->id }}" @if ($t->id == $trait->data->id) selected @endif>{{ $t->name }}
                                                            @if ($t->cost)
                                                                ({{ $t->cost}} {{ $block->settings->currency_name }})
                                                            @endif
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($trait->data->attribute->value)
                                                    <input type="text" value="{{ $trait->value }}" class="form-control" name="values[{{ $trait->data->id }}][]">
                                                @endif
                                            <span class="remove">Remove Trait</span></div>
                                        @endif
                                    @endforeach

                                    @if ($errors->has('attributes[]'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('attributes[]') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="form-group{{ $errors->has('attributes[]') ? ' has-error' : '' }}">
                                <label for="attributes[]" class="col-md-4 control-label">{{ $attribute->name }}</label>

                                <div class="col-md-6">
                                    <select id="attributes[]" class="form-control" name="attributes[]">
                                        @foreach ($attribute->traits as $trait)
                                            <option value="{{ $trait->id }}" @if ($character->hasTrait($trait->id)) selected @endif>
                                                {{ $trait->name }}
                                                @if ($trait->cost && $block->settings->enable_currency)
                                                    ({{ $trait->cost}} {{ $block->settings->currency_name }})
                                                @endif
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('attributes[]'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('attributes[]') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach

                <hr>

                @if ($block->hasWidget(1))
                    <div class="form-group{{ $errors->has('mother') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Mother</label>

                        <div class="col-md-6">
                            <input id="mother" type="text" class="form-control" name="mother" value="{{ $character->mother }}" placeholder="Leave empty if not born on site" autofocus>

                            @if ($errors->has('mother'))
                                <span class="help-block">
                                <strong>{{ $errors->first('mother') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('father') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Father</label>

                        <div class="col-md-6">
                            <input id="father" type="text" class="form-control" name="father" value="{{ $character->father }}" placeholder="Leave empty if not born on site" autofocus>

                            @if ($errors->has('father'))
                                <span class="help-block">
                                <strong>{{ $errors->first('father') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <hr>

                <div class="form-group{{ $errors->has('appearance') ? ' has-error' : '' }}">
                    <label for="appearance" class="col-md-4 control-label">Appearance</label>

                    <div class="col-md-6">
                        <textarea id="appearance" class="form-control" name="appearance">{{ $character->appearance }}</textarea>

                        @if ($errors->has('appearance'))
                            <span class="help-block">
                            <strong>{{ $errors->first('appearance') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('personality') ? ' has-error' : '' }}">
                    <label for="personality" class="col-md-4 control-label">Personality</label>

                    <div class="col-md-6">
                        <textarea id="personality" class="form-control" name="personality">{{ $character->personality }}</textarea>

                        @if ($errors->has('personality'))
                            <span class="help-block">
                            <strong>{{ $errors->first('personality') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('history') ? ' has-error' : '' }}">
                    <label for="history" class="col-md-4 control-label">History</label>

                    <div class="col-md-6">
                        <textarea id="history" class="form-control" name="history">{{ $character->history }}</textarea>

                        @if ($errors->has('history'))
                            <span class="help-block">
                            <strong>{{ $errors->first('history') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('relationships') ? ' has-error' : '' }}">
                    <label for="relationships" class="col-md-4 control-label">Relationships</label>

                    <div class="col-md-6">
                        <textarea id="relationships" class="form-control" name="relationships">{{ $character->relationships }}</textarea>

                        @if ($errors->has('relationships'))
                            <span class="help-block">
                            <strong>{{ $errors->first('relationships') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                @if ($block->hasWidget(9))
                    <div class="form-group{{ $errors->has('pvp') ? ' has-error' : '' }}">
                        <label for="pvp" class="col-md-4 control-label">PVP</label>

                        <div class="col-md-6">
                            <select name="pvp" id="pvp" class="form-control" aria-required="true">
                                <option value="Closed" @if ($character->pvp == 'Closed') selected @endif>Closed</option>
                                <option value="Optional" @if ($character->pvp == 'Optional') selected @endif>Optional</option>
                                <option value="Open" @if ($character->pvp == 'Open') selected @endif>Open</option>
                                <option value="Hardcore" @if ($character->pvp == 'Hardcore') selected @endif>Hardcore</option>
                            </select>

                            @if ($errors->has('pvp'))
                                <span class="help-block">
                                <strong>{{ $errors->first('pvp') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.add-trait').on('click', function() {
                var data = {
                    _token: '{{ csrf_token() }}',
                    attr: $(this).attr('id')
                };

                var that = $(this);

                $.ajax({
                    type: "POST",
                    url: '/process/traits',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'error') {
                            alert(data.message);
                        } else {
                            that.parent().append('<div class="trait"><select id="attributes[]" class="form-control" ' +
                                ' name="attributes[]"> ' + data['traits'] +
                                '</select><span class="remove">Remove Trait</span></div>');
                        }
                    },
                    error: function(data) {
                        // Error...
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            });

            $('.add-value').on('click', function() {
                var data = {
                    _token: '{{ csrf_token() }}',
                    attr: $(this).attr('id')
                };

                var that = $(this);

                $.ajax({
                    type: "POST",
                    url: '/process/traits',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'error') {
                            alert(data.message);
                        } else {
                            that.parent().append('<div class="trait"><select id="attributes[]" class="form-control val" ' +
                                ' name="attributes[]"> ' + data['traits'] + '</select>' +
                                '<input type="text" name="values[' + data['attr'] + '][]" placeholder="Type a value here" class="form-control">' +
                                '<span class="remove">Remove Trait</span></div>');
                        }
                    },
                    error: function(data) {
                        // Error...
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            });

            // Remove Trait
            $(document).on('click', '.remove', function() {
                $(this).parent().remove();
            });

            // Change value name
            $(document).on('change', '.val', function() {
                var element = this.next();
                element.attr('name', 'values[' + this.value() + '][]');
            });
        });
    </script>
@endsection