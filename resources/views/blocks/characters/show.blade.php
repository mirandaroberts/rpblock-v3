@extends('layouts.block')

@section('title')
    Characters

    @if ($name)
         - {{ $name }}
    @endif
@endsection

@section('breadcrumbs')
    Characters
@endsection

@section('content')
    <div class="col-md-12 forum-category rounded top lpad">
        <div class="col-md-10">
            Characters
        </div>
    </div>

    <div class="col-md-12 toggleview normal lpad">
        <h1 class="inset">
            @if ($name)
                {{ $name }}'s Characters
            @else
                All Characters
            @endif

            @if (isset($player))
                <span class="pull-right">
                    <a href="{{ route('createCharacter', ['block' => $block->name]) }}"><i class="fa fa-plus"></i> New Character</a>
                </span>
            @endif
        </h1>

        <div class="text-center">
            @if ($name)
                {{ $name }} has <b>{{ $characters->count() }}</b> characters.
            @else
                There are <b>{{ $block->characters->count() }}</b> total characters.
            @endif

            <br><br>{{ $characters->links() }}
        </div>

        @foreach ($characters as $character)
            <div class="col-md-4 text-center">
                <div class="thumbnail">
                    <a href="{{ route('characterProfile', ['block' => $block->name, 'name' => $character->name]) }}">

                        <img src="{{ $character->icon }}" class="avatar" width="{{ $block->settings->avatar_width }}" height="{{ $block->settings->avatar_height }}">
                        <div class="caption">
                            <div class="text-center">
                                <h4 class="inline">{{ $character->fullName() }}</h4>
                                <small>{{ $character->getActiveStatus() }}</small>

                                <h1 class="group-type">{{ $character->groupName() }}</h1>

                                <div class=".table-responsive">
                                    <table class="table">
                                        <tbody><tr>
                                            <td><b>Player</b></td>
                                            <td><a href="{{ route('playerProfile', ['block' => $block->name, 'name' => $character->player->username]) }}">{{ $character->player->username }}</a></td>
                                        </tr>
                                        <tr>
                                            <td><b>Age</b></td>
                                            <td>{{ $character->age() }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Sex</b></td>
                                            <td>{{ ucfirst($character->gender) }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Posts</b></td>
                                            <td>{{ $character->post_count }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Last Posted</b></td>
                                            <td>
                                                @if ($character->last_post)
                                                    {{ $character->last_post->diffForHumans() }}
                                                @else
                                                    Never
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection