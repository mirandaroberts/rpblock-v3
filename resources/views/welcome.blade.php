@extends('layouts.app')
@section('title', 'Welcome!')

@section('content')
    <div class="blocks">
        <iframe width="100%" height="100%" frameborder="0" src="blocks/blocks.html"></iframe>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Fully Customizable</h2>
                <p>As a block admin, you will have the ability to customize every feature of your site from the layout to the weather patterns to the fight system using our robust admin tools.</p>
                <p><a class="btn btn-default" href="/about" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Mobile Friendly</h2>
                <p>Our mobile firendly layout allows your players to easily participate from any mobile device hassle free!</p>
                <p><a class="btn btn-default" href="/about" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Robust RPG Solution</h2>
                <p>RPBlock is the first forum software created with roleplay games in mind. We feature a wide array of quality of life features for roleplayers and believe in hassle free one-click staff tools.</p>
                <p><a class="btn btn-default" href="/about" role="button">View details &raquo;</a></p>
            </div>
        </div>

        <hr>

        <footer>
            <p>&copy; Nightowl Workshop LLC 2017</p>
        </footer>
    </div> <!-- /container -->
@endsection