<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{{ ucfirst($block->name) }} - @yield('title')</title>

    <meta name="description" content="{{ $block->settings->description }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.buttons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/js/dist/semantic.min.css">
    <link rel="stylesheet" href="/css/main.css">

    @if (isset($skin))
        @include('blocks.partials.skin')
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

    @yield('header')
</head>

<body>
    <div class="ui right sidebar">
        @include('blocks.partials.sidebar')
    </div>

    <div class="pusher" id="main-wrapper">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header id="#top">
            @include('blocks.partials.navbar', ['block' => $block])
        </header>

        <div class="row">
            <div class="col-md-12 top-msg breadcrumb">
            <span id="breadcrumb">
                <a href="/"><i class="fa fa-home"></i></a>
                @yield('breadcrumbs')
            </span>
            </div>
        </div>

        @if (!$block->active)
            <div class="alert alert-danger container" role="alert">
                <strong>Important!</strong> This forum is currently inactive for the following reasons:

                @if ($block->last_paid != NULL && $block->last_paid > \Carbon\Carbon::now()->subDays(30))
                    <b>Hosting has not been paid.</b>
                @else
                    <b>Down for maintenance.</b>
                @endif

                <br>

                Please contact your forum admin for more details.
            </div>
        @endif

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }} alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <strong>{{ ucfirst($msg) }}!</strong> {{ Session::get('alert-' . $msg) }}
                    </div>
                @endif
            @endforeach
        </div>

        <div class="row">
            <div class="col-md-9">
                @yield('content')
            </div>

            <div class="col-md-3" id="sidebar">
                @include('blocks.partials.sidebar', ['block' => $block])
            </div>
        </div>

        <div class="row" id="content-footer">
            <div class="col-md-12">
                @include('blocks.partials.footer', ['block' => $block])
            </div>
        </div>

        <footer>
            <span class="hidden-xs">This forum is hosted by <a href="http://www.rpblock.com">RPBlock</a>.</span>

            <div class="pull-right col-md-4 col-sm-12 col-xs-12">
                @if (isset($player))
                    <div class="col-md-8 col-sm-6 col-xs-6">
                        <form>
                            <b>Theme Switcher</b>
                            <select id="theme-switcher" class="theme-switcher">
                                <option value="0" @if ($player->skin_id == 0) selected @endif>Main</option>
                                @foreach ($block->skins as $s)
                                    <option value="{{ $s->id }}" @if ($player->skin_id == $s->id) selected @endif>{{ $s->name }}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                @endif

                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="hidden-sm hidden-xs">
                        <a href="/modbox">
                            Modbox

                            @if (isset($player) && $player->isStaff())
                                ({{ $block->tickets->count() }})
                            @endif
                        </a>
                    </div>

                    <div class="visible-sm visible-xs pull-right">
                        <button type="button" class="sidebar-toggle" data-toggle="collapse" aria-expanded="false">
                            <i class="fa fa-bars"></i> Toggle Sidebar
                        </button>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#top" id="top-button">
            <i class="fa fa-caret-up fa-2x"></i>
        </a>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="/js/dist/semantic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.buttons.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.desktop.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>
    <script src="/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>

    <script>
        $(document).ready(function() {
            @if (isset($player))
                PNotify.desktop.permission();

                $('#theme-switcher').change(function() {
                    var data = {
                        '_token': '{{ csrf_token() }}',
                        'skin' : $(this).val()
                    };

                    $.ajax({
                        dataType: 'JSON',
                        url: "/changeskin",
                        data: data,
                        type: 'post',
                        success: function(data){
                            if (data.type == 'error') {
                                alert(data.message);
                            } else if (data == 'success') {
                                location.reload();
                            } else {
                                $('#skin').html(data);
                                location.reload();
                            }
                        },
                        error: function(data) {
                            // Error...
                            console.log(data.responseText);
                            var errors = $.parseJSON(data.responseText);
                            console.log(errors);
                        }
                    });
                });

                @foreach ($player->unreadNotifications as $notice)
                    if (notice{{ $notice->id }}) {
                        notice{{ $notice->id }}.open();
                    } else {
                        var notice{{ $notice->id }} = new PNotify({
                            hide: false,
                            styling: 'bootstrap3',
                            title: 'Notification',
                            text: '{{ $notice->message }}',
                            type: '{{ $notice->type }}',
                            mouse_reset: false,
                            buttons: {
                                closer: true,
                                sticker: false
                            },
                            desktop: {
                                desktop: false
                            }
                        });
                    }

                    notice{{ $notice->id }}.get().css('cursor', 'pointer').click(function(e) {
                        if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;

                        window.location.replace('{{ route('readNotice', ['block' => $block->name, 'id' => $notice->id]) }}');
                        notice{{ $notice->id }}.remove();
                    });
                @endforeach
            @endif
        });
    </script>

    @yield('scripts')
</body>
</html>