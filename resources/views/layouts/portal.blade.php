<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css" rel="stylesheet">

    <link href="/css/portal.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-cube"></i> <span>RPBlock</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li>
                                <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/portal">Dashboard</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-cubes"></i> Blocks <span class="fa fa-chevron-down"></span></a>
                                @foreach (auth()->user()->blocks as $block)
                                    <ul class="nav child_menu">
                                        </li><li><a>{{ $block->name }}<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a href="/portal/blocks/{{ $block->id }}">General Settings</a></li>
                                                <li><a href="/portal/blocks/characters/attributes/{{ $block->id }}">Character Creation</a></li>
                                                <li><a href="/portal/blocks/groups/{{ $block->id }}">Manage Groups</a></li>
                                                <li><a href="/portal/blocks/categories/{{ $block->id }}">Manage Categories</a></li>
                                                <li><a href="/portal/blocks/threadtypes/{{ $block->id }}">Thread Types</a></li>
                                                <li><a href="/portal/blocks/guidebook/{{ $block->id }}">Guidebook</a></li>
                                                <li><a href="/portal/blocks/achievements/{{ $block->id }}">Achievements</a></li>
                                                <li><a href="/portal/blocks/widgets/{{ $block->id }}">Widgets</a></li>
                                                <li><a href="/portal/blocks/skins/{{ $block->id }}">Skins</a></li>
                                                <li><a href="/portal/blocks/items/{{ $block->id }}">Items</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                    @if (auth()->user()->role == 'ROLE_ADMIN')
                    <div class="menu_section">
                        <h3>Admin</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-bug"></i> Admin Options <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/portal/users">Users</a></li>
                                    <li><a href="/portal/blocks">Blocks</a></li>
                                    <li><a href="/portal/plans">Plans</a></li>
                                    <li><a href="/portal/widgets">Widgets</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    @endif

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                {{ auth()->user()->firstname }} {{ auth()->user()->lastname }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:"> Profile</a></li>
                                <li>
                                    <a href="javascript:">
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:">Help</a></li>
                                <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="alert alert-{{ $msg }} alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>{{ ucfirst($msg) }}!</strong> {{ Session::get('alert-' . $msg) }}
                        </div>
                    @endif
                @endforeach
            </div>

            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Others -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
<script data-rocketsrc="/vendor/parsleyjs/dist/parsley.min.js" type="text/rocketscript" data-rocketoptimized="true"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.js"></script>
<script src="/js/smartWizard.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.js"></script>

<!-- Custom Scripts -->
<script src="/js/portal.js"></script>

@yield('scripts')

</body>
</html>