@extends('layouts.portal')
@section('title')
    Portal Dashboard
@endsection

@section('content')
    @if (auth()->user()->role == 'ROLE_ADMIN')
        <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Total Blocks</span>
                <div class="count">{{ number_format($blockCount) }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Players</span>
                <div class="count green">{{ number_format($playerCount) }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> Total Characters</span>
                <div class="count">{{ number_format($characterCount) }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-mars"></i> Total Males</span>
                <div class="count">{{ number_format($maleCount) }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-venus"></i> Total Females</span>
                <div class="count">{{ number_format($femaleCount) }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-pencil"></i> Total Posts</span>
                <div class="count">{{ number_format($postCount) }}</div>
            </div>
        </div>
        <!-- /top tiles -->
    @endif

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                    <h2>Dashboard Home</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x-content">
                    You currently have no issues to address.
                </div>
            </div>
        </div>
    </div>
@endsection