@extends('layouts.portal')

@section('title')
    Portal - Items
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Items</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="/portal/blocks/items/{{ $block->id }}/create">
                            <button class="btn btn-success">New Item</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Icon</th>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Description</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td><img src="{{ $item->icon }}" width="100" height="100"></td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->cost }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                <form action="/portal/blocks/items/update/{{ $item->id }}" class="inline-block">
                                    <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Item</button>
                                </form>

                                <form action="/portal/blocks/items/delete/{{ $item->id }}" class="inline-block">
                                    <button class="btn btn-danger btn-xs">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection