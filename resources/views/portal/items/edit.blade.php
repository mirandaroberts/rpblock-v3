@extends('layouts.portal')

@section('title')
    Portal - Edit Item
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Item - {{ $item->name }}</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="edit-item" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<label>
                            <input type="radio" id="type" required="true" name="type" value="ic" class="form-control col-md-7 col-xs-12" @if (old('type', $item->type) == 'ic') checked @endif>
							IC
						</label>
							<label>
                            <input type="radio" id="type" required="true" name="type" value="ooc" class="form-control col-md-7 col-xs-12" @if (old('type', $item->type) == 'ooc') checked @endif>
							OOC
						</label>

                            @if ($errors->has('type'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="icon">Icon <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="icon" required="true" name="icon" value="{{ old('icon', $item->icon) }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('icon'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="name" required="true" name="name" value="{{ old('name', $item->name) }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="description" required="true" name="description" value="{{ old('description', $item->description) }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost">Cost <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="cost" required="true" name="cost" value="{{ old('cost', $item->cost) }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('cost'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('cost') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="/portal/items" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script data-rocketsrc="/public/vendors/parsleyjs/dist/parsley.min.js" type="text/rocketscript" data-rocketoptimized="true"></script>
@endsection