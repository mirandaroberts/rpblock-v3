@extends('layouts.portal')

@section('title')
    Portal - Widgets
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Widgets</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="/portal/widgets/create">
                            <button class="btn btn-success">New Widget</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($widgets as $widget)
                        <tr>
                            <td>{{ $widget->id }}</td>
                            <td>{{ $widget->name }}</td>
                            <td>{{ $widget->description }}</td>
                            <td>
                                <form action="/portal/widgets/update/{{ $widget->id }}" class="inline-block">
                                    <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Widget</button>
                                </form>

                                <form action="/portal/widgets/delete/{{ $widget->id }}" class="inline-block">
                                    <button class="btn btn-danger btn-xs">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection