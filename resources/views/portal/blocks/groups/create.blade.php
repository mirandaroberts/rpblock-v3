@extends('layouts.portal')

@section('title')
    Portal - Create {{ ucfirst($block->settings->group_name) }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create {{ ucfirst($block->settings->group_name) }}</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="create-plan" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="name" required="required" name="name" value="{{ old('name') }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('hex') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amt">Hex <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="hex" required="required" name="hex" value="{{ old('hex', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                            @if ($errors->has('hex'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('hex') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('copy_ranks') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="copy_ranks">Copy ranks?</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="copy_ranks" id="copy_ranks" class="form-control  col-md-7 col-xs-12">
                                <option value="0">None</option>
                                @foreach ($block->groups as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('copy_ranks'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('copy_ranks') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('portalGroups', ['id' => $block->id]) }}" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.cp').colorpicker({

            });
        })
    </script>
@endsection