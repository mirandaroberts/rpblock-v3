@extends('layouts.portal')

@section('title')
    Portal - {{ ucfirst($block->settings->group_name) }}s
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ ucfirst($block->settings->group_name) }}s</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="{{ route('createGroup', ['id' => $block->id]) }}">
                            <button class="btn btn-success">New {{ ucfirst($block->settings->group_name) }}</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Hex</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($block->groups as $group)
                    <tr>
                        <td>{{ $group->id }}</td>
                        <td>{{ $group->name }}</td>
                        <td><div class="inline-block" style="width:25px; height:10px; background-color:{{ $group->hex }}"></div> {{ $group->hex }}</td>
                        <td>
                            <form action="{{ route('editRanks', ['block' => $block->id, 'id' => $group->id]) }}" class="inline-block">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-users"></i> Edit {{ ucfirst($block->settings->rank_name) }}s</button>
                            </form>

                            <form action="{{ route('editGroup', ['block' => $block->id, 'id' => $group->id]) }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit {{ ucfirst($block->settings->group_name) }}</button>
                            </form>

                            <form action="{{ route('deleteGroup', ['block' => $block->id, 'id' => $group->id]) }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection