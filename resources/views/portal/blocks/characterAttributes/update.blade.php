@extends('layouts.portal')

@section('title')
    Portal - Update Attribute
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Update Attribute - {{ $attribute->name }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form action="" method="post">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-2">
                                    <input type="text" name="name" class="form-control" value="{{ $attribute->name }}" placeholder="Name" required>
                                    <br>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <input type="hidden" name="fixed" value="0" />
                            <div class="form-group{{ $errors->has('fixed') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-sm-6 colxs-12 col-md-offset-2">
                                    <label><input type="checkbox" value="1" name="fixed" class="js-switch" data-switchery="true" @if ($attribute->fixed) checked @endif></label>
                                    <label class="control-label">Fixed</label>
                                    <br><small>The attribute cannot be modified after the character is accepted</small>

                                    @if ($errors->has('fixed'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('fixed') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <input type="hidden" name="multi_attribute" value="0" />
                            <div class="form-group{{ $errors->has('multi_attribute') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-sm-6 colxs-12 col-md-offset-2">
                                    <label><input type="checkbox" value="1" name="multi_attribute" class="js-switch" data-switchery="true" @if ($attribute->multi_attribute) checked @endif></label>
                                    <label class="control-label">Multi-trait</label>
                                    <br><small>The attribute can have many traits.</small>

                                    @if ($errors->has('multi_attribute'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('multi_attribute') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <input type="hidden" name="value" value="0" />
                            <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-sm-6 colxs-12 col-md-offset-2">
                                    <label><input type="checkbox" value="1" name="value" class="js-switch" data-switchery="true" @if ($attribute->value) checked @endif></label>
                                    <label class="control-label">Has Value</label>
                                    <br><small>The attribute has a user-input field.</small>

                                    @if ($errors->has('value'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection