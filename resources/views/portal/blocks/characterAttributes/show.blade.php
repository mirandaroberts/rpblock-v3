@extends('layouts.portal')

@section('title')
    Portal - Character Creation
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ ucfirst($block->name) }} Character Creation</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-justify">When a player creates a character on your block, there are both <b>traits</b> and <b>attributes</b> that can be applied to that character. Out of the box, your block will have only the <b>Gender</b> and <b>Age</b> attributes, so you much set the rest. Attributes describe a part of the character you would like to be defined on site and each attribute will be made up of traits that will define it. For example, you may set up the attribute 'Eye Color' and populate it with the traits 'Blue' and 'Brown'.</p>
                    <p class="text-justify">Attributes are configured by default to only accept one trait, but can also be set to allow a character to have multiple traits. For example you can create an attribute called 'Markings' that characters could have multiple traits for. You can also set the currency cost for any trait if currency is enabled on your block.</p>

                    <div class="ln_solid"></div>

                    <div class="pull-left">
                        <h1>Attributes <span class="badge">{{ $block->characterAttributes->count() }}</span></h1>
                    </div>

                    <div class="pull-right">
                        <form method="get" action="{{ route('createAttribute', ['id' => $block->id]) }}">
                            <button class="btn btn-default"><i class="fa fa-plus"></i> Create Attribute</button>
                        </form>
                    </div>

                    <br class="clear">

                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($block->characterAttributes as $attribute)
                            <div class="panel">
                                <a class="panel-heading collapsed" role="tab" id="heading{{ $attribute->id }}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $attribute->id }}" aria-expanded="false" aria-controls="collapse{{ $attribute->id }}">
                                    <div class="pull-left">
                                        <h4 class="panel-title">{{ $attribute->name }}
                                            <h6>
                                                @if ($attribute->fixed)
                                                    Fixed
                                                @endif

                                                @if ($attribute->fixed && $attribute->multi_attribute)
                                                    //
                                                @endif

                                                @if ($attribute->multi_attribute)
                                                    Multi-trait
                                                @endif

                                                @if ($attribute->value)
                                                     // User-Input
                                                @endif
                                            </h6>
                                        </h4>
                                    </div>

                                    <br class="clear">
                                </a>
                                <div id="collapse{{ $attribute->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $attribute->id }}" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="pull-right"><br>
                                            <div class="text-right">
                                                <form class="inline-block" action="{{ route('createTrait', ['id' => $attribute->id]) }}" method="get">
                                                    <button class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add Trait</button>
                                                </form>

                                                <form class="inline-block" action="{{ route('updateAttribute', ['block' => $block->id, 'id' => $attribute->id]) }}" method="get">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Attribute</button>
                                                </form>

                                                <form class="inline-block" action="{{ route('deleteAttribute', ['block' => $block->id, 'id' => $attribute->id]) }}" method="get">
                                                    <button class="btn btn-danger btn-xs">Delete</button>
                                                </form>
                                            </div>
                                        </div>

                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                @if ($block->settings->enable_currency)
                                                    <th>Cost</th>
                                                @endif
                                                <th>Options</th>
                                            </tr>
                                            </thead>


                                            <tbody>
                                            @foreach ($attribute->traits as $trait)
                                                <tr>
                                                    <td>{{ $trait->id }}</td>
                                                    <td>{{ $trait->name }}</td>
                                                    @if ($block->settings->enable_currency)
                                                        <th>{{ $trait->cost }}</th>
                                                    @endif
                                                    <td>
                                                        <form action="{{ route('editTrait', ['id' => $trait->id, 'block' => $block->id]) }}" method="get" class="inline-block">
                                                            <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Trait</button>
                                                        </form>

                                                        <form action="{{ route('deleteTrait', ['id' => $trait->id, 'block' => $block->id]) }}" method="get" class="inline-block">
                                                            <button class="btn btn-danger btn-xs">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datatable').DataTable();
        });
    </script>
@endsection