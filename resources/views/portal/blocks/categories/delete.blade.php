@extends('layouts.portal')

@section('title')
    Portal - Delete Category
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Delete Category - {{ $category->title }}</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="delete-plan" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                        {{ csrf_field() }}

                        <strong>Warning!</strong> You are about to permanently delete <strong>{{ $category->title }}</strong>. Are you sure you wish to proceed?

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{ route('portalCategories', ['id' => $category->block_id]) }}" class="btn btn-primary">Cancel</a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection