@extends('layouts.portal')

@section('title')
    Portal - Reorder Categories
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Reorder Categories</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-justify">Drag and drop the Categories below until they are in the desired order.</p>

                    <ul class="reorder" id="categories">
                        @foreach ($block->categories as $category)
                                <li class="sort-parents" id="{{ $category->id }}">{{ $category->title }}

                                @if ($category->children->count())
                                    <ul class="reorder" id="subcategories">
                                        @foreach ($category->children as $c)
                                            <li class="sort-children"id="{{ $c->id }}">{{ $c->title }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('portalCategories', ['id' => $block->id]) }}" class="btn btn-primary">Finished</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script>
        $(document).ready(function() {
            // Set data object
            var data = {
                _token: '{{ csrf_token() }}',
                block: {{ $block->id }},
                parents: [0],
                children: [0]
            };

            // Set initial order
            $('.sort-parents').each(function() {
                data.parents.push(this.id);
            });

            $('.sort-children').each(function() {
                data.children.push(this.id);
            });

            // Set list to sortable
            $('#categories').sortable({
                scroll: false,
                scrollSensitivity: 80,
                scrollSpeed: 3,
                update: function(event, ui) {
                    // Clear and repush
                    data.parents = [0];
                    $('.sort-parents').each(function() {
                        data.parents.push(this.id);
                    });

                    update();
                }
            });

            // Set list to sortable
            $('#subcategories').sortable({
                scroll: false,
                scrollSensitivity: 80,
                scrollSpeed: 3,
                update: function(event, ui) {
                    // Clear and repush
                    data.children = [0];
                    $('.sort-ranks').each(function() {
                        data.children.push(this.id);
                    });

                    update();
                }
            });

            function update() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('processReorderCategories') }}',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'error') {
                            alert(data.message);
                        } else {

                        }
                    },
                    error: function(data) {
                        // Error...
                        console.log(data.responseText);
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            }
        });
    </script>

@endsection