@extends('layouts.portal')

@section('title')
    Portal - Create Category
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create Category</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="create-category" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="parent_id">Parent Category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="parent_id" id="parent_id">
                                <option value="0">None</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('parent_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('parent_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="title" name="title" value="{{ old('title') }}" maxlength="255" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="description" name="description" class="form-control col-md-7 col-xs-12"></textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('img') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img">Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="img" name="img" value="{{ old('img') }}" maxlength="255" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('img'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="type" id="type">
                                <option value="ooc">Out of Character (OOC)</option>
                                <option value="ic">In Character (IC)</option>
                            </select>

                            @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('enable_threads') ? ' has-error' : '' }}">
                        <input type="hidden" name="enable_threads" value="0">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enable_threads">Enable Threads</label>
                        <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="enable_threads" class="js-switch" data-switchery="true" @if (old('enable_threads', 1)) checked @endif></label>

                        @if ($errors->has('enable_threads'))
                            <span class="help-block">
                                <strong>{{ $errors->first('enable_threads') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('private') ? ' has-error' : '' }}">
                        <input type="hidden" name="private" value="0">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="private">Private</label>
                        <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="private" class="js-switch" data-switchery="true" @if (old('private', 0)) checked @endif></label>

                        @if ($errors->has('private'))
                            <span class="help-block">
                                <strong>{{ $errors->first('private') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('portalCategories', ['id' => $block->id]) }}" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.cp').colorpicker({

            });
        })
    </script>
@endsection