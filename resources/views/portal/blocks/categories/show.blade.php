@extends('layouts.portal')

@section('title')
    Portal - Categories
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Categories</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="{{ route('createCategory', ['id' => $block->id]) }}">
                            <button class="btn btn-success">New Category</button>
                        </form>

                        <form action="{{ route('reorderCategories', ['id' => $block->id]) }}">
                            <button class="btn btn-primary">Reorder Categories</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Parent</th>
                        <th>Description</th>
                        <th>Threads Enabled</th>
                        <th>Private</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($block->categories as $category)
                    <tr>
                        <td>{{ $category->weight }}</td>
                        <td>{{ $category->type }}</td>
                        <td>{{ $category->title }}</td>
                        <td>
                            @if ($category->parent)
                                {{ $category->parent->title }}
                            @else
                                None
                            @endif
                        </td>
                        <td>{{ $category->description }}</td>
                        <td>
                            @if ($category->enable_threads)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if ($category->private)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('editCategory', ['id' => $category->id]) }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Category</button>
                            </form>

                            <form action="{{ route('deleteCategory', ['id' => $category->id]) }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection