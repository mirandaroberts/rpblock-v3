@extends('layouts.portal')

@section('title')
    Portal - Reorder Guidebook
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Reorder Guidebook</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-justify">Drag and drop the pages below until they are in the desired order.</p>

                    <ul class="reorder" id="pages">
                        @foreach ($block->guidebookPages as $page)
                            <li class="sort-pages" id="{{ $page->id }}">{{ $page->name }}</li>
                        @endforeach
                    </ul>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('portalGuidebook', ['block' => $block->id]) }}" class="btn btn-primary">Finished</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script>
        $(document).ready(function() {
            // Set data object
            var data = {
                _token: '{{ csrf_token() }}',
                block: {{ $block->id }},
                pages: [0]
            };

            // Set initial order
            $('.sort-pages').each(function() {
                data.pages.push(this.id);
            });

            // Set list to sortable
            $('#pages').sortable({
                scroll: false,
                scrollSensitivity: 80,
                scrollSpeed: 3,
                update: function(event, ui) {
                    // Clear and repush
                    data.pages = [0];
                    $('.sort-pages').each(function() {
                        data.pages.push(this.id);
                    });

                    update();
                }
            });

            function update() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('processReorderPages') }}',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'error') {
                            alert(data.message);
                        }
                    },
                    error: function(data) {
                        // Error...
                        console.log(data.responseText);
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            }
        });
    </script>

@endsection