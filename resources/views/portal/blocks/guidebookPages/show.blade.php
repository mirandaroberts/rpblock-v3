@extends('layouts.portal')

@section('title')
    Portal - Guidebook
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Guidebook Pages</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <form action="{{ route('createGuidebookPage', ['block' => $block->id]) }}">
                                <button class="btn btn-success">New Page</button>
                            </form>

                            <form action="{{ route('reorderGuidebook', ['block' => $block->id]) }}">
                                <button class="btn btn-primary">Reorder Guidebook</button>
                            </form>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Order</th>
                            <th>Name</th>
                            <th>Options</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($block->guidebookPages as $page)
                            <tr>
                                <td>{{ $page->weight }}</td>
                                <td>{{ $page->name }}</td>
                                <td>
                                    <form action="{{ route('editGuidebookPage', ['block' => $block->id, 'id' => $page->id]) }}" class="inline-block">
                                        <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Page</button>
                                    </form>

                                    <form action="{{ route('deleteGuidebookPage', ['block' => $block->id, 'id' => $page->id]) }}" class="inline-block">
                                        <button class="btn btn-danger btn-xs">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datatable').DataTable();
        });
    </script>
@endsection