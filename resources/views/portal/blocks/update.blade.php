@extends('layouts.portal')

@section('title')
    Portal - Edit Block
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Edit Block</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="edit-block" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user">User <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="user" name="user_id" class="form-control col-md-7 col-xs-12">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @if ($block->user->id == $user->id) selected @endif>{{ $user->username }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('user_id'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('plan_id') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="plan">Plan <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="plan" name="plan_id" class="form-control col-md-7 col-xs-12">
                                    @foreach ($plans as $plan)
                                        <option value="{{ $plan->id }}" @if ($block->plan->id == $plan->id) selected @endif>{{ $plan->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('plan_id'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('plan_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" required="required" name="name" value="{{ $block->name }}" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/portal/blocks" class="btn btn-primary">Cancel</a>
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script data-rocketsrc="/public/vendors/parsleyjs/dist/parsley.min.js" type="text/rocketscript" data-rocketoptimized="true"></script>
@endsection