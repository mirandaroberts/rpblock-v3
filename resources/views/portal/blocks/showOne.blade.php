@extends('layouts.portal')

@section('title')
    Portal - {{ $block->name  }}
@endsection

@section('content')
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Players</span>
            <div class="count">1</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-users"></i> Total Characters</span>
            <div class="count">0</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-mars"></i> Total Males</span>
            <div class="count">0</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-venus"></i> Total Females</span>
            <div class="count">0</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-pencil"></i> Total Posts</span>
            <div class="count">0</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-paperclip"></i> Total Threads</span>
            <div class="count">0</div>
        </div>
    </div>
    <!-- /top tiles -->

    @if ($block->initialized)
        @include('portal.blocks.reinitialize')
    @else
        @include('portal.blocks.initialize')
    @endif
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();

            document.querySelector('#initiate_block_enable_currency').addEventListener('click', function() {
                $(".currency-group :input").each(function() {
                    if ($(this).prop("disabled") === true) {
                        $(this).prop("disabled", false);
                    } else {
                        $(this).prop("disabled", true);
                    }
                });
            });
        });
    </script>
@endsection