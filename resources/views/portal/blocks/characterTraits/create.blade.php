@extends('layouts.portal')

@section('title')
    Portal - Create Trait
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Trait for Attribute {{ $attribute->name }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form action="" method="post">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-2">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name" required>
                                    <br>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if ($block->settings->enable_currency)
                            <div class="row">
                                <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                                    <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-2">
                                        <input type="text" name="cost" class="form-control" value="{{ old('cost', 0) }}" placeholder="Cost" required>
                                        <br>

                                        @if ($errors->has('cost'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('cost') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection