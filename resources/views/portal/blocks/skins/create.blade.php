@extends('layouts.portal')

@section('title')
    Portal - Create Skin
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Skin</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-justify">Use the form below to create a new skin for your block. Please note that RPBlock reserves the right to remove any skins that are deemed inappropriate or break our terms of service.</p>
                    <hr>
                    <form id="create-skin" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" name="name" value="{{ old('name') }}" maxlength="50" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('banner') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner">Banner <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="banner" name="banner" value="{{ old('banner') }}" maxlength="255" class="form-control col-md-7 col-xs-12">
                                <small>Must be at least 500px high. Width is dependant player resolution, a minimum width of 950px is recommended but remember some will be cut off at smaller screen sizes.</small>

                                @if ($errors->has('banner'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('banner') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('header_bg') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header_bg">Header Background</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="header_bg" name="header_bg" value="{{ old('header_bg') }}" maxlength="255" class="form-control col-md-7 col-xs-12">
                                <small>A repeating background highly recommended. This is optional.</small>

                                @if ($errors->has('header_bg'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('header_bg') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bg_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bg_color">Background Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="bg_color" required="required" name="bg_color" value="{{ old('bg_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('bg_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bg_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('font_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="font_color">Font Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="font_color" required="required" name="font_color" value="{{ old('font_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('font_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('font_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link_color">Link Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="link_color" required="required" name="link_color" value="{{ old('link_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('link_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_hover') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link_hover">Link Hover Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="link_hover" required="required" name="link_hover" value="{{ old('link_hover', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('link_hover'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_hover') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_visited') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link_visited">Link Visited Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="link_visited" required="required" name="link_visited" value="{{ old('link_visited', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('link_visited'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_visited') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_active') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link_active">Link Active Color <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="link_active" required="required" name="link_active" value="{{ old('link_active', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('link_active'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_active') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('accent_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="accent_color">Accent Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="accent_color" required="required" name="accent_color" value="{{ old('accent_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('accent_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accent_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('accent_font') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="accent_font">Accent Font <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="accent_font" required="required" name="accent_font" value="{{ old('accent_font', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('accent_font'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accent_font') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('accent_dark') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="accent_dark">Accent Dark <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="accent_dark" required="required" name="accent_dark" value="{{ old('accent_dark', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('accent_dark'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accent_dark') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('secondary_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="secondary_color">Secondary Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="secondary_color" required="required" name="secondary_color" value="{{ old('secondary_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('secondary_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('secondary_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('secondary_font') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="secondary_font">Secondary Font <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="secondary_font" required="required" name="secondary_font" value="{{ old('secondary_font', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('secondary_font'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('secondary_font') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_color') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_color">Category Color <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="category_color" required="required" name="category_color" value="{{ old('category_color', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('category_color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_font') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_font">Category Font <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="category_font" required="required" name="category_font" value="{{ old('category_font', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('category_font'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_font') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_icon') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_icon">Category Icon <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="category_icon" required="required" name="category_icon" value="{{ old('category_icon', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('category_icon'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_icon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_border') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_border">Category Border <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="category_border" required="required" name="category_border" value="{{ old('category_border', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('category_border'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_border') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('navbar_links') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="navbar_links">Navbar Links <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="navbar_links" required="required" name="navbar_links" value="{{ old('navbar_links', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('navbar_links'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('navbar_links') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('navbar_hover') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="navbar_hover">Navbar Hover <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="navbar_hover" required="required" name="navbar_hover" value="{{ old('navbar_hover', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('navbar_hover'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('navbar_hover') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('breadcrumbs') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="breadcrumbs">Breadcrumbs <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="breadcrumbs" required="required" name="breadcrumbs" value="{{ old('breadcrumbs', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('breadcrumbs'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('breadcrumbs') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('breadcrumbs_border') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="breadcrumbs_border">Breadcrumbs Border <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="breadcrumbs_border" required="required" name="breadcrumbs_border" value="{{ old('breadcrumbs_border', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('breadcrumbs_border'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('breadcrumbs_border') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('breadcrumbs_links') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="breadcrumbs_links">Breadcrumbs Links <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="breadcrumbs_links" required="required" name="breadcrumbs_links" value="{{ old('breadcrumbs_links', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('breadcrumbs_links'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('breadcrumbs_links') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('breadcrumbs_hover') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="breadcrumbs_hover">Breadcrumbs Hover <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="breadcrumbs_hover" required="required" name="breadcrumbs_hover" value="{{ old('breadcrumbs_hover', '#000000') }}" maxlength="7" class="form-control col-md-7 col-xs-12 cp">

                                @if ($errors->has('breadcrumbs_hover'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('breadcrumbs_hover') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{ route('portalSkins', ['block' => $block->id]) }}" class="btn btn-primary">Cancel</a>
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.cp').colorpicker({

            });
        })
    </script>
@endsection