@extends('layouts.portal')

@section('title')
    Portal - Skins
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Skins</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <form action="{{ route('createSkin', ['block' => $block->id]) }}">
                                <button class="btn btn-success">New Skin</button>
                            </form>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Options</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>Main</td>
                            <td></td>
                        </tr>
                        @foreach ($block->skins as $skin)
                            <tr>
                                <td>{{ $skin->name }}</td>
                                <td>
                                    <form action="{{ route('editSkin', ['block' => $block->id, 'id' => $skin->id]) }}" class="inline-block">
                                        <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Skin</button>
                                    </form>

                                    <form action="{{ route('deleteSkin', ['block' => $block->id, 'id' => $skin->id]) }}" class="inline-block">
                                        <button class="btn btn-danger btn-xs">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datatable').DataTable();
        });
    </script>
@endsection