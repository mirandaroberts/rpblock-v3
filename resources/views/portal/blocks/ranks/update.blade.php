@extends('layouts.portal')

@section('title')
    Portal - Update {{ ucfirst($block->settings->rank_name) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Update {{ ucfirst($block->settings->rank_name) }} - {{ $rank->name() }}</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="create-plan" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name_male') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_male">Name (Males)</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name_male" name="name_male" value="{{ $rank->name_male }}" maxlength="20" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('name_male'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name_male') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name_female') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_male">Name (Females)</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name_female" name="name_female" value="{{ $rank->name_female }}" maxlength="20" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('name_female'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name_female') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('min_age') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="min_age">Minimum Age (In Months)</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="min_age" name="min_age" value="{{ $rank->min_age }}" maxlength="20" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('min_age'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('min_age') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('max_age') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="max_age">Maximum Age (In Months)</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="max_age" name="max_age" value="{{ $rank->max_age }}" maxlength="20" class="form-control col-md-7 col-xs-12">

                                @if ($errors->has('max_age'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('max_age') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('posts_needed') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="posts_needed">Posts Needed</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="posts_needed" name="posts_needed" value="{{ $rank->posts_needed }}" maxlength="20" class="form-control col-md-7 col-xs-12">
                                <small>The number of posts required to challenge for or be awarded this {{ $block->settings->rank_name }}.</small>

                                @if ($errors->has('posts_needed'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('posts_needed') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('post_reqs') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_reqs">Post Requirements</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="post_reqs" name="post_reqs" value="{{ $rank->post_reqs }}" maxlength="20" class="form-control col-md-7 col-xs-12">
                                <small>The number of posts required each month to keep this {{ $block->settings->rank_name }}.</small>

                                @if ($errors->has('post_reqs'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('post_reqs') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('max_amount') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="max_amount">Max Amount</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="max_amount" name="max_amount" value="{{ $rank->max_amount }}" maxlength="20" class="form-control col-md-7 col-xs-12">
                                <small>The number of characters that can be this {{ $block->settings->rank_name }} at one time.</small>

                                @if ($errors->has('max_amount'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('max_amount') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('leader') ? ' has-error' : '' }}">
                            <input type="hidden" name="leader" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leader">Leader</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="leader" class="js-switch" data-switchery="true" @if ($rank->leader) checked @endif></label>

                            @if ($errors->has('leader'))
                                <span class="help-block">
                                <strong>{{ $errors->first('leader') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('coleader') ? ' has-error' : '' }}">
                            <input type="hidden" name="coleader" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leader">Co-leader</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="coleader" class="js-switch" data-switchery="true" @if ($rank->coleader) checked @endif></label>

                            @if ($errors->has('coleader'))
                                <span class="help-block">
                                <strong>{{ $errors->first('coleader') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('default') ? ' has-error' : '' }}">
                            <input type="hidden" name="default" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="default">Default</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="default" class="js-switch" data-switchery="true" @if ($rank->default) checked @endif></label>

                            @if ($errors->has('default'))
                                <span class="help-block">
                                <strong>{{ $errors->first('default') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_edit') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_edit" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_edit">Can edit rules & alliances</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_edit" class="js-switch" data-switchery="true" @if ($rank->can_edit) checked @endif></label>

                            @if ($errors->has('can_edit'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_edit') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_update') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_update" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_update">Can update news</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_update" class="js-switch" data-switchery="true" @if ($rank->can_update) checked @endif></label>

                            @if ($errors->has('can_update'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_update') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_invite') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_invite" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_invite">Can invite new members</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_invite" class="js-switch" data-switchery="true" @if ($rank->can_invite) checked @endif></label>

                            @if ($errors->has('can_invite'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_invite') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_remove') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_remove" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_remove">Can remove members</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_remove" class="js-switch" data-switchery="true" @if ($rank->can_remove) checked @endif></label>

                            @if ($errors->has('can_remove'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_remove') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_promote') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_promote" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_promote">Can promote members</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_promote" class="js-switch" data-switchery="true" @if ($rank->can_promote) checked @endif></label>

                            @if ($errors->has('can_promote'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_promote') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_demote') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_demote" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_demote">Can demote members</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_demote" class="js-switch" data-switchery="true" @if ($rank->can_demote) checked @endif></label>

                            @if ($errors->has('can_demote'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_demote') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('can_be_challenged') ? ' has-error' : '' }}">
                            <input type="hidden" name="can_be_challenged" value="0">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="can_be_challenged">Can be challenged for</label>
                            <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="can_be_challenged" class="js-switch" data-switchery="true" @if ($rank->can_be_challenged) checked @endif></label>

                            @if ($errors->has('can_be_challenged'))
                                <span class="help-block">
                                <strong>{{ $errors->first('can_be_challenged') }}</strong>
                            </span>
                            @endif
                        </div>

                        @if ($block->hasWidget(6))
                            <div class="form-group {{ $errors->has('prisoner') ? ' has-error' : '' }}">
                                <input type="hidden" name="prisoner" value="0">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prisoner">Prisoner</label>
                                <label class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" value="1" name="prisoner" class="js-switch" data-switchery="true" @if ($rank->prisoner) checked @endif></label>

                                @if ($errors->has('prisoner'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('prisoner') }}</strong>
                                </span>
                                @endif
                            </div>
                        @endif

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{ route('editRanks', ['id' => $group->id]) }}" class="btn btn-primary">Cancel</a>
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.cp').colorpicker({

            });
        })
    </script>
@endsection