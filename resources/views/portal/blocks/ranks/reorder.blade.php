@extends('layouts.portal')

@section('title')
    Portal - Reorder {{ ucfirst($block->settings->rank_name) }}s
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Reorder {{ $group->name }}'s {{ ucfirst($block->settings->rank_name) }}s</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-justify">Drag and drop the {{ $block->settings->rank_name }}s below until they are in the desired order. Co-leaders cannot sit below regular {{ $block->settings->rank_name }}s and the leader will remain at the top of the order.</p>

                    <div class="ln_solid"></div>
                    <h5>Leader</h5>
                    <ul>
                        <li>{!! $group->leader->name() !!}</li>
                    </ul>

                    <h5>Co-leaders</h5>
                    <ul class="reorder" id="coleader">
                        @foreach ($group->ranks as $rank)
                            @if ($rank->coleader && !$rank->leader)
                                <li class="sort-co" id="{{ $rank->id }}">{!! $rank->name() !!}</li>
                            @endif
                        @endforeach
                    </ul>

                    <h5>Regular {{ ucfirst($block->settings->rank_name) }}s</h5>
                    <ul class="reorder" id="ranks">
                        @foreach ($group->ranks as $rank)
                            @if (!$rank->coleader && !$rank->leader)
                                <li class="sort-ranks" id="{{ $rank->id }}">{!! $rank->name() !!}</li>
                            @endif
                        @endforeach
                    </ul>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('editRanks', ['id' => $group->id]) }}" class="btn btn-primary">Finished</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script>
        $(document).ready(function() {
            // Set data object
            var data = {
                _token: '{{ csrf_token() }}',
                group: {{ $group->id }},
                coleaders: [0],
                ranks: [0]
            };

            // Set initial order
            $('.sort-co').each(function() {
                data.coleaders.push(this.id);
            });

            $('.sort-ranks').each(function() {
                data.ranks.push(this.id);
            });

            // Set list to sortable
            $('#coleaders').sortable({
                scroll: false,
                scrollSensitivity: 80,
                scrollSpeed: 3,
                update: function(event, ui) {
                    // Clear and repush
                    data.coleaders = [0];
                    $('.sort-co').each(function() {
                        data.coleaders.push(this.id);
                    });

                    update();
                }
            });

            // Set list to sortable
            $('#ranks').sortable({
                scroll: false,
                scrollSensitivity: 80,
                scrollSpeed: 3,
                update: function(event, ui) {
                    // Clear and repush
                    data.ranks = [0];
                    $('.sort-ranks').each(function() {
                        data.ranks.push(this.id);
                    });

                    update();
                }
            });

            function update() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('processReorderRanks') }}',
                    data: data,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'error') {
                            alert(data.message);
                        } else {

                        }
                    },
                    error: function(data) {
                        // Error...
                        console.log(data.responseText);
                        var errors = $.parseJSON(data.responseText);
                        console.log(errors);
                    }
                });
            }
        });
    </script>

@endsection