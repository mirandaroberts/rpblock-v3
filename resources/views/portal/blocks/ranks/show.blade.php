@extends('layouts.portal')

@section('title')
    Portal - {{ ucfirst($block->settings->rank_name) }}s
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ $group->name }}'s {{ ucfirst($block->settings->rank_name) }}s</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="{{ route('createRank', ['id' => $group->id]) }}">
                            <button class="btn btn-success">New {{ ucfirst($block->settings->rank_name) }}</button>
                        </form>

                        <form action="{{ route('reorderRanks', ['id' => $group->id]) }}">
                            <button class="btn btn-primary">Reorder {{ ucfirst($block->settings->rank_name) }}s</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if (!$group->leader)
                    <p class="text-justify"><span class="label label-danger">Notice!</span> This {{ $block->settings->group_name }} doesn't have a Leader {{ $block->settings->rank_name }}. One must be made before this {{ $block->settings->group_name }} will be active on your block.</p>
                @endif

                @if (!$group->defaultRank)
                    <p class="text-justify"><span class="label label-danger">Notice!</span> This {{ $block->settings->group_name }} doesn't have a Default {{ $block->settings->rank_name }}. One must be made before this {{ $block->settings->group_name }} will be active on your block.</p>
                @endif

                <div class="ln_solid"></div>

                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Name</th>
                        <th>Leader</th>
                        <th>Co-leader</th>
                        <th>Default</th>
                        <th>Posts Needed</th>
                        <th>Post Requirement</th>
                        <th>Minimum Age</th>
                        <th>Maximum Age</th>
                        <th>Challengeable</th>
                        <th>Privileges</th>
                        @if ($block->hasWidget(6))
                            <th>Prisoner</th>
                        @endif
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($group->ranks as $rank)
                    <tr>
                        <td>{{ $rank->weight }}</td>
                        <td>{!! $rank->name() !!}</td>
                        <td>
                            @if ($rank->leader)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if ($rank->coleader)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if ($rank->default)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if ($rank->posts_needed)
                                {{ $rank->posts_needed }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            @if ($rank->post_reqs)
                                {{ $rank->post_reqs }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            @if ($rank->min_age)
                                {{ $rank->min_age }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            @if ($rank->max_age)
                                {{ $rank->max_age }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            @if ($rank->can_be_challenged)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if ($rank->can_edit)
                                Can Edit<br>
                            @endif

                            @if ($rank->can_update)
                                Can Update<br>
                            @endif

                            @if ($rank->can_invite)
                                Can Invite<br>
                            @endif

                            @if ($rank->can_remove)
                                Can Remove<br>
                            @endif

                            @if ($rank->can_promote)
                                Can Promote<br>
                            @endif

                            @if ($rank->can_demote)
                                Can Demote<br>
                            @endif
                        </td>
                        @if ($block->hasWidget(6))
                            <td>
                                @if ($rank->prisoner)
                                    Yes
                                @else
                                    No
                                @endif
                            </td>
                        @endif
                        <td>
                            <form action="{{ route('editRank', ['block' => $block->id, 'id' => $rank->id]) }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit {{ ucfirst($block->settings->rank_name) }}</button>
                            </form>

                            <form action="{{ route('deleteRank', ['block' => $block->id, 'id' => $rank->id]) }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection