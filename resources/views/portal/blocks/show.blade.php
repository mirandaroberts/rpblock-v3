@extends('layouts.portal')

@section('title')
    Portal - Blocks
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Blocks</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="/portal/blocks/create">
                            <button class="btn btn-success">New Block</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Plan</th>
                        <th>User</th>
                        <th>Players</th>
                        <th>Initialized</th>
                        <th>Last Paid</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($blocks as $block)
                    <tr>
                        <td>{{ $block->id }}</td>
                        <td>{{ $block->name }}</td>
                        <td>{{ $block->plan->name }}</td>
                        <td>{{ $block->user->username }}</td>
                        <td>{{ $block->players->count() }}</td>
                        <td>{{ $block->initialized }}</td>
                        <td>
                            @if ($block->last_paid)
                                {{ $block->last_paid->format('m-d-Y') }}
                            @else
                                Never
                            @endif
                        </td>
                        <td>
                            <form action="/portal/blocks/{{ $block->id }}" class="inline-block">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Settings</button>
                            </form>

                            <form action="/portal/blocks/update/{{ $block->id }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Block</button>
                            </form>

                            <form action="/portal/blocks/delete/{{ $block->id }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection