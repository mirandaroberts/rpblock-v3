@extends('layouts.portal')

@section('title')
    Portal - Thread Types
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Thread Types</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="{{ route('createType', ['id' => $block->id]) }}">
                            <button class="btn btn-success">New Thread Type</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="text-justify">Thread types are what your players will use to identify thread statuses and events. Each block comes prepackaged with three default types: <b>All Welcome (AW), Private (P),</b> and <b>Mature (M)</b>. By default, a thread will always have at least Private or All-Welcome. All welcome threads are threads that any character can tag into, while only the already tagged characters can post in private threads. Mature posts will deliver a warning to players before showing them the thread. A player can add any number of other thread types to a thread as well to better identify their thread's purpose. Examples of types you might make are Task (T) to show that a task is being completed for skill points (see Character Skills widget) or War (WAR) to show that a battle is being fought in that thread.</p>

                <div class="ln_solid"></div>

                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Acronym</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    <tr>
                        <td>All-Welcome</td>
                        <td>AW</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Private</td>
                        <td>P</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Mature</td>
                        <td>M</td>
                        <td></td>
                    </tr>
                    @foreach ($block->types as $type)
                    <tr>
                        <td>{{ $type->name }}</td>
                        <td>{{ strtoupper($type->acronym) }}</td>
                        <td>
                            <form action="{{ route('editType', ['block' => $block->id, 'id' => $type->id]) }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Thread Type</button>
                            </form>

                            <form action="{{ route('deleteType', ['block' => $block->id, 'id' => $type->id]) }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection