@extends('layouts.portal')

@section('title')
    Portal - Plans
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Plans</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="/portal/plans/create">
                            <button class="btn btn-success">New Plan</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Monthly Cost</th>
                        <th>Max Players</th>
                        <th>Max Characters</th>
                        <th>Blocks</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($plans as $plan)
                    <tr>
                        <td>{{ $plan->id }}</td>
                        <td>{{ $plan->name }}</td>
                        <td>{{ $plan->amt }}</td>
                        <td>
                            @if (!$plan->players)
                                ∞
                            @else
                                {{ $plan->players }}
                            @endif
                        </td>
                        <td>
                            @if (!$plan->characters)
                                ∞
                            @else
                                {{ $plan->characters }}
                            @endif
                        </td>
                        <td>{{ $plan->blocks->count() }}</td>
                        <td>
                            <form action="/portal/plans/update/{{ $plan->id }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit Plan</button>
                            </form>

                            <form action="/portal/plans/delete/{{ $plan->id }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection