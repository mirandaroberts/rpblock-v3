@extends('layouts.portal')

@section('title')
    Portal - Create Plan
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create Plan</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="create-plan" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="name" required="name" name="name" value="{{ old('name') }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('amt') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amt">Monthly Cost <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="amt" required="required" name="amt" value="{{ old('amt') }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('amt'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('amt') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('players') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="players">Max Players <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="players" required="required" name="players" value="{{ old('players') }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('players'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('players') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('characters') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="characters">Max Characters <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="characters" required="required" name="characters" value="{{ old('characters') }}" class="form-control col-md-7 col-xs-12">

                            @if ($errors->has('characters'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('characters') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="/portal/plans" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script data-rocketsrc="/public/vendors/parsleyjs/dist/parsley.min.js" type="text/rocketscript" data-rocketoptimized="true"></script>
@endsection