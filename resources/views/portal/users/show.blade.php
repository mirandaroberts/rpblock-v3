@extends('layouts.portal')

@section('title')
    Portal - Users
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <form action="/portal/users/create">
                            <button class="btn btn-success">New User</button>
                        </form>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Options</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->firstname }}</td>
                        <td>{{ $user->lastname }}</td>
                        <td>
                            <form action="/portal/users/update/{{ $user->id }}" class="inline-block">
                                <button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit User</button>
                            </form>

                            <form action="/portal/users/delete/{{ $user->id }}" class="inline-block">
                                <button class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection